#include "Project.h"

TableOfConstantsRow * sConstructorDeclarator::getTableOfConstantsRow() {
	if ( this->table_of_constants_row == NULL ) {
		UTF8Row * _name = new UTF8Row("<init>");
		this->getConstantsTable()->addRow( _name );
		vector< HandlerRow * > _args;
		if ( this->formal_parameter_list != NULL ) {
			sFormalParameter * _parameter =  this->formal_parameter_list->begin;
			while (_parameter != NULL) {
				_args.push_back( (HandlerRow *) _parameter->type->getTableOfConstantsRow() );
				_parameter = _parameter->next;
			}
		}
		MethodHandlerRow * _type = new MethodHandlerRow( _args, new HandlerRow( false, false, "V" ) );
		this->getConstantsTable()->addRow( _type );
		this->table_of_constants_row = new NameAndTypeRow(_name, _type);
		this->getConstantsTable()->addRow( this->table_of_constants_row );
	}
	return this->table_of_constants_row;
}