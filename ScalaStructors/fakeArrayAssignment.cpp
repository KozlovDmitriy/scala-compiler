#include "Project.h"

fakeArrayAssignment::fakeArrayAssignment(eAssignmentOperator _type,
	sName * _arrayname,
	Expression * _indexator,
	Expression * _source,
	Expression * _assigment_expression
	) : sAssignmentOperator(_type)
{
	arrayname = _arrayname;
	indexator =_indexator;
	source = _source;
	assigment_expression = _assigment_expression;

	this->setDotLabel(string("[]")+this->dot_label);
	this->cpp_type = "fakeArrayAssignment";
	this->transformed = true;

	this->addChildren(source, "source");
	this->addChildren(arrayname, "arrayname");
	this->addChildren(indexator, "indexator");
	this->addChildren(assigment_expression, "assigment_expression");
}