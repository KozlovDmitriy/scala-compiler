#pragma once

class IdManager
{
public:
	IdManager(void) { 
		id = 0;
	}
	~IdManager(void) {
	}

	unsigned int getNextId() {
		++id;
		return id;
	}
private:
	unsigned int id;
};