#include "Errors.h"
#include "Project.h"

extern int yyparse();
/*extern int errorCode;
extern char errorMessage[256];*/
extern FILE * yyin;
extern sProgrammStatement * root;

IdManager id_manager;

void write_dot(const char * fname, string * dot_script) {
    ofstream out(fname);
    out << *dot_script;
	out.close();
}

void saveTreeInImage(string * dot_script) {
	static int k = 0;
	string dotfilename = string("scalatree")
		+ to_string(++k)
		+ string(".dot");
	write_dot(dotfilename.c_str(), dot_script);
	string dotcmd = string("dot ") 
		+ dotfilename
		+ string(" -Tsvg -oscalatree") 
		+ to_string(k) 
		+ string(".svg");
	system(dotcmd.c_str());
} 

string getTreeDotScript() {
	string dot_script = ( (Node * ) root )->getDotScript();
	return (new string("digraph ScalaTree {\r\n"))->append( dot_script ).append("}");
}

int main(int argc, char* argv[]) {
	/*if ( argc > 1 ) {
	yyin = fopen( argv[1], "r" );
	} else {
	yyin = stdin;
	} */ 
	yyin = fopen( "in.txt", "r" );
	yyparse();
	fclose(yyin);
	if (root != NULL) {
		string dot_script = getTreeDotScript();
		saveTreeInImage(&dot_script);
		root->createTableOfConstants();
		root->transformTree();
		root->createGlobalConstantsTable();
		root->generateByteCode( NULL );
		string dot_script2 = getTreeDotScript();
		saveTreeInImage(&dot_script2);
		// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		// @ AFTER THAT LINE YOU __MUST__ USE GET_CHILD(NAME) @
		// @ INSTEAD OF THIS->....                            @
		// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		try {
			root->attribution();
		}
		catch (Error * e) {
			string dot_scriptAttr = getTreeDotScript();
			saveTreeInImage(&dot_scriptAttr);
			DIE(e->initiator, e->msg);
		}
		string dot_scriptAttr = getTreeDotScript();
		saveTreeInImage(&dot_scriptAttr);
		//delete root;

	}
	system("pause");
}