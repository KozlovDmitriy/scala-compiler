#include "Project.h"

TableOfConstantsRow * sFieldDeclaration::getTableOfConstantsRow() {
	if ( this->table_of_constants_row == NULL ) {
		UTF8Row * _name = (UTF8Row *) this->variable_declarator->getTableOfConstantsRow();
		UTF8Row * _type = (UTF8Row *) this->type->getTableOfConstantsRow();
		this->getConstantsTable()->addRow( _type );
		NameAndTypeRow * _name_and_type = new NameAndTypeRow( _name, _type );
		this->getConstantsTable()->addRow( _name_and_type );
		sClassDeclaration * _class_decl = (sClassDeclaration *) this->findParentOfType( & string("sTypeDeclaration") );
		ClassRow * _class = (ClassRow *) _class_decl->getTableOfConstantsRow();
		this->table_of_constants_row = new FieldRefRow(_class, _name_and_type, this);
		this->getConstantsTable()->addRow( this->table_of_constants_row );
	}
	return this->table_of_constants_row;
}