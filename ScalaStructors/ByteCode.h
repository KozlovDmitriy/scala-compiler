#pragma once

const unsigned short TWOBYTES_MAX         =  32767;
const unsigned short TWOBYTES_MIN         = -32768;

const unsigned int	  MAGIC_NUMBER        = 0xCAFEBABE;
const unsigned short  VERSION_MINOR       = 0x0000;
const unsigned short  VERSION_MAJOR       = 0x0032;

const unsigned short  ACC_PUBLIC          = 0x0001;
const unsigned short  ACC_PRIVATE         = 0x0002;
const unsigned short  ACC_PROTECTED       = 0x0004;
const unsigned short  ACC_STATIC          = 0x0008;
const unsigned short  ACC_FINAL           = 0x0010;
const unsigned short  ACC_SUPER           = 0x0020;
const unsigned short  ACC_VOLATILE        = 0x0040;
const unsigned short  ACC_TRANSIENT       = 0x0080;
const unsigned short  ACC_INTERFACE       = 0x0200;
const unsigned short  ACC_ABSTRACT        = 0x0400;
const unsigned short  ACC_SYNTHETIC       = 0x1000;
const unsigned short  ACC_ANNOTATION      = 0x2000;
const unsigned short  ACC_ENUM            = 0x4000;

const unsigned char  CMD_ACONST_NULL     = 0x01;
const unsigned char  CMD_ICONST_1        = 0x04;
const unsigned char  CMD_BIPUSH          = 0x10;
const unsigned char  CMD_SIPUSH          = 0x11;
const unsigned char  CMD_LDC             = 0x12;
const unsigned char  CMD_LDC_W           = 0x13;
const unsigned char  CMD_ILOAD           = 0x15;
const unsigned char  CMD_ALOAD           = 0x19;
const unsigned char  CMD_ALOAD_0         = 0x2A;
const unsigned char  CMD_ISTORE          = 0x36;
const unsigned char  CMD_ASTORE          = 0x3A;
const unsigned char  CMD_AASTORE         = 0x53;
const unsigned char  CMD_POP             = 0x57;
const unsigned char  CMD_DUP             = 0x59;
const unsigned char  CMD_IADD            = 0x60;
const unsigned char  CMD_ISUB            = 0x64;
const unsigned char  CMD_IFEQ            = 0x99;
const unsigned char  CMD_IFNE            = 0x9A;
const unsigned char  CMD_ICMPEQ          = 0x9F;
const unsigned char  CMD_ICMPNE          = 0xA0;
const unsigned char  CMD_ICMPLT          = 0xA1;
const unsigned char  CMD_ICMPLE          = 0xA4;
const unsigned char  CMD_ICMPGT          = 0xA3;
const unsigned char  CMD_ICMPGE          = 0xA2;
const unsigned char  CMD_GOTO            = 0xA7;
const unsigned char  CMD_IINC            = 0x84;
const unsigned char  CMD_ARETURN         = 0xB0;
const unsigned char  CMD_RETURN          = 0xB1;
const unsigned char  CMD_GETFIELD        = 0xB4;
const unsigned char  CMD_PUTFIELD        = 0xB5;
const unsigned char  CMD_INVOKEVIRTUAL   = 0xB6;
const unsigned char  CMD_INVOKESPECIAL   = 0xB7;
const unsigned char  CMD_INVOKESTATIC    = 0xB8;
const unsigned char  CMD_INVOKEINTERFACE = 0xB9;
const unsigned char  CMD_NEW             = 0xBB;
const unsigned char  CMD_ANEWARRAY       = 0xBD;
const unsigned char  CMD_CHECKCAST       = 0xC0;

const unsigned short  STACK_SIZE         = 1024;