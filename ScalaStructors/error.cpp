#include "Project.h"
void DIE(string initiator, string msg) {
	DIE(string("unspecified"), initiator, msg, string("unspecified"));
}

void DIE(string cpp_type, string initiator, string msg, string stage) {
	cout << "\n\n\n\n@@@@@ DIED WITH ERROR: @@@@@\n\nmessage:\n" 
		<< msg 
		<< "\n\nin fuction:\n" 
		<< initiator 
		<< "\n\ncpp type:\n" 
		<< cpp_type 
		<< "\n\nin stage:\n" 
		<< stage 
		<< "\n\n" 
		<< endl;
	system("pause");
	exit(0);
}