#pragma once

#include "Project.h"

class BinaryWriter
{
private:
    ofstream stream;

public:
    BinaryWriter();
	BinaryWriter( string fileName ) {
		this->stream = ofstream(fileName, ios::out | ios::binary);
	}

	~BinaryWriter() {
		this->stream.close();
	}

	bool isAlive() const {
		return this->stream.is_open();
	}

	void writeU4( unsigned int value ) {
		 if ( this->isAlive() ) {
			stream << value;
		}
	}

	void writeU2( unsigned short value ) {
		if ( this->isAlive() ) {
			stream << value;
		}
	}

	void writeU1( unsigned char value ) {
		if ( this->isAlive() ) {
			stream << value;
		}
	}

	void writeF4( float value ) {
		 if ( isAlive() ) {
			char * data = (char*) & value;
			stream.write(&data[3], 1);
			stream.write(&data[2], 1);
			stream.write(&data[1], 1);
			stream.write(&data[0], 1);
		}
	}

	void writeByteArray( vector<unsigned char> bytes ) {
		if ( isAlive() ) {
			for (unsigned char byte : bytes) {
				stream << byte;
			}
		}
	}

};