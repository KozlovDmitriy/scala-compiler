#include "Project.h"

enum EAttr : int {
	UNKNOWN,
	CBOOL,
	CCHAR,
	CBYTE,
	CINT,
	CFLOAT,
	CLONG,
	CDOUBLE,
	CSTRING,
	CNULL
};

class Attr {
public:
	int ariphmetic_type;
	int dimentions;
	bool is_print;
	bool is_static;
	string descriptor;
	Attr() {
		ariphmetic_type = UNKNOWN;
		dimentions = 0;
		is_print = false;
		is_static = false;
		descriptor = string();
	}
	/*Attr(int _ariphmetic_type, int _dimentions) : Attr() {
		ariphmetic_type = _ariphmetic_type;
		dimentions = _dimentions;
	}*/
};