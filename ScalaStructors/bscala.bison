%{
	#include "Project.h"

    extern int yylex(void);
    /*extern enum error_types errorCode;
    extern char errorMessage[256];*/

    void yyerror(const char * str);

    sProgrammStatement * root;
%}

%token TDEF
%token TIMPORT
%token TPACKAGE
%token TIF
%token TTHEN
%token TELSE
%token TWHILE
%token TFOR
%token TDO
%token TBOOLEAN
%token TINT
%token TDOUBLE
%token TBYTE
%token TSHORT
%token TCHAR
%token TLONG
%token TFLOAT
%token UNIT
%token VAL
%token WITH
%token TTYPE
%token VAR
%token YIELD
%token TRETURN
%token TTRUE
%token TFALSE
%token TNULL
%token TTHIS
%token SUPER
%token STRING_TCLASS
%token ARRAY_TCLASS
%token TPRIVATE
%token TPROTECTED
%token OVERRIDE
%token TABSTRACT
%token FINAL
%token SEALED
%token TTHROW
%token TTRY
%token TCATCH
%token FINALLY
%token TRAIT
%token TCLASS
%token TCASE
%token TFORSOME
%token IMPLICIT
%token LAZY
%token MATCH
%token TNEW
%token TDOTTDOTTDOT
%token EXTENDS
%token ANYVAR
%token TOBJECT
%token TBOOLEAN_JT
%token TBYTE_JT
%token TCHARACTER_JT
%token TDOUBLE_JT
%token TFLOAT_JT
%token TINTEGER_JT
%token TOBJECT_JT
%token TSHORT_JT
%token VOID_JT
%token TCLASS_JT
%token NUMBER_JT
%token TPACKAGE_JT
%token STRINGBUFFER_JT
%token STRINGBUILDER_JT
%token TCHARSEQUENCE_JT
%token THREAD_JT
%token TTHROWABLE_JI
%token CLONABLE_JI
%token COMPARABLE_JI
%token SERIALIZABLE_JI
%token RUNNABLE_JI
%token SEMICOLON
%token COMMA
%token POINT
%token ASSIGNING
%token GT
%token LT
%token NOT
%token TILDE
%token QUESTION
%token COLON
%token EQEQ
%token LTEQ
%token GTEQ
%token COMPILES
%token NOTEQ
%token ANDAND
%token OROR
%token PLUSPLUS
%token MINUSMINUS
%token PLUS
%token MINUS
%token TIMES
%token DIV
%token AND
%token OR
%token CIRCUMFLEX
%token XOR
%token LTLT
%token GTGT
%token GTGTGT
%token PLUSEQ
%token MINUSEQ
%token TIMESEQ
%token DIVEQ
%token ANDEQ
%token OREQ
%token CIRCUMFLEXEQ
%token XOREQ
%token LTLTEQ
%token GTGTEQ
%token GTCOLON
%token LTCOLON
%token LTDIV
%token LTMINUS
%token COLONCOLONCOLON
%token GTGTGTEQ
%token ID
%token STRINGLITERAL
%token TCHARLITERAL
%token TINTLITERAL
%token TLONGLITERAL
%token TFLOATLITERAL
%token TDOUBLELITERAL
%token TPUBLIC
%token TSTATIC
%token TARRAY
%token TUNTIL

%right ASSIGNING PLUSEQ MINUSEQ TIMESEQ DIVEQ ANDEQ OREQ CIRCUMFLEXEQ XOREQ
%left SEMICOLON
%left COMMA
%left POINT
%left GT
%left LT
%left NOT
%left TILDE
%left QUESTION
%left COLON
%left EQEQ
%left LTEQ
%left GTEQ
%left COMPILES
%left NOTEQ
%left ANDAND
%left OROR
%left PLUSPLUS
%left MINUSMINUS
%left PLUS
%left MINUS
%left TIMES
%left DIV
%left AND
%left OR
%left CIRCUMFLEX
%left XOR
%left LTLT
%left GTGT
%left GTGTGT
%left LTLTEQ
%left GTGTEQ
%left GTCOLON
%left LTCOLON
%left LTDIV
%left LTMINUS
%left COLONCOLONCOLON
%left GTGTGTEQ
%nonassoc ')'

%union {
    char * char_value;
    double * double_value;
    float * float_value;
    long long * long_value;
    long * int_value;
    string * string_value;
    sType * sType_value;
    sPrimitiveType * sPrimitiveType_value;
    sReferenceType * sReferenceType_value;
    sArrayType * sArrayType_value;
    sExpression * sExpression_value;
    sAssignmentExpression * sAssignmentExpression_value;
    sAssignmentOperator * sAssignmentOperator_value;
    sConditionalExpression * sConditionalExpression_value;
    sConditionalOrExpression * sConditionalOrExpression_value;
    sConditionalAndExpression * sConditionalAndExpression_value;
    sInclusiveOrExpression * sInclusiveOrExpression_value;
    sExclusiveOrExpression * sExclusiveOrExpression_value;
    sAndExpression * sAndExpression_value;
    sEqualityExpression * sEqualityExpression_value;
    sRelationalExpression * sRelationalExpression_value;
    sShiftExpression * sShiftExpression_value;
    sAdditiveExpression * sAdditiveExpression_value;
    sMultiplicativeExpression * sMultiplicativeExpression_value;
    sUnaryExpression * sUnaryExpression_value;
    sPreIncrementExpression * sPreIncrementExpression_value;
    sPreDecrementExpression * sPreDecrementExpression_value;
    sUnaryExpressionNotPlusMinus * sUnaryExpressionNotPlusMinus_value;
    sPostfixExpression * sPostfixExpression_value;
    sPrimary * sPrimary_value;
    sArrayCreationExpression * sArrayCreationExpression_value;
    sPostIncrementExpression * sPostIncrementExpression_value;
    sPostDecrementExpression * sPostDecrementExpression_value;
    sAssignment * sAssignment_value;
    sLeftHandSide * sLeftHandSide_value;
    sFieldAccess * sFieldAccess_value;
    sArrayAccess * sArrayAccess_value;
    sPrimaryNoNewArray * sPrimaryNoNewArray_value;
    sLiteral * sLiteral_value;
    sBooleanLiteral * sBooleanLiteral_value;
    sClassInstanceCreationExpression * sClassInstanceCreationExpression_value;
    sMethodInvocation * sMethodInvocation_value;
    sArgumentList * sArgumentList_value;
    Statement * sStatementWithoutTrailingSubstatement_value;
    sExpressionStatement * sExpressionStatement_value;
    sStatementExpression * sStatementExpression_value;
    sDoStatement * sDoStatement_value;
    sReturnStatement * sReturnStatement_value;
    sThrowStatement * sThrowStatement_value;
    sTryStatement * sTryStatement_value;
    sCatches * sCatches_value;
    sCatchClause * sCatchClause_value;
    sFinally * sFinally_value;
    sIfThenStatement * sIfThenStatement_value;
    sIfThenElseStatement * sIfThenElseStatement_value;
    Statement * sStatementNoShortIf_value;
    sIfThenElseStatementNoShortIf * sIfThenElseStatementNoShortIf_value;
    sWhileStatementNoShortIf * sWhileStatementNoShortIf_value;
    sForStatementNoShortIf * sForStatementNoShortIf_value;
    sForInit * sForInit_value;
    sStatementExpressionList * sStatementExpressionList_value;
    sForUpdate * sForUpdate_value;
    sWhileStatement * sWhileStatement_value;
    sProgramm * sProgramm_value;
    sName * sName_value;
    sTypeDeclarations * sTypeDeclarations_value;
    sTypeDeclaration * sTypeDeclaration_value;
    sClassDeclaration * sClassDeclaration_value;
    sSingletonDeclaration * sSingletonDeclaration_value;
    sClassBody * sClassBody_value;
    sClassBodyDeclarations * sClassBodyDeclarations_value;
    sClassBodyDeclaration * sClassBodyDeclaration_value;
    sConstructorDeclaration * sConstructorDeclaration_value;
    sConstructorDeclarator * sConstructorDeclarator_value;
    sFormalParameterList * sFormalParameterList_value;
    sFormalParameter * sFormalParameter_value;
    sVariableDeclaratorId * sVariableDeclaratorId_value;
    sConstructorBody * sConstructorBody_value;
    sExplicitConstructorInvocation * sExplicitConstructorInvocation_value;
    sBlockStatements * sBlockStatements_value;
    sBlockStatement * sBlockStatement_value;
    sVariableInitializer * sVariableInitializer_value;
    sArrayInitializer * sArrayInitializer_value;
    sVariableInitializers * sVariableInitializers_value;
    sClassMemberDeclaration * sClassMemberDeclaration_value;
    sFieldDeclaration * sFieldDeclaration_value;
    sMethodDeclaration * sMethodDeclaration_value;
    sMethodHeader * sMethodHeader_value;
    sMethodDeclarator * sMethodDeclarator_value;
    sMethodType * sMethodType_value;
    Statement * sBlock_value;
    sThis * sThis_value;
    Expression * Expression_value;
	Statement * Statement_value;
    IfStatement * IfStatement_value;
	ForStatement * ForStatement_value;
}

%type <long_value> TLONGLITERAL
%type <double_value> TDOUBLELITERAL
%type <string_value> STRINGLITERAL
%type <char_value> TCHARLITERAL
%type <float_value> TFLOATLITERAL
%type <int_value> TINTLITERAL
%type <string_value> ID
%type <sType_value> Type
%type <sPrimitiveType_value> PrimitiveType
%type <sReferenceType_value> ReferenceType
%type <sArrayType_value> ArrayType
%type <Expression_value> Expression
%type <Expression_value> AssignmentExpression
%type <Expression_value> ConditionalExpression
%type <Expression_value> ConditionalOrExpression
%type <Expression_value> ConditionalAndExpression
%type <Expression_value> InclusiveOrExpression
%type <Expression_value> ExclusiveOrExpression
%type <Expression_value> AndExpression
%type <Expression_value> EqualityExpression
%type <Expression_value> RelationalExpression
%type <Expression_value> ShiftExpression
%type <Expression_value> AdditiveExpression
%type <Expression_value> MultiplicativeExpression
%type <Expression_value> UnaryExpression
%type <Expression_value> PreIncrementExpression
%type <Expression_value> PreDecrementExpression
%type <Expression_value> UnaryExpressionNotPlusMinus
%type <Expression_value> PostfixExpression
%type <Expression_value> Primary
%type <Expression_value> ArrayCreationExpression
%type <Expression_value> PostIncrementExpression
%type <Expression_value> PostDecrementExpression
%type <Expression_value> Assignment
%type <Expression_value> LeftHandSide
%type <sFieldAccess_value> FieldAccess
%type <Expression_value> PrimaryNoNewArray
%type <Expression_value> Literal
%type <sBooleanLiteral_value> BooleanLiteral
%type <sClassInstanceCreationExpression_value> ClassInstanceCreationExpression
%type <sMethodInvocation_value> MethodInvocation
%type <Expression_value> AssignmentOperator
%type <sArgumentList_value> ArgumentList
%type <Statement_value> Statement
%type <Statement_value> StatementWithoutTrailingSubstatement
%type <Statement_value> ExpressionStatement
%type <Statement_value> StatementExpression
%type <Statement_value> DoStatement
%type <Statement_value> ReturnStatement
%type <Statement_value> ThrowStatement
%type <Statement_value> TryStatement
%type <sCatches_value> Catches
%type <sCatchClause_value> CatchClause
%type <Statement_value> Finally
%type <Statement_value> IfThenStatement
%type <Statement_value> IfThenElseStatement
%type <Statement_value> IfThenElseStatementNoShortIf
%type <Statement_value> StatementNoShortIf
%type <Statement_value> WhileStatementNoShortIf
%type <Statement_value> ForStatementNoShortIf
%type <sForInit_value> ForInit
%type <sStatementExpressionList_value> StatementExpressionList
%type <sForUpdate_value> ForUpdate
%type <Statement_value> WhileStatement
%type <Statement_value> ForStatement
%type <sProgramm_value> ProgrammStatement
%type <sName_value> Name
%type <sTypeDeclarations_value> TypeDeclarations
%type <sTypeDeclaration_value> TypeDeclaration
%type <sClassDeclaration_value> ClassDeclaration
%type <sClassBody_value> ClassBody
%type <sClassBodyDeclarations_value> ClassBodyDeclarations
%type <sClassBodyDeclaration_value> ClassBodyDeclaration
%type <sConstructorDeclaration_value> ConstructorDeclaration
%type <sConstructorDeclarator_value> ConstructorDeclarator
%type <sFormalParameterList_value> FormalParameterList
%type <sFormalParameter_value> FormalParameter
%type <sConstructorBody_value> ConstructorBody
%type <sExplicitConstructorInvocation_value> ExplicitConstructorInvocation
%type <sBlockStatements_value> BlockStatements
%type <sBlockStatement_value> BlockStatement
%type <sVariableDeclaratorId_value> VariableDeclaratorId
%type <sVariableInitializer_value> VariableInitializer
%type <sArrayInitializer_value> ArrayInitializer
%type <sVariableInitializers_value> VariableInitializers
%type <sClassMemberDeclaration_value> ClassMemberDeclaration
%type <sFieldDeclaration_value> FieldDeclaration
%type <sMethodDeclaration_value> MethodDeclaration
%type <sMethodHeader_value> MethodHeader
%type <sMethodDeclarator_value> MethodDeclarator
%type <sMethodType_value> MethodType
%type <sBlock_value> Block
%type <sSingletonDeclaration_value> SingletonDeclaration

%start ProgrammStatement

%%
ProgrammStatement:
    TypeDeclarations { root = new sProgrammStatement($1); } |
    { root = NULL; };

TypeDeclarations:
    TypeDeclaration  { $$ = new sTypeDeclarations($1); } |
    TypeDeclarations TypeDeclaration { $$ = sTypeDeclarations::append($1, $2); } ;

TypeDeclaration:
    ClassDeclaration  { $$ = new sTypeDeclaration($1, NULL, CLASS_DECLARATION); } |
    SingletonDeclaration { $$ = new sTypeDeclaration(NULL, $1, SINGLETON_DECLARATION); } ;

Name:
    ID { $$ = new sName($1); } // параметр 1 должен быть string
    //Name POINT ID {$$ = sName::append($1, $3)};

BooleanLiteral:
    TTRUE  { $$ = new sBooleanLiteral(TRUE_LITERAL); } |
    TFALSE { $$ = new sBooleanLiteral(FALSE_LITERAL);} ;

Literal:
    TINTLITERAL           { $$ = (Expression *) (new sLiteral(*$1, 0.0, FALSE_LITERAL, '\0', "\0", 0,   0.0, INT_LITERAL    ) );} |
    TFLOATLITERAL         { $$ = (Expression *) (new sLiteral(0,   *$1, FALSE_LITERAL, '\0', "\0", 0,   0.0, FLOAT_LITERAL  ) );} |
    BooleanLiteral        { $$ = (Expression *) (new sLiteral(0,   0.0, *$1,           '\0', "\0", 0,   0.0, BOOLEAN_LITERAL) );} |
    TCHARLITERAL          { $$ = (Expression *) (new sLiteral(0,   0.0, FALSE_LITERAL,  *$1, "\0", 0,   0.0, CHAR_LITERAL   ) );} |
    STRINGLITERAL         { $$ = (Expression *) (new sLiteral(0,   0.0, FALSE_LITERAL, '\0', *$1,  0,   0.0, STRING_LITERAL ) );} |
    TNULL                 { $$ = (Expression *) (new sLiteral(0,   0.0, FALSE_LITERAL, '\0', "\0", 0,   0.0, NULL_LITERAL   ) );} |
    TLONGLITERAL          { $$ = (Expression *) (new sLiteral(0,   0.0, FALSE_LITERAL, '\0', "\0", *$1, 0.0, LONG_LITERAL   ) );} |
    TDOUBLELITERAL        { $$ = (Expression *) (new sLiteral(0,   0.0, FALSE_LITERAL, '\0', "\0", 0,   *$1, DOUBLE_LITERAL ) );} ;

Type:
    COLON PrimitiveType  { $$ = new sType($2, NULL, PRIMITIVE_TYPE); } |
    ReferenceType { $$ = new sType(NULL, $1, REFERENCE_TYPE); } ;

MethodType:
    Type  { $$ = new sMethodType($1, false); } |
    COLON UNIT { $$ = new sMethodType(NULL, true); } ;

PrimitiveType:
    TINT  { $$ = new sPrimitiveType(INT); } |
    TLONG  { $$ = new sPrimitiveType(LONG); } |
    TBYTE  { $$ = new sPrimitiveType(BYTE); } |
    TFLOAT  { $$ = new sPrimitiveType(FLOAT); } |
    TDOUBLE  { $$ = new sPrimitiveType(DOUBLE);} |
    TBOOLEAN  { $$ = new sPrimitiveType(BOOLEAN); } |
    TCHAR  { $$ = new sPrimitiveType(CHAR); } |
    STRING_TCLASS { $$ = new sPrimitiveType(STRING_CLASS); } ;

ReferenceType:
	COLON Name  { $$ = new sReferenceType($2, NULL); } |
	COLON ArrayType { $$ = new sReferenceType(NULL, $2); } ;

ArrayType:
	TARRAY '[' PrimitiveType ']'  { $$ = new sArrayType($3, NULL, ARRAY_PRIMITVE_TYPE); } |
	TARRAY '[' Name ']'  { $$ = new sArrayType(NULL, $3, ARRAY_NAME); };

ClassDeclaration: /*учесть отсуствие модификаторов*/
    //Modifiers TCLASS Name  dy { $$ = new sClassDeclaration($1, $3, NULL, NULL, $4); } |
    TCLASS Name ClassBody { $$ = new sClassDeclaration( $2, $3); };

SingletonDeclaration:
    //Modifiers TOBJECT Name ClassBody { $$ = new sSingletonDeclaration($1, $3, NULL, NULL, $4); } |
    TOBJECT Name ClassBody { $$ = new sSingletonDeclaration( $2, $3); };

ClassBody:
    '{' ClassBodyDeclarations '}'  { $$ = new sClassBody($2); } |
    '{' '}'  { $$ = new sClassBody(NULL); } ;

ClassBodyDeclarations:
    ClassBodyDeclaration  { $$ = new sClassBodyDeclarations($1); } |
    ClassBodyDeclarations ClassBodyDeclaration { $$ = sClassBodyDeclarations::append($1, $2); } ;

ClassBodyDeclaration:
    /*TOTDO!!! StaticInit  { } |*/
    ClassMemberDeclaration  { $$ = new sClassBodyDeclaration($1, NULL); } |
    ConstructorDeclaration { $$ = new sClassBodyDeclaration(NULL, $1); } ;

ClassMemberDeclaration:
    FieldDeclaration  { $$ = new sClassMemberDeclaration($1, NULL, FIELD_DECLARATION); } |
    MethodDeclaration { $$ = new sClassMemberDeclaration(NULL, $1, METHOD_DECLARATION);} ;

FieldDeclaration:
    VariableDeclaratorId Type SEMICOLON { $$ = (new sFieldDeclaration( $1, NULL, $2)); } |
	VariableDeclaratorId Type ASSIGNING VariableInitializer SEMICOLON { $$ = (new sFieldDeclaration( $1, $4, $2)); };

VariableDeclaratorId:
    VAR Name  { $$ = new sVariableDeclaratorId($2, false); } |
    VAL Name  { $$ = new sVariableDeclaratorId($2, true); } ;

VariableInitializer:
    Expression  { $$ = new sVariableInitializer($1, NULL, EXPRESSION); } |
    ArrayInitializer { $$ = new sVariableInitializer(NULL, $1, ARRAY_INITIALIZER); } ;

MethodDeclaration:
    //MethodHeader Block { $$ = new sMethodDeclaration($1, $2); } |
    MethodHeader Block { $$ = new sMethodDeclaration($1, $2); } |
    MethodHeader SEMICOLON { $$ = new sMethodDeclaration($1, NULL); };

/*посмотреть случай с VOName*/
MethodHeader:
    //Modifers MethodDeclarator MethodType  { $$ = new sMethodHeader($1, $2, $3); } |
    MethodDeclarator MethodType ASSIGNING { $$ = new sMethodHeader( $1, $2); } |
    MethodDeclarator  { $$ = new sMethodHeader( $1, NULL); };

MethodDeclarator:
    TDEF Name '(' FormalParameterList ')'      { $$ = new sMethodDeclarator($2, $4); } |
    TDEF Name '(' ')'                          { $$ = new sMethodDeclarator($2, NULL); } ;

FormalParameterList:
    FormalParameter  { $$ = new sFormalParameterList($1); } |
    FormalParameterList COMMA FormalParameter { $$ = sFormalParameterList::append($1, $3); } ;

FormalParameter:
    Name Type { $$ = new sFormalParameter($1, $2); };
	//FormalParameter '[' ']' {};// TODO - add dims to formal parameter

ConstructorDeclaration:
    //Modifiers ConstructorDeclarator ConstructorBody  { $$ = sConstructorDeclaration($1, $2, $3); } |
    ConstructorDeclarator ConstructorBody { $$ = new sConstructorDeclaration($1, $2); } ;

ConstructorDeclarator:
    Name '(' FormalParameterList ')'  { $$ = new sConstructorDeclarator($1, $3); } |
    Name '(' ')' { $$ = new sConstructorDeclarator($1, NULL); } ;

ConstructorBody:
    '{' ExplicitConstructorInvocation BlockStatements '}'  { $$ = new sConstructorBody($2, $3); } |
    '{' ExplicitConstructorInvocation '}'  { $$ = new sConstructorBody($2, NULL); } |
    '{' BlockStatements '}'  { $$ = new sConstructorBody(NULL, $2);} |
    '{' '}' { $$ = new sConstructorBody(NULL, NULL); } ;

ExplicitConstructorInvocation:
    TTHIS '(' ArgumentList ')' SEMICOLON  { $$ = new sExplicitConstructorInvocation($3, THIS_EXPLICIT_CONSTRUCTOR_INVOCATION); } |
    TTHIS '(' ')' SEMICOLON               { $$ = new sExplicitConstructorInvocation(NULL, THIS_EXPLICIT_CONSTRUCTOR_INVOCATION); } |
    SUPER '(' ArgumentList ')' SEMICOLON  { $$ = new sExplicitConstructorInvocation($3, SUPER_EXPLICIT_CONSTRUCTOR_INVOCATION);} |
    SUPER '(' ')' SEMICOLON               { $$ = new sExplicitConstructorInvocation(NULL, SUPER_EXPLICIT_CONSTRUCTOR_INVOCATION);} ;

ArrayInitializer:
    TARRAY '[' PrimitiveType ']' '(' VariableInitializers ')'	{ $$ = new sArrayInitializer($3, NULL, $6  ); } |
    TARRAY '[' Name ']' '(' VariableInitializers ')'			{ $$ = new sArrayInitializer(NULL, $3, $6  ); } |
    TARRAY '[' PrimitiveType ']' '('  ')'						{ $$ = new sArrayInitializer($3, NULL, NULL); } |
    TARRAY '[' Name ']' '('  ')'								{ $$ = new sArrayInitializer(NULL, $3, NULL); } ;

VariableInitializers:
    VariableInitializer  { $$ = new sVariableInitializers($1); } |
    VariableInitializers COMMA VariableInitializer { $$ = $$ = sVariableInitializers::append($1, $3); } ;

Block:
    '{' BlockStatements '}'	{ $$ = (Statement *) ($2); } |
    '{' '}'					{ $$ = NULL; } ;

BlockStatements:
    BlockStatement  { $$ = new sBlockStatements($1); } |
    BlockStatements BlockStatement { $$ = sBlockStatements::append($1, $2); } ;

BlockStatement:
    VariableDeclaratorId Type SEMICOLON  { $$ = new sBlockStatement(LOCAL_VARIBLE_DECLARATION, $1, $2, NULL, NULL ); } |
	VariableDeclaratorId Type ASSIGNING VariableInitializer SEMICOLON {$$ = new sBlockStatement(LOCAL_VARIBLE_DECLARATION, $1, $2, $4, NULL ); } |
    Statement { $$ = new sBlockStatement(STATEMENT, NULL, NULL, NULL, $1); } ;

Statement:
    StatementWithoutTrailingSubstatement	{ $$ = (Statement *) ($1); } |
    IfThenStatement							{ $$ = (Statement *) ($1); } |
    IfThenElseStatement						{ $$ = (Statement *) ($1); } |
    WhileStatement							{ $$ = (Statement *) ($1); } |
    ForStatement							{ $$ = (Statement *) ($1); } ;

StatementNoShortIf:
    StatementWithoutTrailingSubstatement	{ $$ = (Statement *) ($1); } |
    IfThenElseStatementNoShortIf			{ $$ = (Statement *) ($1); } |
    WhileStatementNoShortIf					{ $$ = (Statement *) ($1); } |
    ForStatementNoShortIf					{ $$ = (Statement *) ($1); } ;

StatementWithoutTrailingSubstatement:
    //FieldDeclaration      { $$ = (Statement *) ($1); } |
    Block					{ $$ = (Statement *) ($1); } |
    //EmptyStatement		{ $$ = (Statement *) ($1); } |
    ExpressionStatement		{ $$ = (Statement *) ($1); } |
    DoStatement				{ $$ = (Statement *) ($1); } |
    ReturnStatement			{ $$ = (Statement *) ($1); } |
    ThrowStatement			{ $$ = (Statement *) ($1); } |
    TryStatement			{ $$ = (Statement *) ($1); } ;

ExpressionStatement:
    StatementExpression SEMICOLON { $$ = (Statement *) ($1); } ;

StatementExpression:
    Expression                      { $$ = (Statement *)(new sStatementExpression((Expression *)($1)));} ;
    //PreIncrementExpression          { $$ = (Statement *)(new sStatementExpression((Expression *)($1)));} |
    //PreDecrementExpression          { $$ = (Statement *)(new sStatementExpression((Expression *)($1)));} |
    //PostIncrementExpression         { $$ = (Statement *)(new sStatementExpression((Expression *)($1)));} |
    //PostDecrementExpression         { $$ = (Statement *)(new sStatementExpression((Expression *)($1)));} |
    //MethodInvocation                { $$ = (Statement *)(new sStatementExpression((Expression *)($1)));} |
    //ClassInstanceCreationExpression { $$ = (Statement *)(new sStatementExpression((Expression *)($1)));} ;

IfThenStatement:
    TIF '(' Expression ')' TTHEN Statement  { $$ = (Statement *) new IfStatement($3, $6, NULL); } |
    TIF '(' Expression ')' Statement		{ $$ = (Statement *) new IfStatement($3, $5, NULL); } ;

IfThenElseStatement:
    TIF '(' Expression ')' TTHEN StatementNoShortIf TELSE Statement	{ $$ = (Statement *) new IfStatement($3, $6, $8); } |
    TIF '(' Expression ')' StatementNoShortIf TELSE Statement		{ $$ = (Statement *) new IfStatement($3, $5, $7); } ;

IfThenElseStatementNoShortIf:
    TIF '(' Expression ')' TTHEN StatementNoShortIf TELSE StatementNoShortIf	{ $$ = (Statement *) new IfStatement($3, $6, $8); } |
    TIF '(' Expression ')' StatementNoShortIf TELSE StatementNoShortIf			{ $$ = (Statement *) new IfStatement($3, $5, $7); } ;

WhileStatement:
    TWHILE '(' Expression ')' Statement { $$ = (Statement *) new sWhileStatement($3, $5); } ;

WhileStatementNoShortIf:
    TWHILE '(' Expression ')' StatementNoShortIf { $$ = (Statement *) new sWhileStatement($3, $5); } ;

DoStatement:
    TDO Statement TWHILE '(' Expression ')' SEMICOLON { $$ = (Statement *) ( new sDoStatement($2, $5) ); } ;

ForStatement:
    TFOR '(' Name LTMINUS Expression TUNTIL Expression ')' Statement { $$ = new ForStatement($3,   $5,   $7,   $9); } ;

ForStatementNoShortIf:
	TFOR '(' Name LTMINUS Expression TUNTIL Expression ')' StatementNoShortIf { $$ = new ForStatement($3,   $5,   $7,   $9); } ;

ForInit:
    StatementExpressionList  { $$ = new sForInit($1, NULL, NULL, NULL); } |
    VariableDeclaratorId { $$ = new sForInit(NULL, $1, NULL, NULL); } |
    VariableDeclaratorId Type { $$ = new sForInit(NULL, $1, $2, NULL); } |
    VariableDeclaratorId Type ASSIGNING VariableInitializer { $$ = new sForInit(NULL, $1, $2, $4); };

ForUpdate:
    StatementExpressionList { $$ = new sForUpdate($1); } ;

StatementExpressionList:
    StatementExpression	{ $$ = new sStatementExpressionList( (sStatementExpression *) ($1) ); } |
    StatementExpressionList COMMA StatementExpression { $$ = sStatementExpressionList::append( $1, (sStatementExpression *) ($3) ); } ;

ReturnStatement:
    TRETURN Expression SEMICOLON	{ $$ = (Statement *) ( new sReturnStatement( $2 ) ); } |
    TRETURN SEMICOLON				{ $$ = (Statement *) ( new sReturnStatement(NULL) ); } ;

ThrowStatement:
    TTHROW Expression SEMICOLON { $$ = (Statement *) ( new sThrowStatement($2) ); } ;

TryStatement:
    TTRY Block Catches			{ $$ = (Statement *) ( new sTryStatement($2, $3,   NULL) ); } |
    TTRY Block Catches Finally  { $$ = (Statement *) ( new sTryStatement($2, $3,   $4  ) ); } |
    TTRY Block Finally			{ $$ = (Statement *) ( new sTryStatement($2, NULL, $3  ) ); } ;

Catches:
    CatchClause  { $$ = new sCatches($1); } | Catches CatchClause { $$ = sCatches::append($1, $2); } ;

CatchClause:
    TCATCH '(' FormalParameter ')' Block { $$ = new sCatchClause($3, $5); } ;

Finally:
    FINALLY Block { $$ = (Statement *) new sFinally($2); } ;

Primary:
    PrimaryNoNewArray       { $$ = $1; } |
    ArrayCreationExpression { $$ = $1; } ;

PrimaryNoNewArray:
    Literal                         { $$ = (Expression *) ($1);} |
    TTHIS                           { $$ = (Expression *) (new sThis());} |
    '(' Expression ')'              { $$ = $2;} |
    ClassInstanceCreationExpression { $$ = (Expression *) ($1);} |
    FieldAccess                     { $$ = (Expression *) ($1);} |
    //Name '(' TINTLITERAL ')'         { $$ = (Expression *) ($1);} |
    MethodInvocation                { $$ = (Expression *) ($1);} ;

/*ArrayAccess:
    Name '(' Expression ')';*/

ClassInstanceCreationExpression:
    TNEW Name '(' ArgumentList ')'  { $$ = new sClassInstanceCreationExpression($2, $4  ); } |
    TNEW Name '(' ')'               { $$ = new sClassInstanceCreationExpression($2, NULL); } ;

ArgumentList:
    Expression  { $$ = new sArgumentList($1); } |
    ArgumentList COMMA Expression { $$ = sArgumentList::append($1, $3); } ;

ArrayCreationExpression:
    TNEW TARRAY '[' PrimitiveType ']' '(' ')'	{ $$ = (Expression *) ( new sArrayCreationExpression($4, NULL) ); } |
    TNEW TARRAY '[' Name ']' '(' ')'			{ $$ = (Expression *) ( new sArrayCreationExpression(NULL, $4) ); } ;

FieldAccess:
    Primary POINT Name  { $$ = new sFieldAccess( $1, $3, false); } |
    SUPER POINT Name { $$ = new sFieldAccess(NULL, $3, true); } |
    Name POINT Name { $$ = new sFieldAccess($1, $3, true); } ;

MethodInvocation:
    Name '(' ArgumentList ')'  { $$ = new sMethodInvocation($1,$3,NULL); } | /* name -> Name */
    Name '(' ')'  { $$ = new sMethodInvocation($1, NULL, NULL);} |
    Primary POINT Name '(' ArgumentList ')'  { $$ = new sMethodInvocation($3, $5, $1); } |
    Primary POINT Name '(' ')' { $$ = new sMethodInvocation($3, NULL, $1); } |
    Name POINT Name '(' ArgumentList ')'  { $$ = new sMethodInvocation($3, $5, $1); } |
    Name POINT Name '(' ')' { $$ = new sMethodInvocation($3, NULL, $1); } ;

PostfixExpression:
    Primary  { $$ = $1; } |
    Name  { $$ = (Expression *) ($1);} |
    PostIncrementExpression  { $$ = $1; } |
    PostDecrementExpression { $$ = $1; } ;

PostIncrementExpression:
    PostfixExpression PLUSPLUS { $$ = new sPostIncrementExpression($1); } ;

PostDecrementExpression:
    PostfixExpression MINUSMINUS { $$ = new sPostDecrementExpression($1); } ;

UnaryExpression:
    PreIncrementExpression  { $$ = $1;} |
    PreDecrementExpression  { $$ = $1;} |
    PLUS UnaryExpression  { $$ = new sUnaryExpression(NULL, NULL, $2, NULL, NULL);} |
    MINUS UnaryExpression  { $$ = new sUnaryExpression(NULL, NULL, NULL, $2, NULL);} |
    UnaryExpressionNotPlusMinus { $$ = $1;} ;

PreIncrementExpression:
    PLUSPLUS UnaryExpression { $$ = new sPreIncrementExpression($2); } ;

PreDecrementExpression:
    MINUSMINUS UnaryExpression { $$ = new sPreDecrementExpression($2); } ;

UnaryExpressionNotPlusMinus:
    PostfixExpression  { $$ = $1; } |
    TILDE UnaryExpression  { $$ = new sUnaryExpressionNotPlusMinus(NULL, $2, NULL, NULL); } |
    NOT UnaryExpression  { $$ = new sUnaryExpressionNotPlusMinus(NULL, NULL, $2, NULL); } ;

MultiplicativeExpression:
    UnaryExpression                                 { $$ = $1;} |
    MultiplicativeExpression TIMES UnaryExpression  { $$ = new sMultiplicativeExpression($3, $1,   TIMES_MULTIPLICATE_EXPRESSION);} |
    MultiplicativeExpression DIV UnaryExpression    { $$ = new sMultiplicativeExpression($3, $1,   DIV_MULTIPLICATE_EXPRESSION  );} |
    MultiplicativeExpression XOR UnaryExpression    { $$ = new sMultiplicativeExpression($3, $1,   XOR_MULTIPLICATE_EXPRESSION  );} ;

AdditiveExpression:
    MultiplicativeExpression                            { $$ = $1;} |
    AdditiveExpression PLUS MultiplicativeExpression    { $$ = new sAdditiveExpression($3, $1,   PLUS_ADDITIVE_EXPRESSION );} |
    AdditiveExpression MINUS MultiplicativeExpression   { $$ = new sAdditiveExpression($3, $1,   MINUS_ADDITIVE_EXPRESSION);} ;

ShiftExpression:
    AdditiveExpression                          { $$ = $1;} |
    ShiftExpression LTLT AdditiveExpression     { $$ = new sShiftExpression($3, $1,   LTLT_SHIFT_EXPRESSION  );} |
    ShiftExpression GTGT AdditiveExpression     { $$ = new sShiftExpression($3, $1,   GTGT_SHIFT_EXPRESSION  );} |
    ShiftExpression GTGTGT AdditiveExpression   { $$ = new sShiftExpression($3, $1,   GTGTGT_SHIFT_EXPRESSION);} ;

RelationalExpression:
    ShiftExpression                             { $$ = $1;} |
    RelationalExpression LT ShiftExpression     { $$ = new sRelationalExpression($3, $1,   LT_RELATIONAL_EXPRESSION   );} |
    RelationalExpression GT ShiftExpression     { $$ = new sRelationalExpression($3, $1,   GT_RELATIONAL_EXPRESSION   );} |
    RelationalExpression LTEQ ShiftExpression   { $$ = new sRelationalExpression($3, $1,   LTEQ_RELATIONAL_EXPRESSION );} |
    RelationalExpression GTEQ ShiftExpression   { $$ = new sRelationalExpression($3, $1,   GTEQ_RELATIONAL_EXPRESSION );} ;

EqualityExpression:
    RelationalExpression                            { $$ = $1;} |
    EqualityExpression EQEQ RelationalExpression    { $$ = new sEqualityExpression($3, $1,   EQEQ_EQUALITY_EXPRESSION );} |
    EqualityExpression NOTEQ RelationalExpression   { $$ = new sEqualityExpression($3, $1,   NOTEQ_EQUALITY_EXPRESSION);} ;

AndExpression:
    EqualityExpression                      { $$ = $1;} |
    AndExpression AND EqualityExpression    { $$ = new sAndExpression($3, $1  );} ;

ExclusiveOrExpression:
    AndExpression  { $$ = $1; } |
    ExclusiveOrExpression CIRCUMFLEX AndExpression { $$ = new sExclusiveOrExpression($3, $1);} ;

InclusiveOrExpression:
    ExclusiveOrExpression  { $$ = $1; } |
    InclusiveOrExpression OR ExclusiveOrExpression { $$ = new sInclusiveOrExpression($3, $1); } ;

ConditionalAndExpression:
    InclusiveOrExpression  { $$ = $1; } |
    ConditionalAndExpression ANDAND InclusiveOrExpression { $$ = new sConditionalAndExpression($3, $1); } ;

ConditionalOrExpression:
    ConditionalAndExpression  { $$ = $1; } |
    ConditionalOrExpression OROR ConditionalAndExpression { $$ = new sConditionalOrExpression($3, $1); } ;

ConditionalExpression:
    ConditionalOrExpression  { $$ = $1; } |
    ConditionalOrExpression QUESTION Expression COLON ConditionalExpression { $$ = new sConditionalExpression($1, $3, $5);} ; // ИЗМЕНИЛ ТИПЫ ПАРАМЕТРОВ

AssignmentExpression:
    ConditionalExpression   { $$ = $1;} |
    Assignment              { $$ = $1;} ;

Assignment:
    LeftHandSide AssignmentOperator AssignmentExpression { $$ = ((sAssignmentOperator *)($2))->setOperands($1, $3); } ;

LeftHandSide:
    Name        { $$ = $1 } |
    MethodInvocation { $$ = $1 } |
    FieldAccess { $$ = $1 };

AssignmentOperator:
    ASSIGNING       { $$ = new sAssignmentOperator(ASSIGNING_ASSIGNMENT_OPERATOR   );} |
    TIMESEQ         { $$ = new sAssignmentOperator(TIMESEQ_ASSIGNMENT_OPERATOR     );} |
    DIVEQ           { $$ = new sAssignmentOperator(DIVEQ_ASSIGNMENT_OPERATOR       );} |
    XOREQ           { $$ = new sAssignmentOperator(XOREQ_ASSIGNMENT_OPERATOR       );} |
    PLUSEQ          { $$ = new sAssignmentOperator(PLUSEQ_ASSIGNMENT_OPERATOR      );} |
    MINUSEQ         { $$ = new sAssignmentOperator(MINUSEQ_ASSIGNMENT_OPERATOR     );} |
    LTLTEQ          { $$ = new sAssignmentOperator(LTLTEQ_ASSIGNMENT_OPERATOR      );} |
    GTGTEQ          { $$ = new sAssignmentOperator(GTGTEQ_ASSIGNMENT_OPERATOR      );} |
    GTGTGTEQ        { $$ = new sAssignmentOperator(GTGTGTEQ_ASSIGNMENT_OPERATOR    );} |
    ANDEQ           { $$ = new sAssignmentOperator(ANDEQ_ASSIGNMENT_OPERATOR       );} |
    OREQ            { $$ = new sAssignmentOperator(OREQ_ASSIGNMENT_OPERATOR        );} |
    CIRCUMFLEXEQ    { $$ = new sAssignmentOperator(CIRCUMFLEXEQ_ASSIGNMENT_OPERATOR);} ;

Expression:
    AssignmentExpression { $$ = $1; } ;


%%

void yyerror(char const *s)
{
    printf("%s",s);
}