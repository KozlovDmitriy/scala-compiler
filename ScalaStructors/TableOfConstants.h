#pragma once

#include "Project.h"

enum TableOfConstantsRowType {
	CONSTANT_UTF8 = 1,
	CONSTANT_INTEGER = 3,
	CONSTANT_FLOAT = 4,
	CONSTANT_LONG = 5,
	CONSTANT_DOUBLE = 6,
	CONSTANT_CLASS = 7,
	CONSTANT_STRING = 8,
	CONSTANT_FIELDREF = 9,
	CONSTANT_METHODREF = 10,
	CONSTANT_NAMEANDTYPE = 12
};

class TableOfConstantsRow {
private:
	int number;	
protected:	
	string value;		
	TableOfConstantsRow ( ) { }
public:	
	TableOfConstantsRowType type;
	TableOfConstantsRow (
			TableOfConstantsRowType _type,
			string _value
		) : type (_type),
			value (_value) {
		this->number = 0;
	}
	int getNumber () {
		return number;
	}
	void setNumber (int value) {
		this->number = value;
	}
	string getValue () {
		return value;
	}
	void setValue ( string _value ) {
		this->value = _value;
	}
	string getType() {
		switch (type) {
			case CONSTANT_UTF8:			return string("UTF-8");
			case CONSTANT_INTEGER:		return string("Integer");
			case CONSTANT_FLOAT:		return string("Float");
			case CONSTANT_CLASS:		return string("Class");
			case CONSTANT_STRING:		return string("String");
			case CONSTANT_FIELDREF:		return string("FieldRef");
			case CONSTANT_METHODREF:	return string("MethodRef");
			case CONSTANT_NAMEANDTYPE:	return string("Name&Type");
			case CONSTANT_LONG:			return string("Long");
			case CONSTANT_DOUBLE:		return string("Double");
			default:					return string("???");
		}
	}

	virtual string toString() {
		return itoa( this->getNumber(), new char[50], 10 ) + 
			   string( "\t\t" ) + 
			   this->getType() + 
			   string( "\t\t" ) +
			   this->getValue();
	}

	void generateByteCode( BinaryWriter * writer );
};

enum UTF8Type {
	STRING_UTF8TYPE = 0,
	HANDLER_UTF8TYPE = 1,
	METHOD_HANDLER_UTF8TYPE = 2
};

class UTF8Row : public TableOfConstantsRow {
protected:	
	UTF8Type utf8_type;
	UTF8Row () {}
public:
	UTF8Row ( string _value ) : TableOfConstantsRow ( CONSTANT_UTF8, _value ) {	
		this->utf8_type = STRING_UTF8TYPE;
	}
	bool isHandler() {
		return this->utf8_type == HANDLER_UTF8TYPE;
	}
	bool isMethodHandler() {
		return this->utf8_type == METHOD_HANDLER_UTF8TYPE;
	}
};

// ���������� �������� ����������
class HandlerRow : public UTF8Row {
public:
	HandlerRow ( 
			bool _is_array, 
			bool _is_object_type, 
			string _name_of_type 
		) : is_array (_is_array), 
			is_object_type (_is_object_type),
			name_of_type (_name_of_type) {
		this->type = CONSTANT_UTF8;
		this->utf8_type = HANDLER_UTF8TYPE;
		this->value = name_of_type;
		if ( _is_object_type ) {
			this->value = "L" + this->value + ";";
		}
		if ( _is_array ) {
			this->value = "[" + this->value;
		}
	}

	bool isArray() {
		return this->is_array;
	}

	bool isObjectType() {
		return this->is_object_type;
	}

	HandlerRow * toArray() {
		if ( !this->is_array ) {
			return new HandlerRow(true, this->is_object_type, this->name_of_type);
		}
		return this;
	}

	string getNameOfType() {
		return this->name_of_type;
	}

	bool isPrimitiveTypeCast( string f, string s ) {
		const int CASTS_SIZE = 3;
		string casts[CASTS_SIZE][2] = { ("C","I"), ("C","F"), ("I","F") };
		for ( int i = 0; i < CASTS_SIZE; ++i ) {
			if ( casts[i][0] == f && casts[i][1] == s ) {
				return true;
			}
		}
		return false;
	}

	bool isCast( HandlerRow * other ) {
		if ( other->isObjectType() == this->isObjectType() ) {
			if ( other->isArray() == this->isArray() ) {
				if ( other->getNameOfType() == this->getNameOfType() ) {
					return true;
				} else {
					if ( other->isArray() == true && this->isArray() == true ) {
						return false;
					}
					if ( other->isArray() == false && this->isArray() == false && 
						 isPrimitiveTypeCast( other->getNameOfType(), this->getNameOfType() ) ) {
						return true;
					}
				}
			}
		} 
		return false;
	}
private:
	 bool is_array;
	 bool is_object_type;
	 string name_of_type;
};

// ���������� ������
class MethodHandlerRow : public UTF8Row {
public:
	MethodHandlerRow (
			vector<HandlerRow *> _arguments,
			HandlerRow * _return_handler
		) : arguments (_arguments),
			return_handler (_return_handler) {
		this->type = CONSTANT_UTF8;
		this->utf8_type = METHOD_HANDLER_UTF8TYPE;
		this->value = this->getHandlerValue();
	}

	void addArgument( HandlerRow * row ) {
		this->arguments.push_back( row );
		this->value = this->getHandlerValue();
	}

	vector<HandlerRow *> * getArguments() {
		return & this->arguments;
	}

	HandlerRow * getReturnHandler() {
		return this->return_handler;
	}

	bool isCast( MethodHandlerRow * other ) {
		if ( ! this->getReturnHandler()->isCast( other->getReturnHandler() ) ) {
			return false;
		}
		vector<HandlerRow *> * other_args = other->getArguments();
		if ( other_args->size() != this->arguments.size() ) {
			return false;
		}
		for ( int i = 0; i < this->arguments.size(); ++i) {
			if ( ! this->arguments[i]->isCast( other_args->at(i) ) ) {
				return false;
			}
		}
		return true;
	}

private:
	vector<HandlerRow *> arguments;
	HandlerRow * return_handler;

	string getHandlerValue() {
		string ret = "(";
		for ( HandlerRow * arg : arguments ) {
			ret += arg->getValue();
		}
		ret += ")" + return_handler->getValue();
		return ret;
	}
};

class StringRow : public TableOfConstantsRow {
public:
	StringRow ( UTF8Row * _utf8 ) : utf8 (_utf8) {
		type = CONSTANT_STRING;
		value = string( itoa( _utf8->getNumber(), new char[50], 10 ) );
	}
	int getUtf8Number() {
		return utf8->getNumber();
	}

	string toString() override {
		return itoa( this->getNumber(), new char[50], 10 ) + 
			   string( "\t\t" ) + 
			   this->getType() + 
			   string( "\t\t->" ) +
			   std::to_string( this->getUtf8Number() );
	}
private:
	UTF8Row * utf8;
};

class IntegerRow : public TableOfConstantsRow {
public:
	IntegerRow ( int _value	) : int_value (_value) {
		type = CONSTANT_INTEGER;
		value = string( itoa( _value, new char[50], 10 ) );
	}
	int getIntValue () {
		return this->int_value;
	}
private:
	int int_value;
};

class LongRow : public TableOfConstantsRow {
public:
	LongRow ( long long _value	) : long_value (_value) {
		type = CONSTANT_LONG;
		value = string( itoa( _value, new char[50], 10 ) );
	}
	
	long long * getLongValue() {
		return & this->long_value;
	}
private:
	long long long_value;
};

class FloatRow : public TableOfConstantsRow {
public:
	FloatRow ( float _value	) : float_value (_value) {
		type = CONSTANT_FLOAT;
		ostringstream buff;
		buff << float_value;
		value = buff.str() ;
	}
	float getFloatValue() {
		return this->float_value;
	}
private:
	float float_value;
};

class DoubleRow : public TableOfConstantsRow {
public:
	DoubleRow ( double _value	) : double_value (_value) {
		type = CONSTANT_DOUBLE;
		ostringstream buff;
		buff << double_value;
		value = buff.str() ;
	}

	double * getDoubleValue() {
		return & this->double_value;
	}
private:
	double double_value;
};

class NameAndTypeRow : public TableOfConstantsRow {
public:
	NameAndTypeRow ( 
			UTF8Row * _name, 
			UTF8Row * _type 
		) : name_pointer (_name), 
			type_pointer (_type) {
		type = CONSTANT_NAMEANDTYPE;
		value = string( itoa( _name->getNumber(), new char[50], 10 ) );
		value.append( "," );
		value.append( string( itoa( _type->getNumber(), new char[50], 10 ) ) );
	}
	string GetName() {
		return this->name_pointer->getValue();
	}
	string GetType() {
		return this->type_pointer->getValue();
	}
	int getNameNumber() {
		return this->name_pointer->getNumber();
	}
	int getTypeNumber() {
		return this->type_pointer->getNumber();
	}	
	string toString() override {
		return itoa( this->getNumber(), new char[50], 10 ) + 
			   string( "\t\t" ) + 
			   this->getType() + 
			   string( "\t\t->" ) +
			   std::to_string( this->getNameNumber() ) +
			   string( ",->" ) +
			   std::to_string( this->getTypeNumber() );
	}
private:
	UTF8Row * name_pointer;
	UTF8Row * type_pointer;
};

class ClassRow : public TableOfConstantsRow {
public:
	ClassRow ( UTF8Row * _name ) : name_pointer (_name) {
		type = CONSTANT_CLASS;
		value = string( itoa( _name->getNumber(), new char[50], 10 ) );
	}
	string getClassName() {
		return this->name_pointer->getValue();
	}
	int getUtf8Number() {
		return name_pointer->getNumber();
	}
	string toString() override {
		return itoa( this->getNumber(), new char[50], 10 ) + 
			   string( "\t\t" ) + 
			   this->getType() + 
			   string( "\t\t->" ) +
			   std::to_string( this->getUtf8Number() );
	}
private:
	UTF8Row * name_pointer;
};

class MethodRefRow : public TableOfConstantsRow {
public:	
	sMethodDeclaration * abstract_node;
	ClassRow * class_pointer;
	NameAndTypeRow * name_and_type_pointer;
	MethodRefRow ( 
			ClassRow * _class,
			NameAndTypeRow * _name_and_type,
			sMethodDeclaration * _abstract_node
		) : class_pointer (_class),
			name_and_type_pointer (_name_and_type),
			abstract_node (_abstract_node) {
		type = CONSTANT_METHODREF;
		value = string( itoa( _class->getNumber(), new char[50], 10 ) );
		value.append( "," );
		value.append( string( itoa( _name_and_type->getNumber(), new char[50], 10 ) ) );
	}
	int getClassNumber();
	int getNameAndTypeNumber();
	void generateMethodTableByteCode( BinaryWriter * writer );
	string toString() override {
		return itoa( this->getNumber(), new char[50], 10 ) + 
			   string( "\t\t" ) + 
			   this->getType() + 
			   string( "\t\t->" ) +
			   std::to_string( this->getClassNumber() ) +
			   string( ",->" ) +
			   std::to_string( this->getNameAndTypeNumber() );
	}
};

class FieldRefRow : public TableOfConstantsRow {
public:
	FieldRefRow ( 
			ClassRow * _class,
			NameAndTypeRow * _name_and_type,
			sFieldDeclaration * _abstract_node
		) : class_pointer (_class),
			name_and_type_pointer (_name_and_type),
			abstract_node (_abstract_node) {
		type = CONSTANT_FIELDREF;
		value = string( itoa( _class->getNumber(), new char[50], 10 ) );
		value.append( "," );
		value.append( string( itoa( _name_and_type->getNumber(), new char[50], 10 ) ) );
	}
	int getClassNumber();
	int getNameAndTypeNumber();
	void generateFieldTableByteCode( BinaryWriter * writer );
	string toString() override {
		return itoa( this->getNumber(), new char[50], 10 ) + 
			   string( "\t\t" ) + 
			   this->getType() + 
			   string( "\t\t->" ) +
			   std::to_string( this->getClassNumber() ) +
			   string( ",->" ) +
			   std::to_string( this->getNameAndTypeNumber() );
	}
private:
	sFieldDeclaration * abstract_node;
	ClassRow * class_pointer;
	NameAndTypeRow * name_and_type_pointer;
};

class TableOfConstants {
public:
	TableOfConstants() {
		this->name_and_type_count = 0;
		this->field_ref_count = 0;
		this->method_ref_count = 0;
		this->name = "";
		this->addRow( new UTF8Row("Code") );
	}

	vector<TableOfConstantsRow *> *getTable () {
		return &table;
	}
	
	void addRow (TableOfConstantsRow * row) {
		if ( row != NULL ) {
			switch ( row->type ) {
				case CONSTANT_FIELDREF: ++this->field_ref_count; break;
				case CONSTANT_METHODREF: ++this->method_ref_count; break;
				case CONSTANT_NAMEANDTYPE: ++this->name_and_type_count; break;
			}
			row->setNumber( this->table.size() + 1 );
			table.push_back( row );
		}
	}

	TableOfConstantsRow * getRow ( int index ) {
		return (this->table.size() > index) ? this->table[index] : NULL;
	}

	string getStringTable() {
		string ret = "";
		for (int i = 0; i < table.size(); ++i) {
			ret += table[i]->toString() + "\n";
		}
		return ret;
	}

	void setName( string value ) {
		this->name = value;
	}

	string getName() {
		return this->name;
	}
	
	string getVariableType( string variable ) {
		for (int i = 0; i < table.size(); ++i ) {
			if ( table[i]->type == CONSTANT_NAMEANDTYPE ) {
				if ( ( (NameAndTypeRow *) table[i] )->GetName() == variable ) {
					return string( ( (NameAndTypeRow *) table[i] )->GetType() );
				}
			}
		}
		return string();
	}

	static bool isArray(string type) {
		if (type.size() != 0) {
			if (type[0] == '[') {
				return true;
			}
		}
		return false;
	}

	bool isVariableArray( string variable ) {
		string _type = this->getVariableType( variable );
		if (_type.size()!=0) {
			if ( (_type)[0] == '[' ) {
				return true;
			}
		}
		return false;
	}

	void clearTable() {
		this->table.clear();
	}

	void operator << ( TableOfConstants table ) {
		for ( int i = 1; i < table.getTable()->size(); ++i ) {
			this->addRow( table.getTable()->at( i ) );
		}
	}

	void generateByteCode( BinaryWriter * writer ) {
		writer->writeU2(this->table.size() + 1);
		for ( TableOfConstantsRow * row : this->table ) {
			row->generateByteCode( writer );
		}
	}

	int getFieldRefCount() {
		return this->field_ref_count;
	}

	int getMethodRefCount() {
		return this->method_ref_count;
	}

	int getNameAndTypeCount() {
		return this->name_and_type_count;
	}
protected:
	int name_and_type_count;
	int field_ref_count;
	int method_ref_count;
	vector<TableOfConstantsRow *> table;
	string name;
};


class TableOfLocalConstants : public TableOfConstants {
public:
	TableOfLocalConstants() {
		table.clear();
	}
};
