#pragma once 

using namespace std;

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <cstdlib>
void DIE(string initiator, string msg);
void DIE(string cpp_type, string initiator, string msg, string stage);

class Error {
public:
	string cpp_type;
	string initiator;
	string msg;
	string stage;
	Error (
			string _cpp_type,
			string _initiator,
			string _msg,
			string _stage
		) : cpp_type(_cpp_type),
			initiator(_initiator),
			msg(_msg),
			stage(_stage) {
	}
};
#include "attrib.h"
#include "IdManager.h"
#include "ByteCode.h"
#include "ScalaEnums.h"
#include "BinaryWriter.h"
#include "TableOfConstants.h"
#include "Node.h"
#include "Expressions.h"
#include "Statements.h"
#include "AbstractTree.h"

