#include "Project.h"

sMethodInvocation::sMethodInvocation(
		sName * _methodname,
		sArgumentList * _argument_list,
		Expression * _source
	) : methodname(_methodname),
		argument_list(_argument_list),
		source(_source) 
{
	this->cpp_type = "sMethodInvocation";
	bool selfmethod = false;
	string todotlabel = "Method Invocation";
	/*if (this->canBeArrayAccess()) {
		todotlabel += "\nor array access";
	}*/
	if (source != NULL) {
		todotlabel += "\\n(other class)";
	}
	else if (methodname != NULL) {
		todotlabel += "\\n(inner)";
	}
	this->setDotLabel(todotlabel);
	this->addChildren((Node *)source, "source");
	this->addChildren((Node *)methodname, "methodname");
	this->addChildren((Node *)argument_list, "argument_list");
}

bool sMethodInvocation::canBeArrayAccess() {
	Node * method = this->findParentOfType(new string("sMethodDeclaration"));
	if (method != NULL ) {
		sMethodDeclaration * smethod = static_cast<sMethodDeclaration *>(method);
		if (smethod == NULL) {
			DIE("sMethodInvocation::canBeArrayAccess", "smethod==null");
		}
		string ttype = findType(*(methodname->id),false);
		return TableOfConstants::isArray(ttype);
	}
	return false;
}

void sMethodInvocation::transformToArrayAccess() {
	if (this->canBeArrayAccess()) {
		Node * parent = this->getParent();
		fakeArrayAccess * newnode = new fakeArrayAccess(methodname, argument_list->expressions[0], source);
		parent->replaceChildren(this, newnode);
	}
}

void sMethodInvocation::calcAttr() {
	// SUPAMAGIK WILL BE HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! TODO
}