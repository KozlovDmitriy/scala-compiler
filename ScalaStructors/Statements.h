#pragma once
#include "Project.h"

class sBlockStatement : public Statement {
private:
	eBlockStatement type;
	sVariableDeclaratorId * variable_declaration_id;
	sType * s_type;
	sVariableInitializer * variable_initializer;
	Statement * statement;
public:
	sBlockStatement * next;
	sBlockStatement (
			eBlockStatement _type,
			sVariableDeclaratorId * _variable_declaration_id,
			sType * _s_type,
			sVariableInitializer * _variable_initializer,
			Statement * _statement
		) :	type (_type),
			variable_declaration_id (_variable_declaration_id),
			s_type (_s_type),
			variable_initializer (_variable_initializer),
			statement (_statement) {
		this->cpp_type = "sBlockStatement";
		this->addChildren( (Node *) variable_declaration_id , "variable_declaration_id");
		this->addChildren( (Node *) s_type , "s_type");
		this->addChildren( (Node *) variable_initializer , "variable_initializer");
		this->addChildren( (Node *) statement , "statement");
		next = NULL;
		this->setDotLabel( "Block Statement" );
	}
	sVariableDeclaratorId * getVariableDeclaratorId( ) {
		return this->variable_declaration_id;
	}
	sType * getType( ) {
		return this->s_type;
	}
	bool isVar( ) {
		return this->type == LOCAL_VARIBLE_DECLARATION;
	}
	virtual void calcAttr() override {
		if (isVar())
		{
			string varname = string(*(static_cast<sName *>(getChild("variable_declaration_id")->getChild("id"))->id));
			string type = this->findType(varname, false);
			if (type.size() > 0) {
				throwError(string("sBlockStatement::calcAttr"),
					string("var with name ")
					+ varname
					+ string(" already exists")
					);
			}
		}
	}
};

class sBlockStatements : public Statement {
private:
	sBlockStatement * begin;
	sBlockStatement * end;
public:
	sBlockStatements (
			sBlockStatement * _block_statement
		) : begin (_block_statement),
			end (_block_statement) {
		this->addChildren( (Node *) begin , "begin");
		this->setDotLabel( "Block Statements" );
	}
	static sBlockStatements * append (
			sBlockStatements * _block_statements,
			sBlockStatement * _block_statement
		) {
		_block_statements->end->next = _block_statement;
		( (Node *)(_block_statements) )->addChildren( (Node *) _block_statements->end->next , "next");
		_block_statements->end = _block_statement;
		( (Node *)(_block_statements) )->rewriteEnd( (Node *) _block_statement );
		return _block_statements;
	}
};

class StatementExpressions : public Statement {
public:
	virtual vector<Expression *> * getExpression() {
		return &(this->exprs);
	}
	virtual void addExpression(Expression * value) {
		this->exprs.push_back(value);
		this->addChildren(value, "value");
	}
private:
	vector<Expression *> exprs;
};

class FieldDefenition : public Statement {};

class MethodDefenition : public Statement {};

class ClassDefinition : public Statement {
public:
	virtual void addMethodDefenition(MethodDefenition * method_defenition) {
		this->method_defenitions.push_back( method_defenition );
	}
	virtual void addFieldDefenition(FieldDefenition * field_defenition) {
		this->field_defenitions.push_back( field_defenition );
	}
private:
	vector<MethodDefenition *> method_defenitions;
	vector<FieldDefenition *> field_defenitions;
};

class IfStatement : public Statement {
private:
	Expression * condition;
	Statement * if_stmt;
	Statement * else_stmt;
public:
	IfStatement (
			Expression * _condition,
			Statement * _if_stmt,
			Statement * _else_stmt
		) : condition (_condition),
			if_stmt (_if_stmt),
			else_stmt (_else_stmt) {
		this->addChildren( (Node *) condition , "condition");
		this->addChildren( (Node *) if_stmt , "if_stmt");
		this->addChildren( (Node *) else_stmt , "else_stmt");
		this->setDotLabel( "IF Statement" );
	}
	void setConditin(Expression * value) {
		this->condition = value;
	}
	Expression * getCondition() {
		return this->condition;
	}
	void setIfStmt(Statement * value) {
		this->if_stmt = value;
	}
	Statement * getIfStmt() {
		return this->if_stmt;
	}
	void setElseStmt(Statement * value) {
		this->else_stmt = value;
	}
	Statement * getElseStmt() {
		return this->else_stmt;
	}
};

class ForStatement : public Statement {
private:
	sName * first_arg;
	Expression * second_arg;
	Expression * third_arg;
	Statement * block;
public:
	ForStatement (
			sName * _first_arg,
			Expression * _second_arg,
			Expression * _third_arg,
			Statement * _block
		) : first_arg (_first_arg),
			second_arg (_second_arg),
			third_arg (_third_arg),
			block (_block) {
		this->addChildren( (Node *) first_arg , "first_arg");
		this->addChildren( (Node *) second_arg , "second_arg");
		this->addChildren( (Node *) third_arg , "third_arg");
		this->addChildren( (Node *) block , "block");
		this->setDotLabel( "FOR Statement" );
	}
	void setFirstArg(sName * value) {
		this->first_arg = value;
	}
	sName * getFirstAgr() {
		return this->first_arg;
	}
	void setSecondArg(Expression * value) {
		this->second_arg = value;
	}
	Expression * getSecondAgr() {
		return this->second_arg;
	}
	void setThirdArg(Expression * value) {
		this->third_arg = value;
	}
	Expression * getThirdArg() {
		return this->third_arg;
	}
	void setBlock(Statement * value) {
		this->block = value;
	}
	Statement * getBlock() {
		return this->block;
	}
};

class sDoStatement : public Statement {
public:
	Statement * statement;
	Expression * expression;
	sDoStatement (
			Statement * _statement,
			Expression * _expression
		) : statement (_statement),
			expression (_expression) {
		this->addChildren( (Node *) statement , "statement");
		this->addChildren( (Node *) expression , "expression");
		this->setDotLabel( "Do Statement" );
	}
};

class sReturnStatement : public Statement {
public:
	Expression * expression;
	sReturnStatement (
			Expression * _expression
		) : expression (_expression) {
		this->addChildren( (Node *) expression , "expression");
		this->setDotLabel( "Return Statement" );
	}
};

class sWhileStatement : public Statement {
public:
	Expression * expression;
	Statement * statement;
	sWhileStatement (
			Expression * _expression,
			Statement * _statement
		) : expression (_expression),
			statement (_statement) {
		this->addChildren( (Node *) expression , "expression");
		this->addChildren( (Node *) statement , "statement");
		this->setDotLabel( "While Statement" );
	}
};

class sThrowStatement : public Statement {
public:
	Expression * expression;
	sThrowStatement (
			Expression * _expression
		) : expression (_expression) {
		this->addChildren( (Node *) expression , "expression");
		this->setDotLabel( "Throw Statement" );
	}
};

class sTryStatement : public Statement {
public:
	Statement * block;
	sCatches * catches;
	Statement * finally_value;
	sTryStatement (
			Statement * _block,
			sCatches * _catches,
			Statement * _finally_value
		) : block (_block),
			catches (_catches),
			finally_value (_finally_value) {
		this->addChildren( (Node *) block , "block");
		this->addChildren( (Node *) catches , "catches");
		this->addChildren( (Node *) finally_value , "finally_value");
		this->setDotLabel( "Try Statement" );
	}
};

class sStatementExpression : public Statement {
public:
	Expression * value;
	sStatementExpression * next;
	sStatementExpression ( Expression * _value ) : value (_value) {
		this->addChildren( (Node *) value , "value");
		next = NULL;
		this->setDotLabel( "Statement Expression" );
	}
};

class sStatementExpressionList : public Statement {
public:
	sStatementExpression * begin;
	sStatementExpression * end;
	sStatementExpressionList (
			sStatementExpression * _statement_expression
		) : begin (_statement_expression),
			end (_statement_expression) {
		this->addChildren( (Node *) begin , "begin");
		this->setDotLabel( "Statement Expression List" );
	}
	static sStatementExpressionList * append (
			sStatementExpressionList * _statement_expression_list,
			sStatementExpression * _statement_expression
		) {
		_statement_expression_list->end->next = _statement_expression;
		( (Node *)(_statement_expression_list) )->addChildren( (Node *) _statement_expression_list->end->next , "next");
		_statement_expression_list->end = _statement_expression;
		( (Node *)(_statement_expression_list) )->rewriteEnd( (Node *) _statement_expression );
		return _statement_expression_list;
	}
};