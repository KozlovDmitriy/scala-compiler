#pragma once

#include "Project.h"
#include <iostream>
#include <string>
#include <vector>
using namespace std;
extern IdManager id_manager;

class Node
{
public:
	Node() {	
		this->transformed = false;
		this->cpp_type = "Node";
		this->parent = NULL;
		this->table_of_constants_row = NULL;
		this->constants_ref = NULL;
		this->node_id = id_manager.getNextId();
		hide = false;
		corrupted = false;
		error = false;
	}

	~Node() {
		for (int i = 0; i < this->childrens.size(); ++i) {
			if ( childrens[i] != NULL) {
				delete childrens[i];
				childrens[i] = NULL;
			}
		}
	}

	Node* findParentOfType( string* _type ) {
		if ( this->getParent() != NULL ) {
			if ( this->getParent()->cpp_type == *_type ) {
				return this->getParent();
			} else {
				return this->getParent()->findParentOfType( _type );
			}
		} else {
			return NULL;
		}
		delete _type;
	}

	vector<Node*> findAllChildrensOfType( string* _type ) {
		vector<Node*> ret;
		if ( this->cpp_type == *_type ) {
			ret.push_back( this );
		}
		for ( int i = 0; i < childrens.size(); ++i ) {
			vector<Node*> children_ret = childrens[i]->findAllChildrensOfType( _type );
			for ( int i = 0; i < children_ret.size(); ++i ) {
				ret.push_back( children_ret[i] );
			}
		}
		return ret;
	}

	void setParent(Node * _parent) {
		this->parent = _parent;
	}

	Node * getParent() {
		return this->parent;
	}

	Node * getChild(string name) {
		for (int i = 0; i < childrenNames.size(); ++i) {
			if (childrenNames[i]==name) {
				return childrens[i];
			}
		}
		DIE("Node::getChild", string("there is no child with name ")+name);
		return NULL;
	}

	void setChild(string name, Node * newnode) {
		if (newnode != NULL) {
			for (int i = 0; i < childrenNames.size(); ++i) {
				if (childrenNames[i] == name) {
					childrens[i] = newnode; // TODO - memory leak!!!
				}
			}
		}
	}

	void removeChild(string name) {
		for (int i = 0; i < childrenNames.size(); ++i) {
			if (childrenNames[i] == name) {
				childrens.erase(childrens.begin() + i);
				childrenNames.erase(childrenNames.begin() + i);
			}
		}
	}

	void replaceChildren(Node * oldnode, Node * newnode) {
		for (int i = 0; i < childrens.size(); ++i) {
			if (childrens[i]->id() == oldnode->id()) {
				if (newnode != NULL) {
					childrens[i] = newnode;
					newnode->setParent(this);
				}
			}
		}
		oldnode->corrupt();
	}

	void addChildren(Node * children, string edgeDotLabel) {
		if (children != NULL) {
			childrens.push_back(children);
			childrenNames.push_back(edgeDotLabel);
			children->setParent(this);
		}
	}

	void rewriteEnd(Node * children) {
		childrens[childrens.size() - 1] = children;
	}

	vector< Node * > getChildrens() {
		return this->childrens;
	}

	unsigned int getId() {
		return this->node_id;
	}

	void setDotLabel (string label) {
		this->dot_label = label;
	}

	bool isHide() {
		return this->hide;
	}

	void hidden() {
		this->hide = true;
	}

	void corrupt() {
		corrupted = true;
		childrens.clear();
		childrenNames.clear();
	}

	TableOfConstants * getConstantsTable();

	string getDotScript() {
		string dot_script = "";
		dot_script.append("edge");
		dot_script.append( string( itoa( this->node_id, new char[50], 10 ) ) );
		dot_script.append(" [label=\"" );
		dot_script.append( dot_label );
		if (this->table_of_constants_row) {
			dot_script.append("\\n");
			dot_script.append(this->table_of_constants_row->getValue());
		}
		if (this->constants_ref) {
			dot_script.append("\\n");
			dot_script.append(this->constants_ref->getValue());
		}
		dot_script.append( "\",shape=box");
		if (this->error) 
		{
			dot_script.append(",color=red");
		}
		else if (this->transformed) 
		{
			dot_script.append(",color=blue");
		}
		if (this->attr.is_static) 
		{
			dot_script.append(",style=dashed");
		}
		dot_script.append("]\n");
		//this->hidden();
		for ( int i = 0; i < childrens.size(); ++i ) {
			dot_script.append("edge");
			dot_script.append( string( itoa( this->node_id, new char[50], 10 ) ) );
			dot_script.append( " -> " );
			dot_script.append("edge");
			dot_script.append( string( itoa( childrens[i]->getId(), new char[50], 10 ) ) );
			dot_script.append( "[label=\"" );
			dot_script.append(childrenNames[i]);
			dot_script.append("\",fontsize=8");
			dot_script.append("]\n");
			if ( !childrens[i]->isHide() ) {
				dot_script.append( childrens[i]->getDotScript() );
			}
		}
		return dot_script;
	}
	
	virtual TableOfConstantsRow * getTableOfConstantsRow() {
		return NULL;
	}
	
	virtual vector<unsigned char> * generateByteCode( BinaryWriter * writer ) {
		return NULL;
	}

	unsigned int id() {
		return node_id;
	}

	void attribution(bool forcestatic = false);

	virtual void calcAttr() {
		return;
	}

	void throwError(string initiator, string msg, string stage) {
		this->error = true;
		throw new Error(this->cpp_type, initiator, msg, stage);
	}

	void throwError(string initiator, string msg) {
		throwError(initiator, msg, string("?"));
	}

	string findType(string, bool);

	bool error;
	bool corrupted;
	bool transformed;
	string cpp_type;
	Attr attr;
	TableOfConstantsRow * constants_ref;
protected:
	bool hide;
	unsigned int node_id;
	vector< Node * > childrens;
	vector< string > childrenNames; // aka children name
	Node * parent;
	string dot_label;
	TableOfConstantsRow * table_of_constants_row;
};

class Expression : public Node {
public :
	Expression () : Node() {}
};

class Statement : public Node {
public :
	Statement () : Node() {}
};