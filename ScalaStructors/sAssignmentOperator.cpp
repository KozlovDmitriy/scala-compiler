#include "Project.h"

sAssignmentOperator::sAssignmentOperator(eAssignmentOperator _type) : type(_type) {
	string str = "";
	switch (_type) {
	case ASSIGNING_ASSIGNMENT_OPERATOR:
		str = "=";
		break;
	case TIMESEQ_ASSIGNMENT_OPERATOR:
		str = "*=";
		break;
	case DIVEQ_ASSIGNMENT_OPERATOR:
		str = "\\=";
		break;
	case XOREQ_ASSIGNMENT_OPERATOR:
		str = "%=";
		break;
	case PLUSEQ_ASSIGNMENT_OPERATOR:
		str = "+=";
		break;
	case MINUSEQ_ASSIGNMENT_OPERATOR:
		str = "-=";
		break;
	case LTLTEQ_ASSIGNMENT_OPERATOR:
		str = "<<=";
		break;
	case GTGTEQ_ASSIGNMENT_OPERATOR:
		str = ">>=";
		break;
	case GTGTGTEQ_ASSIGNMENT_OPERATOR:
		str = ">>>=";
		break;
	case ANDEQ_ASSIGNMENT_OPERATOR:
		str = "&=";
		break;
	case OREQ_ASSIGNMENT_OPERATOR:
		str = "|=";
		break;
	case CIRCUMFLEXEQ_ASSIGNMENT_OPERATOR:
		str = "^=";
		break;
	}
	this->setDotLabel(str);
	this->cpp_type = "sAssignmentOperator";
}

sAssignmentOperator * sAssignmentOperator::setOperands(
	Expression * _left_hand_side,
	Expression * _assigment_expression
	) 
{
	this->left_hand_side = _left_hand_side;
	this->addChildren((Node *)left_hand_side, "left_hand_side");
	this->assigment_expression = _assigment_expression;
	this->addChildren((Node *)assigment_expression, "assigment_expression");
	return this;
}

void sAssignmentOperator::transformToArrayAssignment() {
	Node * n = this->getChild("left_hand_side");
	if (n->cpp_type == "fakeArrayAccess") {
		fakeArrayAccess * aa = static_cast<fakeArrayAccess *>(n);
		fakeArrayAssignment * newnode = new fakeArrayAssignment(this->type,
			aa->arrayname,
			aa->argument,
			aa->source,
			this->assigment_expression
			);
		this->parent->replaceChildren(this, newnode);
	}
}