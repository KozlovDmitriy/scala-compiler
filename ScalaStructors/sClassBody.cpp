#include "Project.h"

TableOfConstantsRow * sClassBody::getTableOfConstantsRow()  {
	if ( this->table_of_constants_row == NULL ) {
		if ( class_body_declarations != NULL ) {
			class_body_declarations->getTableOfConstantsRow();
		}
	}
	return this->table_of_constants_row;
}