#include "Project.h"

TableOfConstantsRow * sMethodDeclaration::getTableOfConstantsRow() {
	if ( this->table_of_constants_row == NULL ) {
		NameAndTypeRow * _name_and_type = (NameAndTypeRow *) this->method_header->getTableOfConstantsRow();
		sClassDeclaration * _class_decl = (sClassDeclaration *) this->findParentOfType( & string("sTypeDeclaration") );
		ClassRow * _class = (ClassRow *) _class_decl->getTableOfConstantsRow();
		this->table_of_constants_row = new MethodRefRow(_class, _name_and_type, this);
		this->getConstantsTable()->addRow( this->table_of_constants_row );
	}
	return this->table_of_constants_row;
}

void sMethodDeclaration::fillLocalConstants(sBlockStatement * cur_block_stmt, bool includelast) {
	bool fl = false;
	this->local_constants->clearTable();
	vector<Node *> block_stmts = this->findAllChildrensOfType(&string("sBlockStatement"));
	for (int i = 0; i < block_stmts.size(); ++i) {
		sBlockStatement * block_stmt = (sBlockStatement *)block_stmts[i];
		if ((includelast && !fl && block_stmt->isVar()) || (block_stmt != cur_block_stmt && !fl && block_stmt->isVar()))
		{
			if (block_stmt == cur_block_stmt) {
				fl = true;
			}
			UTF8Row * _name = (UTF8Row *)block_stmt->getVariableDeclaratorId()->getTableOfConstantsRow();
			local_constants->addRow(_name);
			UTF8Row * _type = (UTF8Row *)block_stmt->getType()->getTableOfConstantsRow();
			local_constants->addRow(_type);
			local_constants->addRow(new NameAndTypeRow(_name, _type));
			local_constants->setName(*this->method_header->method_declarator->id->id);
		}
		else
		{
			fl = true;
		}
	}
}

vector<unsigned char> * sMethodDeclaration::generateByteCode( BinaryWriter * writer ) {
	vector<unsigned char> * byte_code = this->block->generateByteCode( NULL );
	if ( byte_code == NULL ) {
		byte_code = new vector<unsigned char>();
	}
	const unsigned long EMPTY_CODE_ATTR_LENGTH = 18;
	// Write the attribute length.
    writer->writeU4( byte_code->size() + EMPTY_CODE_ATTR_LENGTH - 6 ); // excluding first 6 bytes.
    // Write the operands stack size.
    writer->writeU2(STACK_SIZE);
    // Write the local vars table size.
	writer->writeU2( this->local_constants->getNameAndTypeCount() );
    // Write the byteCode size.
    writer->writeU4( byte_code->size() );
    // Write the byteCode itself.
    writer->writeByteArray( *byte_code );
    // Write the exceptions table size (0), skip the table itself.
    writer->writeU2(0);
    // Write number of attributes of "Code" (0), skip them too.
    writer->writeU2(0);
	return NULL;
}