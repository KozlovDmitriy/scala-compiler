
/*  A Bison parser, made from bscala.bison with Bison version GNU Bison version 1.24
  */

#define YYBISON 1  /* Identify Bison output.  */

#define	TDEF	258
#define	TIMPORT	259
#define	TPACKAGE	260
#define	TIF	261
#define	TTHEN	262
#define	TELSE	263
#define	TWHILE	264
#define	TFOR	265
#define	TDO	266
#define	TBOOLEAN	267
#define	TINT	268
#define	TDOUBLE	269
#define	TBYTE	270
#define	TSHORT	271
#define	TCHAR	272
#define	TLONG	273
#define	TFLOAT	274
#define	UNIT	275
#define	VAL	276
#define	WITH	277
#define	TTYPE	278
#define	VAR	279
#define	YIELD	280
#define	TRETURN	281
#define	TTRUE	282
#define	TFALSE	283
#define	TNULL	284
#define	TTHIS	285
#define	SUPER	286
#define	STRING_TCLASS	287
#define	ARRAY_TCLASS	288
#define	TPRIVATE	289
#define	TPROTECTED	290
#define	OVERRIDE	291
#define	TABSTRACT	292
#define	FINAL	293
#define	SEALED	294
#define	TTHROW	295
#define	TTRY	296
#define	TCATCH	297
#define	FINALLY	298
#define	TRAIT	299
#define	TCLASS	300
#define	TCASE	301
#define	TFORSOME	302
#define	IMPLICIT	303
#define	LAZY	304
#define	MATCH	305
#define	TNEW	306
#define	TDOTTDOTTDOT	307
#define	EXTENDS	308
#define	ANYVAR	309
#define	TOBJECT	310
#define	TBOOLEAN_JT	311
#define	TBYTE_JT	312
#define	TCHARACTER_JT	313
#define	TDOUBLE_JT	314
#define	TFLOAT_JT	315
#define	TINTEGER_JT	316
#define	TOBJECT_JT	317
#define	TSHORT_JT	318
#define	VOID_JT	319
#define	TCLASS_JT	320
#define	NUMBER_JT	321
#define	TPACKAGE_JT	322
#define	STRINGBUFFER_JT	323
#define	STRINGBUILDER_JT	324
#define	TCHARSEQUENCE_JT	325
#define	THREAD_JT	326
#define	TTHROWABLE_JI	327
#define	CLONABLE_JI	328
#define	COMPARABLE_JI	329
#define	SERIALIZABLE_JI	330
#define	RUNNABLE_JI	331
#define	SEMICOLON	332
#define	COMMA	333
#define	POINT	334
#define	ASSIGNING	335
#define	GT	336
#define	LT	337
#define	NOT	338
#define	TILDE	339
#define	QUESTION	340
#define	COLON	341
#define	EQEQ	342
#define	LTEQ	343
#define	GTEQ	344
#define	COMPILES	345
#define	NOTEQ	346
#define	ANDAND	347
#define	OROR	348
#define	PLUSPLUS	349
#define	MINUSMINUS	350
#define	PLUS	351
#define	MINUS	352
#define	TIMES	353
#define	DIV	354
#define	AND	355
#define	OR	356
#define	CIRCUMFLEX	357
#define	XOR	358
#define	LTLT	359
#define	GTGT	360
#define	GTGTGT	361
#define	PLUSEQ	362
#define	MINUSEQ	363
#define	TIMESEQ	364
#define	DIVEQ	365
#define	ANDEQ	366
#define	OREQ	367
#define	CIRCUMFLEXEQ	368
#define	XOREQ	369
#define	LTLTEQ	370
#define	GTGTEQ	371
#define	GTCOLON	372
#define	LTCOLON	373
#define	LTDIV	374
#define	LTMINUS	375
#define	COLONCOLONCOLON	376
#define	GTGTGTEQ	377
#define	ID	378
#define	STRINGLITERAL	379
#define	TCHARLITERAL	380
#define	TINTLITERAL	381
#define	TLONGLITERAL	382
#define	TFLOATLITERAL	383
#define	TDOUBLELITERAL	384
#define	TPUBLIC	385
#define	TSTATIC	386
#define	TARRAY	387
#define	TUNTIL	388

#line 1 "bscala.bison"

	#include "Project.h"

    extern int yylex(void);
    /*extern enum error_types errorCode;
    extern char errorMessage[256];*/

    void yyerror(const char * str);

    sProgrammStatement * root;

#line 185 "bscala.bison"
typedef union {
    char * char_value;
    double * double_value;
    float * float_value;
    long long * long_value;
    long * int_value;
    string * string_value;
    sType * sType_value;
    sPrimitiveType * sPrimitiveType_value;
    sReferenceType * sReferenceType_value;
    sArrayType * sArrayType_value;
    sExpression * sExpression_value;
    sAssignmentExpression * sAssignmentExpression_value;
    sAssignmentOperator * sAssignmentOperator_value;
    sConditionalExpression * sConditionalExpression_value;
    sConditionalOrExpression * sConditionalOrExpression_value;
    sConditionalAndExpression * sConditionalAndExpression_value;
    sInclusiveOrExpression * sInclusiveOrExpression_value;
    sExclusiveOrExpression * sExclusiveOrExpression_value;
    sAndExpression * sAndExpression_value;
    sEqualityExpression * sEqualityExpression_value;
    sRelationalExpression * sRelationalExpression_value;
    sShiftExpression * sShiftExpression_value;
    sAdditiveExpression * sAdditiveExpression_value;
    sMultiplicativeExpression * sMultiplicativeExpression_value;
    sUnaryExpression * sUnaryExpression_value;
    sPreIncrementExpression * sPreIncrementExpression_value;
    sPreDecrementExpression * sPreDecrementExpression_value;
    sUnaryExpressionNotPlusMinus * sUnaryExpressionNotPlusMinus_value;
    sPostfixExpression * sPostfixExpression_value;
    sPrimary * sPrimary_value;
    sArrayCreationExpression * sArrayCreationExpression_value;
    sPostIncrementExpression * sPostIncrementExpression_value;
    sPostDecrementExpression * sPostDecrementExpression_value;
    sAssignment * sAssignment_value;
    sLeftHandSide * sLeftHandSide_value;
    sFieldAccess * sFieldAccess_value;
    sArrayAccess * sArrayAccess_value;
    sPrimaryNoNewArray * sPrimaryNoNewArray_value;
    sLiteral * sLiteral_value;
    sBooleanLiteral * sBooleanLiteral_value;
    sClassInstanceCreationExpression * sClassInstanceCreationExpression_value;
    sMethodInvocation * sMethodInvocation_value;
    sArgumentList * sArgumentList_value;
    Statement * sStatementWithoutTrailingSubstatement_value;
    sExpressionStatement * sExpressionStatement_value;
    sStatementExpression * sStatementExpression_value;
    sDoStatement * sDoStatement_value;
    sReturnStatement * sReturnStatement_value;
    sThrowStatement * sThrowStatement_value;
    sTryStatement * sTryStatement_value;
    sCatches * sCatches_value;
    sCatchClause * sCatchClause_value;
    sFinally * sFinally_value;
    sIfThenStatement * sIfThenStatement_value;
    sIfThenElseStatement * sIfThenElseStatement_value;
    Statement * sStatementNoShortIf_value;
    sIfThenElseStatementNoShortIf * sIfThenElseStatementNoShortIf_value;
    sWhileStatementNoShortIf * sWhileStatementNoShortIf_value;
    sForStatementNoShortIf * sForStatementNoShortIf_value;
    sForInit * sForInit_value;
    sStatementExpressionList * sStatementExpressionList_value;
    sForUpdate * sForUpdate_value;
    sWhileStatement * sWhileStatement_value;
    sProgramm * sProgramm_value;
    sName * sName_value;
    sTypeDeclarations * sTypeDeclarations_value;
    sTypeDeclaration * sTypeDeclaration_value;
    sClassDeclaration * sClassDeclaration_value;
    sSingletonDeclaration * sSingletonDeclaration_value;
    sClassBody * sClassBody_value;
    sClassBodyDeclarations * sClassBodyDeclarations_value;
    sClassBodyDeclaration * sClassBodyDeclaration_value;
    sConstructorDeclaration * sConstructorDeclaration_value;
    sConstructorDeclarator * sConstructorDeclarator_value;
    sFormalParameterList * sFormalParameterList_value;
    sFormalParameter * sFormalParameter_value;
    sVariableDeclaratorId * sVariableDeclaratorId_value;
    sConstructorBody * sConstructorBody_value;
    sExplicitConstructorInvocation * sExplicitConstructorInvocation_value;
    sBlockStatements * sBlockStatements_value;
    sBlockStatement * sBlockStatement_value;
    sVariableInitializer * sVariableInitializer_value;
    sArrayInitializer * sArrayInitializer_value;
    sVariableInitializers * sVariableInitializers_value;
    sClassMemberDeclaration * sClassMemberDeclaration_value;
    sFieldDeclaration * sFieldDeclaration_value;
    sMethodDeclaration * sMethodDeclaration_value;
    sMethodHeader * sMethodHeader_value;
    sMethodDeclarator * sMethodDeclarator_value;
    sMethodType * sMethodType_value;
    Statement * sBlock_value;
    sThis * sThis_value;
    Expression * Expression_value;
	Statement * Statement_value;
    IfStatement * IfStatement_value;
	ForStatement * ForStatement_value;
} YYSTYPE;

#ifndef YYLTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YYLTYPE yyltype
#endif

#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		387
#define	YYFLAG		-32768
#define	YYNTBASE	140

#define YYTRANSLATE(x) ((unsigned)(x) <= 388 ? yytranslate[x] : 223)

static const short yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,   139,
   134,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
   135,     2,   136,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,   137,     2,   138,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
    26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
    36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
    46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
    56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
    66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
    76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
    86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
    96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
   106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
   116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
   126,   127,   128,   129,   130,   131,   132,   133
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     2,     3,     5,     8,    10,    12,    14,    16,    18,
    20,    22,    24,    26,    28,    30,    32,    34,    37,    39,
    41,    44,    46,    48,    50,    52,    54,    56,    58,    60,
    63,    66,    71,    76,    80,    84,    88,    91,    93,    96,
    98,   100,   102,   104,   108,   114,   117,   120,   122,   124,
   127,   130,   134,   136,   142,   147,   149,   153,   156,   159,
   164,   168,   173,   177,   181,   184,   190,   195,   201,   206,
   214,   222,   229,   236,   238,   242,   246,   249,   251,   254,
   258,   264,   266,   268,   270,   272,   274,   276,   278,   280,
   282,   284,   286,   288,   290,   292,   294,   296,   299,   301,
   308,   314,   323,   331,   340,   348,   354,   360,   368,   378,
   388,   390,   392,   395,   400,   402,   404,   408,   412,   415,
   419,   423,   428,   432,   434,   437,   443,   446,   448,   450,
   452,   454,   458,   460,   462,   464,   470,   475,   477,   481,
   489,   497,   501,   505,   509,   514,   518,   525,   531,   538,
   544,   546,   548,   550,   552,   555,   558,   560,   562,   565,
   568,   570,   573,   576,   578,   581,   584,   586,   590,   594,
   598,   600,   604,   608,   610,   614,   618,   622,   624,   628,
   632,   636,   640,   642,   646,   650,   652,   656,   658,   662,
   664,   668,   670,   674,   676,   680,   682,   688,   690,   692,
   696,   698,   700,   702,   704,   706,   708,   710,   712,   714,
   716,   718,   720,   722,   724,   726
};

static const short yyrhs[] = {   141,
     0,     0,   142,     0,   141,   142,     0,   151,     0,   152,
     0,   123,     0,    27,     0,    28,     0,   126,     0,   128,
     0,   144,     0,   125,     0,   124,     0,    29,     0,   127,
     0,   129,     0,    86,   148,     0,   149,     0,   146,     0,
    86,    20,     0,    13,     0,    18,     0,    15,     0,    19,
     0,    14,     0,    12,     0,    17,     0,    32,     0,    86,
   143,     0,    86,   150,     0,   132,   135,   148,   136,     0,
   132,   135,   143,   136,     0,    45,   143,   153,     0,    55,
   143,   153,     0,   137,   154,   138,     0,   137,   138,     0,
   155,     0,   154,   155,     0,   156,     0,   165,     0,   157,
     0,   160,     0,   158,   146,    77,     0,   158,   146,    80,
   159,    77,     0,    24,   143,     0,    21,   143,     0,   222,
     0,   169,     0,   161,   171,     0,   161,    77,     0,   162,
   147,    80,     0,   162,     0,     3,   143,   139,   163,   134,
     0,     3,   143,   139,   134,     0,   164,     0,   163,    78,
   164,     0,   143,   146,     0,   166,   167,     0,   143,   139,
   163,   134,     0,   143,   139,   134,     0,   137,   168,   172,
   138,     0,   137,   168,   138,     0,   137,   172,   138,     0,
   137,   138,     0,    30,   139,   196,   134,    77,     0,    30,
   139,   134,    77,     0,    31,   139,   196,   134,    77,     0,
    31,   139,   134,    77,     0,   132,   135,   148,   136,   139,
   170,   134,     0,   132,   135,   143,   136,   139,   170,   134,
     0,   132,   135,   148,   136,   139,   134,     0,   132,   135,
   143,   136,   139,   134,     0,   159,     0,   170,    78,   159,
     0,   137,   172,   138,     0,   137,   138,     0,   173,     0,
   172,   173,     0,   158,   146,    77,     0,   158,   146,    80,
   159,    77,     0,   174,     0,   176,     0,   179,     0,   180,
     0,   182,     0,   185,     0,   176,     0,   181,     0,   183,
     0,   186,     0,   171,     0,   177,     0,   184,     0,   187,
     0,   188,     0,   189,     0,   178,    77,     0,   222,     0,
     6,   139,   222,   134,     7,   174,     0,     6,   139,   222,
   134,   174,     0,     6,   139,   222,   134,     7,   175,     8,
   174,     0,     6,   139,   222,   134,   175,     8,   174,     0,
     6,   139,   222,   134,     7,   175,     8,   175,     0,     6,
   139,   222,   134,   175,     8,   175,     0,     9,   139,   222,
   134,   174,     0,     9,   139,   222,   134,   175,     0,    11,
   174,     9,   139,   222,   134,    77,     0,    10,   139,   143,
   120,   222,   133,   222,   134,   174,     0,    10,   139,   143,
   120,   222,   133,   222,   134,   175,     0,     0,     0,   158,
     0,   158,   146,     0,   158,   146,    80,   159,     0,     0,
     0,   178,     0,     0,    78,   178,     0,    26,   222,    77,
     0,    26,    77,     0,    40,   222,    77,     0,    41,   171,
   190,     0,    41,   171,   190,   192,     0,    41,   171,   192,
     0,   191,     0,   190,   191,     0,    42,   139,   164,   134,
   171,     0,    43,   171,     0,   194,     0,   197,     0,   145,
     0,    30,     0,   139,   222,   134,     0,   195,     0,   198,
     0,   199,     0,    51,   143,   139,   196,   134,     0,    51,
   143,   139,   134,     0,   222,     0,   196,    78,   222,     0,
    51,   132,   135,   148,   136,   139,   134,     0,    51,   132,
   135,   143,   136,   139,   134,     0,   193,    79,   143,     0,
    31,    79,   143,     0,   143,    79,   143,     0,   143,   139,
   196,   134,     0,   143,   139,   134,     0,   193,    79,   143,
   139,   196,   134,     0,   193,    79,   143,   139,   134,     0,
   143,    79,   143,   139,   196,   134,     0,   143,    79,   143,
   139,   134,     0,   193,     0,   143,     0,   201,     0,   202,
     0,   200,    94,     0,   200,    95,     0,   204,     0,   205,
     0,    96,   203,     0,    97,   203,     0,   206,     0,    94,
   203,     0,    95,   203,     0,   200,     0,    84,   203,     0,
    83,   203,     0,   203,     0,   207,    98,   203,     0,   207,
    99,   203,     0,   207,   103,   203,     0,   207,     0,   208,
    96,   207,     0,   208,    97,   207,     0,   208,     0,   209,
   104,   208,     0,   209,   105,   208,     0,   209,   106,   208,
     0,   209,     0,   210,    82,   209,     0,   210,    81,   209,
     0,   210,    88,   209,     0,   210,    89,   209,     0,   210,
     0,   211,    87,   210,     0,   211,    91,   210,     0,   211,
     0,   212,   100,   211,     0,   212,     0,   213,   102,   212,
     0,   213,     0,   214,   101,   213,     0,   214,     0,   215,
    92,   214,     0,   215,     0,   216,    93,   215,     0,   216,
     0,   216,    85,   222,    86,   217,     0,   217,     0,   219,
     0,   220,   221,   218,     0,   143,     0,   199,     0,   198,
     0,    80,     0,   109,     0,   110,     0,   114,     0,   107,
     0,   108,     0,   115,     0,   116,     0,   122,     0,   111,
     0,   112,     0,   113,     0,   218,     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
   381,   382,   385,   386,   389,   390,   393,   397,   398,   401,
   402,   403,   404,   405,   406,   407,   408,   411,   412,   415,
   416,   419,   420,   421,   422,   423,   424,   425,   426,   429,
   430,   433,   434,   437,   441,   445,   446,   449,   450,   453,
   455,   458,   459,   462,   463,   466,   467,   470,   471,   474,
   476,   480,   482,   485,   486,   489,   490,   493,   497,   501,
   502,   505,   506,   507,   508,   511,   512,   513,   514,   517,
   518,   519,   520,   523,   524,   527,   528,   531,   532,   535,
   536,   537,   540,   541,   542,   543,   544,   547,   548,   549,
   550,   553,   555,   557,   558,   559,   560,   563,   566,   575,
   576,   579,   580,   583,   584,   587,   590,   593,   596,   599,
   602,   603,   604,   605,   608,   611,   612,   615,   616,   619,
   622,   623,   624,   627,   628,   630,   633,   636,   637,   640,
   641,   642,   643,   644,   645,   652,   653,   656,   657,   660,
   661,   664,   665,   666,   669,   670,   671,   672,   673,   674,
   677,   678,   679,   680,   683,   686,   689,   690,   691,   692,
   693,   696,   699,   702,   703,   704,   707,   708,   709,   710,
   713,   714,   715,   718,   719,   720,   721,   724,   725,   726,
   727,   728,   731,   732,   733,   736,   737,   740,   741,   744,
   745,   748,   749,   752,   753,   756,   757,   760,   761,   764,
   767,   768,   769,   772,   773,   774,   775,   776,   777,   778,
   779,   780,   781,   782,   783,   786
};

static const char * const yytname[] = {   "$","error","$undefined.","TDEF","TIMPORT",
"TPACKAGE","TIF","TTHEN","TELSE","TWHILE","TFOR","TDO","TBOOLEAN","TINT","TDOUBLE",
"TBYTE","TSHORT","TCHAR","TLONG","TFLOAT","UNIT","VAL","WITH","TTYPE","VAR",
"YIELD","TRETURN","TTRUE","TFALSE","TNULL","TTHIS","SUPER","STRING_TCLASS","ARRAY_TCLASS",
"TPRIVATE","TPROTECTED","OVERRIDE","TABSTRACT","FINAL","SEALED","TTHROW","TTRY",
"TCATCH","FINALLY","TRAIT","TCLASS","TCASE","TFORSOME","IMPLICIT","LAZY","MATCH",
"TNEW","TDOTTDOTTDOT","EXTENDS","ANYVAR","TOBJECT","TBOOLEAN_JT","TBYTE_JT",
"TCHARACTER_JT","TDOUBLE_JT","TFLOAT_JT","TINTEGER_JT","TOBJECT_JT","TSHORT_JT",
"VOID_JT","TCLASS_JT","NUMBER_JT","TPACKAGE_JT","STRINGBUFFER_JT","STRINGBUILDER_JT",
"TCHARSEQUENCE_JT","THREAD_JT","TTHROWABLE_JI","CLONABLE_JI","COMPARABLE_JI",
"SERIALIZABLE_JI","RUNNABLE_JI","SEMICOLON","COMMA","POINT","ASSIGNING","GT",
"LT","NOT","TILDE","QUESTION","COLON","EQEQ","LTEQ","GTEQ","COMPILES","NOTEQ",
"ANDAND","OROR","PLUSPLUS","MINUSMINUS","PLUS","MINUS","TIMES","DIV","AND","OR",
"CIRCUMFLEX","XOR","LTLT","GTGT","GTGTGT","PLUSEQ","MINUSEQ","TIMESEQ","DIVEQ",
"ANDEQ","OREQ","CIRCUMFLEXEQ","XOREQ","LTLTEQ","GTGTEQ","GTCOLON","LTCOLON",
"LTDIV","LTMINUS","COLONCOLONCOLON","GTGTGTEQ","ID","STRINGLITERAL","TCHARLITERAL",
"TINTLITERAL","TLONGLITERAL","TFLOATLITERAL","TDOUBLELITERAL","TPUBLIC","TSTATIC",
"TARRAY","TUNTIL","')'","'['","']'","'{'","'}'","'('","ProgrammStatement","TypeDeclarations",
"TypeDeclaration","Name","BooleanLiteral","Literal","Type","MethodType","PrimitiveType",
"ReferenceType","ArrayType","ClassDeclaration","SingletonDeclaration","ClassBody",
"ClassBodyDeclarations","ClassBodyDeclaration","ClassMemberDeclaration","FieldDeclaration",
"VariableDeclaratorId","VariableInitializer","MethodDeclaration","MethodHeader",
"MethodDeclarator","FormalParameterList","FormalParameter","ConstructorDeclaration",
"ConstructorDeclarator","ConstructorBody","ExplicitConstructorInvocation","ArrayInitializer",
"VariableInitializers","Block","BlockStatements","BlockStatement","Statement",
"StatementNoShortIf","StatementWithoutTrailingSubstatement","ExpressionStatement",
"StatementExpression","IfThenStatement","IfThenElseStatement","IfThenElseStatementNoShortIf",
"WhileStatement","WhileStatementNoShortIf","DoStatement","ForStatement","ForStatementNoShortIf",
"ReturnStatement","ThrowStatement","TryStatement","Catches","CatchClause","Finally",
"Primary","PrimaryNoNewArray","ClassInstanceCreationExpression","ArgumentList",
"ArrayCreationExpression","FieldAccess","MethodInvocation","PostfixExpression",
"PostIncrementExpression","PostDecrementExpression","UnaryExpression","PreIncrementExpression",
"PreDecrementExpression","UnaryExpressionNotPlusMinus","MultiplicativeExpression",
"AdditiveExpression","ShiftExpression","RelationalExpression","EqualityExpression",
"AndExpression","ExclusiveOrExpression","InclusiveOrExpression","ConditionalAndExpression",
"ConditionalOrExpression","ConditionalExpression","AssignmentExpression","Assignment",
"LeftHandSide","AssignmentOperator","Expression","LeftHandSide"
};
#endif

static const short yyr1[] = {     0,
   140,   140,   141,   141,   142,   142,   143,   144,   144,   145,
   145,   145,   145,   145,   145,   145,   145,   146,   146,   147,
   147,   148,   148,   148,   148,   148,   148,   148,   148,   149,
   149,   150,   150,   151,   152,   153,   153,   154,   154,   155,
   155,   156,   156,   157,   157,   158,   158,   159,   159,   160,
   160,   161,   161,   162,   162,   163,   163,   164,   165,   166,
   166,   167,   167,   167,   167,   168,   168,   168,   168,   169,
   169,   169,   169,   170,   170,   171,   171,   172,   172,   173,
   173,   173,   174,   174,   174,   174,   174,   175,   175,   175,
   175,   176,   176,   176,   176,   176,   176,   177,   178,   179,
   179,   180,   180,   181,   181,   182,   183,   184,   185,   186,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,   187,   187,   188,
   189,   189,   189,   190,   190,   191,   192,   193,   193,   194,
   194,   194,   194,   194,   194,   195,   195,   196,   196,   197,
   197,   198,   198,   198,   199,   199,   199,   199,   199,   199,
   200,   200,   200,   200,   201,   202,   203,   203,   203,   203,
   203,   204,   205,   206,   206,   206,   207,   207,   207,   207,
   208,   208,   208,   209,   209,   209,   209,   210,   210,   210,
   210,   210,   211,   211,   211,   212,   212,   213,   213,   214,
   214,   215,   215,   216,   216,   217,   217,   218,   218,   219,
   220,   220,   220,   221,   221,   221,   221,   221,   221,   221,
   221,   221,   221,   221,   221,   222
};

static const short yyr2[] = {     0,
     1,     0,     1,     2,     1,     1,     1,     1,     1,     1,
     1,     1,     1,     1,     1,     1,     1,     2,     1,     1,
     2,     1,     1,     1,     1,     1,     1,     1,     1,     2,
     2,     4,     4,     3,     3,     3,     2,     1,     2,     1,
     1,     1,     1,     3,     5,     2,     2,     1,     1,     2,
     2,     3,     1,     5,     4,     1,     3,     2,     2,     4,
     3,     4,     3,     3,     2,     5,     4,     5,     4,     7,
     7,     6,     6,     1,     3,     3,     2,     1,     2,     3,
     5,     1,     1,     1,     1,     1,     1,     1,     1,     1,
     1,     1,     1,     1,     1,     1,     1,     2,     1,     6,
     5,     8,     7,     8,     7,     5,     5,     7,     9,     9,
     1,     1,     2,     4,     1,     1,     3,     3,     2,     3,
     3,     4,     3,     1,     2,     5,     2,     1,     1,     1,
     1,     3,     1,     1,     1,     5,     4,     1,     3,     7,
     7,     3,     3,     3,     4,     3,     6,     5,     6,     5,
     1,     1,     1,     1,     2,     2,     1,     1,     2,     2,
     1,     2,     2,     1,     2,     2,     1,     3,     3,     3,
     1,     3,     3,     1,     3,     3,     3,     1,     3,     3,
     3,     3,     1,     3,     3,     1,     3,     1,     3,     1,
     3,     1,     3,     1,     3,     1,     5,     1,     1,     3,
     1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
     1,     1,     1,     1,     1,     1
};

static const short yydefact[] = {     2,
     0,     0,     1,     3,     5,     6,     7,     0,     0,     4,
     0,    34,    35,     0,     0,     0,    37,     0,     0,    38,
    40,    42,     0,    43,     0,    53,    41,     0,     0,    47,
    46,     0,    36,    39,     0,     0,    19,    51,     0,    50,
     0,    20,     0,     0,    59,     0,    61,     0,     0,    56,
    27,    22,    26,    24,    28,    23,    25,    29,     0,    30,
    18,    31,    44,     0,     0,     0,     0,     0,     0,     8,
     9,    15,   131,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,    14,    13,    10,    16,    11,    17,    77,
     0,   152,    12,   130,     0,    92,     0,    78,    82,    83,
    93,     0,    84,    85,    86,    94,    87,    95,    96,    97,
   151,   128,   133,   129,   134,   135,   164,   153,   154,   167,
   157,   158,   161,   171,   174,   178,   183,   186,   188,   190,
   192,   194,   196,   198,   216,   199,     0,    99,    21,    52,
   131,     0,    65,     0,     0,    55,     0,    58,     0,    60,
     0,     0,     0,    49,    48,     0,     0,     0,     0,   119,
     0,     0,     0,     0,     0,     0,   152,   134,   135,   166,
   165,   162,   163,   159,   160,     0,     0,     0,     0,    76,
    79,    98,     0,   155,   156,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,   204,   208,   209,   205,   206,
   213,   214,   215,   207,   210,   211,   212,     0,     0,     0,
    63,     0,    64,    54,    57,     0,     0,     0,    45,     0,
     0,     0,     0,   118,   143,   120,     0,     0,   121,   124,
   123,     0,     0,   132,   144,   146,     0,   138,    80,     0,
   142,   168,   169,   170,   172,   173,   175,   176,   177,   180,
   179,   181,   182,   184,   185,   187,   189,   191,   193,     0,
   195,   200,     0,     0,     0,     0,    62,    33,    32,     0,
     0,     0,     0,     0,     0,     0,   127,   125,   122,     0,
     0,   137,     0,     0,     0,   145,     0,     0,     0,    67,
     0,    69,     0,     0,     0,     0,     0,     0,     0,   101,
     0,    83,    89,    90,    91,   106,     0,     0,     0,     0,
     0,   136,   150,     0,   139,    81,   148,     0,   197,    66,
    68,     0,     0,     0,   100,     0,     0,     0,     0,     0,
     0,     0,     0,     0,   149,   147,    73,    74,     0,    72,
     0,     0,     0,     0,     0,   103,     0,   108,   126,   141,
   140,     0,    71,    70,     0,   102,     0,     0,     0,    75,
     0,     0,   107,     0,   109,     0,     0,     0,     0,   105,
     0,   104,     0,   110,     0,     0,     0
};

static const short yydefgoto[] = {   385,
     3,     4,    92,    93,    94,    36,    43,    61,    37,    62,
     5,     6,    12,    19,    20,    21,    22,    95,   348,    24,
    25,    26,    49,    50,    27,    28,    45,   144,   154,   349,
    96,    97,    98,    99,   311,   100,   101,   102,   103,   104,
   313,   105,   314,   106,   107,   315,   108,   109,   110,   239,
   240,   241,   111,   112,   113,   247,   114,   115,   116,   117,
   118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
   128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
   218,   138
};

static const short yypact[] = {   -33,
   -95,   -95,   -33,-32768,-32768,-32768,-32768,   -87,   -87,-32768,
     2,-32768,-32768,   -95,   -95,   -95,-32768,   -84,     6,-32768,
-32768,-32768,   -13,-32768,   -69,     0,-32768,   -45,   -34,-32768,
-32768,   -10,-32768,-32768,    52,    58,-32768,-32768,   371,-32768,
    24,-32768,    29,   450,-32768,    -6,-32768,   -13,   -59,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,   -24,-32768,
-32768,-32768,-32768,  1145,    -5,    -3,    10,   927,  1170,-32768,
-32768,-32768,-32768,    85,  1488,    23,   -11,  1488,  1488,  1488,
  1488,  1488,  1488,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
  1488,   151,-32768,-32768,   -13,-32768,   511,-32768,-32768,-32768,
-32768,    91,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
    95,-32768,-32768,-32768,  1097,  1273,    59,-32768,-32768,-32768,
-32768,-32768,-32768,    34,   117,    67,    81,   -71,    83,    79,
   105,   119,   -61,-32768,-32768,-32768,  1421,-32768,-32768,-32768,
    70,   -54,-32768,   590,   651,-32768,   -43,-32768,   -95,-32768,
    82,    88,   159,-32768,-32768,  1488,  1488,   -95,   228,-32768,
   161,   -95,   162,   183,   114,   101,   -46,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,   116,   -95,  1195,    78,-32768,
-32768,-32768,   -95,-32768,-32768,  1488,  1488,  1488,  1488,  1488,
  1488,  1488,  1488,  1488,  1488,  1488,  1488,  1488,  1488,  1488,
  1488,  1488,  1488,  1488,  1488,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,  1488,  1231,  1310,
-32768,   730,-32768,-32768,-32768,   115,   118,    82,-32768,   122,
   123,   132,   129,-32768,-32768,-32768,   130,    23,   183,-32768,
-32768,    82,  1346,-32768,   133,-32768,   -31,-32768,-32768,  1145,
   136,-32768,-32768,-32768,    34,    34,   117,   117,   117,    67,
    67,    67,    67,    81,    81,   -71,    83,    79,   105,   167,
   119,-32768,   199,   -26,   200,   -15,-32768,-32768,-32768,   142,
   143,   789,   927,  1488,  1488,   -95,-32768,-32768,-32768,   144,
   145,-32768,    -4,  1371,  1488,-32768,   205,  1430,  1488,-32768,
   206,-32768,   207,   147,   148,   150,  1006,   152,   153,-32768,
   285,   287,-32768,-32768,-32768,-32768,   163,   169,   171,   158,
   160,-32768,-32768,     5,-32768,-32768,-32768,     9,-32768,-32768,
-32768,  1031,  1056,  1488,-32768,   299,  1488,   -95,   927,  1488,
   231,    23,   175,   176,-32768,-32768,-32768,-32768,    11,-32768,
    12,   177,   927,   179,   196,-32768,   186,-32768,-32768,-32768,
-32768,  1145,-32768,-32768,   868,-32768,  1006,  1488,   927,-32768,
  1006,   309,-32768,   188,-32768,   320,  1006,  1488,  1006,-32768,
   195,-32768,  1006,-32768,   330,   331,-32768
};

static const short yypgoto[] = {-32768,
-32768,   329,    -1,-32768,-32768,     3,-32768,  -140,-32768,-32768,
-32768,-32768,   324,-32768,   315,-32768,-32768,   140,   -47,-32768,
-32768,-32768,   292,  -145,-32768,-32768,-32768,-32768,-32768,     7,
   -23,   -37,   -91,   -65,  -155,  -261,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
   100,   102,-32768,-32768,-32768,  -171,-32768,   165,   244,-32768,
-32768,-32768,   -21,-32768,-32768,-32768,    43,    28,   -17,    36,
   172,   141,   146,   166,   138,-32768,    45,   155,-32768,-32768,
-32768,    51
};


#define	YYLAST		1627


static const short yytable[] = {     8,
     9,    40,   159,   225,    14,   181,   145,    38,    14,    18,
   227,     1,    29,    30,    31,   198,   153,    18,   149,   199,
   312,     2,    15,   204,   162,    16,    15,     7,    42,    16,
    48,   205,   177,    60,   149,    51,    52,    53,    54,    60,
    55,    56,    57,   139,    48,   312,   295,   274,   276,    11,
   148,   295,   164,   181,    32,    58,   170,   171,   172,   173,
   174,   175,   295,    51,    52,    53,    54,    39,    55,    56,
    57,   293,    35,   295,   150,   166,   167,   167,   167,   167,
   167,   167,   295,    58,   220,    41,   295,   281,   362,   362,
   224,    44,   178,    51,    52,    53,    54,   179,    55,    56,
    57,   291,   296,   312,    46,   312,   222,   301,   140,   312,
   151,     7,     7,    58,   155,   312,     7,   312,   303,   161,
   165,   312,   324,    47,     7,   163,   328,   146,     7,   322,
   181,   186,   187,   156,    63,   157,   188,    64,   345,    17,
   319,   176,   346,    33,   363,   364,     7,    48,   158,   226,
    23,   336,   184,   185,   249,    59,   232,   250,    23,    39,
   235,   194,   195,   162,   252,   253,   254,   182,   196,   197,
   191,   192,   193,   183,     7,   245,   260,   261,   262,   263,
   201,   251,   200,    59,   167,   167,   167,   167,   167,   167,
   167,   167,   167,   167,   167,   167,   167,   167,   167,   167,
   167,   167,   297,   167,     7,   202,   230,   231,   219,   372,
   203,   373,   189,   190,   287,   376,   310,   316,   257,   258,
   259,   380,   228,   382,   237,   238,   280,   384,   248,   177,
  -201,   255,   256,   264,   265,   229,   233,   234,   236,   243,
   290,   335,   168,   168,   168,   168,   168,   168,   242,   244,
   278,   284,   299,   279,   270,   282,   283,  -201,  -201,  -201,
  -201,  -201,  -201,  -201,  -201,  -201,  -201,   285,   286,   248,
   248,   294,  -201,   356,   298,   300,   302,   304,   305,   320,
   321,   326,   330,   331,    48,   332,   333,   366,   334,   178,
   337,   338,   339,   248,   -88,   340,   343,   167,   344,   310,
   155,   316,   341,   375,   342,   335,   353,   358,   360,   361,
   365,   356,   367,   366,   370,   368,   377,   375,   359,   369,
   378,   169,   169,   169,   169,   169,   169,   379,   383,   386,
   387,    10,    13,    34,   317,   318,   355,   147,   288,   351,
   289,   267,   271,   329,   248,   325,     0,   268,   248,     0,
   168,   168,   168,   168,   168,   168,   168,   168,   168,   168,
   168,   168,   168,   168,   168,   168,   168,   168,   269,   168,
     0,   266,   272,     0,     0,     0,    65,     0,     0,    66,
    67,    68,   155,   155,   352,     0,     0,   354,     0,     0,
   357,    15,     0,     0,    16,     0,    69,    70,    71,    72,
    73,    74,     0,     0,     0,     0,     0,     0,     0,     0,
    75,    76,   155,     0,     0,     0,     0,     0,   374,     0,
     0,    77,     0,     0,     0,     0,     0,     0,   381,   169,
   169,   169,   169,   169,   169,   169,   169,   169,   169,   169,
   169,   169,   169,   169,   169,   169,   169,     0,   169,     0,
     0,     0,     0,    78,    79,    65,     0,     0,    66,    67,
    68,     0,     0,   168,    80,    81,    82,    83,     0,     0,
    15,     0,     0,    16,     0,    69,    70,    71,    72,   141,
   142,     0,     0,     0,     0,     0,     0,     0,     0,    75,
    76,     0,     0,     7,    84,    85,    86,    87,    88,    89,
    77,     0,     0,     0,     0,     0,     0,    39,    90,    91,
     0,     0,     0,     0,     0,     0,    65,     0,     0,    66,
    67,    68,     0,     0,     0,     0,     0,     0,     0,     0,
     0,    15,    78,    79,    16,     0,    69,    70,    71,    72,
    73,    74,   169,    80,    81,    82,    83,     0,     0,     0,
    75,    76,     0,     0,     0,     0,     0,     0,     0,     0,
     0,    77,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     7,    84,    85,    86,    87,    88,    89,     0,
     0,     0,     0,     0,     0,     0,    39,   143,    91,     0,
     0,     0,     0,    78,    79,    65,     0,     0,    66,    67,
    68,     0,     0,     0,    80,    81,    82,    83,     0,     0,
    15,     0,     0,    16,     0,    69,    70,    71,    72,    73,
    74,     0,     0,     0,     0,     0,     0,     0,     0,    75,
    76,     0,     0,     7,    84,    85,    86,    87,    88,    89,
    77,     0,     0,     0,     0,     0,     0,    39,   180,    91,
     0,     0,     0,     0,     0,     0,    65,     0,     0,    66,
    67,    68,     0,     0,     0,     0,     0,     0,     0,     0,
     0,    15,    78,    79,    16,     0,    69,    70,    71,    72,
    73,    74,     0,    80,    81,    82,    83,     0,     0,     0,
    75,    76,     0,     0,     0,     0,     0,     0,     0,     0,
     0,    77,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     7,    84,    85,    86,    87,    88,    89,     0,
     0,     0,     0,     0,     0,     0,    39,   221,    91,     0,
     0,     0,     0,    78,    79,    65,     0,     0,    66,    67,
    68,     0,     0,     0,    80,    81,    82,    83,     0,     0,
    15,     0,     0,    16,     0,    69,    70,    71,    72,    73,
    74,     0,     0,     0,     0,     0,     0,     0,     0,    75,
    76,     0,     0,     7,    84,    85,    86,    87,    88,    89,
    77,     0,     0,     0,     0,     0,     0,    39,   223,    91,
     0,     0,     0,     0,   306,   307,     0,   308,   309,    68,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,    78,    79,    69,    70,    71,    72,    73,    74,
     0,     0,     0,    80,    81,    82,    83,     0,    75,    76,
     0,     0,     0,     0,     0,     0,     0,     0,     0,    77,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     7,    84,    85,    86,    87,    88,    89,     0,
     0,     0,     0,     0,     0,     0,    39,   277,    91,     0,
     0,    78,    79,   306,   371,     0,   308,   309,    68,     0,
     0,     0,    80,    81,    82,    83,     0,     0,     0,     0,
     0,     0,     0,    69,    70,    71,    72,    73,    74,     0,
     0,     0,     0,     0,     0,     0,     0,    75,    76,     0,
     0,     7,    84,    85,    86,    87,    88,    89,    77,     0,
     0,     0,     0,     0,     0,    39,     0,    91,     0,     0,
     0,     0,    65,     0,     0,    66,    67,    68,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    78,    79,    69,    70,    71,    72,    73,    74,     0,     0,
     0,    80,    81,    82,    83,     0,    75,    76,     0,     0,
     0,     0,     0,     0,     0,     0,     0,    77,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     7,    84,    85,    86,    87,    88,    89,     0,     0,     0,
     0,     0,     0,     0,    39,     0,    91,     0,     0,    78,
    79,   306,     0,     0,   308,   309,    68,     0,     0,     0,
    80,    81,    82,    83,     0,     0,     0,     0,     0,     0,
     0,    69,    70,    71,    72,    73,    74,     0,     0,     0,
     0,     0,     0,     0,     0,    75,    76,     0,     0,     7,
    84,    85,    86,    87,    88,    89,    77,    70,    71,    72,
    73,    74,     0,    39,     0,    91,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,    77,    70,    71,    72,    73,    74,     0,    78,    79,
     0,     0,     0,     0,     0,     0,     0,     0,     0,    80,
    81,    82,    83,     0,     0,     0,    77,     0,     0,     0,
     0,     0,     0,    78,    79,     0,     0,     0,     0,     0,
     0,     0,     0,     0,    80,    81,    82,    83,     7,    84,
    85,    86,    87,    88,    89,     0,     0,     0,    78,    79,
     0,     0,    39,     0,    91,     0,     0,     0,     0,    80,
    81,    82,    83,     7,    84,    85,    86,    87,    88,    89,
     0,     0,   152,     0,   347,     0,     0,     0,     0,    91,
     0,    70,    71,    72,    73,    74,  -203,     0,     7,    84,
    85,    86,    87,    88,    89,     0,     0,   152,     0,   350,
     0,     0,     0,     0,    91,    77,    70,    71,    72,    73,
    74,     0,     0,  -203,  -203,  -203,  -203,  -203,  -203,  -203,
  -203,  -203,  -203,     0,     0,     0,     0,     0,  -203,     0,
    77,    70,    71,    72,    73,    74,     0,    78,    79,     0,
     0,     0,     0,     0,     0,     0,     0,     0,    80,    81,
    82,    83,     0,     0,     0,    77,   160,     0,     0,     0,
     0,     0,    78,    79,     0,     0,     0,    70,    71,    72,
    73,    74,     0,    80,    81,    82,    83,     7,    84,    85,
    86,    87,    88,    89,     0,     0,   152,    78,    79,     0,
     0,    77,     0,    91,     0,     0,     0,     0,    80,    81,
    82,    83,     7,    84,    85,    86,    87,    88,    89,     0,
     0,     0,     0,     0,     0,     0,     0,     0,    91,     0,
     0,     0,     0,    78,    79,     0,     0,     7,    84,    85,
    86,    87,    88,    89,    80,    81,    82,    83,   246,     0,
     0,     0,     0,    91,     0,     0,    70,    71,    72,    73,
    74,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,  -202,     7,    84,    85,    86,    87,    88,    89,
    77,     0,     0,     0,   273,     0,     0,     0,     0,    91,
     0,     0,    70,    71,    72,    73,    74,     0,     0,  -202,
  -202,  -202,  -202,  -202,  -202,  -202,  -202,  -202,  -202,     0,
     0,     0,    78,    79,  -202,     0,    77,    70,    71,    72,
    73,    74,     0,    80,    81,    82,    83,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,    77,     0,     0,     0,     0,     0,     0,    78,    79,
     0,     0,     7,    84,    85,    86,    87,    88,    89,    80,
    81,    82,    83,   275,     0,     0,     0,     0,    91,     0,
     0,     0,     0,    78,    79,     0,    70,    71,    72,    73,
    74,     0,     0,     0,    80,    81,    82,    83,     7,    84,
    85,    86,    87,    88,    89,     0,     0,     0,     0,   292,
    77,     0,     0,     0,    91,     0,     0,     0,     0,     0,
     0,     0,     0,     7,    84,    85,    86,    87,    88,    89,
   206,     0,     0,     0,   323,     0,     0,     0,     0,    91,
     0,     0,    78,    79,    70,    71,    72,    73,    74,     0,
     0,     0,     0,    80,    81,    82,    83,   207,   208,   209,
   210,   211,   212,   213,   214,   215,   216,     0,    77,     0,
     0,     0,   217,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     7,    84,    85,    86,    87,    88,    89,     0,
     0,     0,     0,   327,     0,     0,     0,     0,    91,     0,
    78,    79,     0,     0,     0,     0,     0,     0,     0,     0,
     0,    80,    81,    82,    83,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     7,    84,    85,    86,    87,    88,    89,     0,     0,     0,
     0,     0,     0,     0,     0,     0,    91
};

static const short yycheck[] = {     1,
     2,    25,    68,   149,     3,    97,    44,    77,     3,    11,
   151,    45,    14,    15,    16,    87,    64,    19,    78,    91,
   282,    55,    21,    85,    79,    24,    21,   123,    26,    24,
    32,    93,    79,    35,    78,    12,    13,    14,    15,    41,
    17,    18,    19,    20,    46,   307,    78,   219,   220,   137,
    48,    78,    76,   145,   139,    32,    78,    79,    80,    81,
    82,    83,    78,    12,    13,    14,    15,   137,    17,    18,
    19,   243,    86,    78,   134,    77,    78,    79,    80,    81,
    82,    83,    78,    32,   139,    86,    78,   228,    78,    78,
   134,   137,   139,    12,    13,    14,    15,    95,    17,    18,
    19,   242,   134,   365,   139,   367,   144,   134,    80,   371,
   135,   123,   123,    32,    64,   377,   123,   379,   134,    69,
   132,   383,   294,   134,   123,    75,   298,   134,   123,   134,
   222,    98,    99,   139,    77,   139,   103,    80,   134,   138,
   286,    91,   134,   138,   134,   134,   123,   149,   139,   151,
    11,   307,    94,    95,    77,   132,   158,    80,    19,   137,
   162,    81,    82,    79,   186,   187,   188,    77,    88,    89,
   104,   105,   106,    79,   123,   177,   194,   195,   196,   197,
   102,   183,   100,   132,   186,   187,   188,   189,   190,   191,
   192,   193,   194,   195,   196,   197,   198,   199,   200,   201,
   202,   203,   250,   205,   123,   101,   156,   157,   139,   365,
    92,   367,    96,    97,   238,   371,   282,   283,   191,   192,
   193,   377,   135,   379,    42,    43,   228,   383,   178,    79,
    80,   189,   190,   198,   199,    77,     9,    77,    77,   139,
   242,   307,    78,    79,    80,    81,    82,    83,   135,   134,
   136,   120,    86,   136,   204,   134,   134,   107,   108,   109,
   110,   111,   112,   113,   114,   115,   116,   139,   139,   219,
   220,   139,   122,   339,   139,    77,    77,   136,   136,   136,
   136,    77,    77,    77,   286,   139,   139,   353,   139,   139,
   139,   139,     8,   243,     8,   133,   139,   299,   139,   365,
   250,   367,   134,   369,   134,   371,     8,    77,   134,   134,
   134,   377,   134,   379,   362,   120,     8,   383,   342,   134,
   133,    78,    79,    80,    81,    82,    83,     8,   134,     0,
     0,     3,     9,    19,   284,   285,   338,    46,   239,   333,
   239,   201,   205,   299,   294,   295,    -1,   202,   298,    -1,
   186,   187,   188,   189,   190,   191,   192,   193,   194,   195,
   196,   197,   198,   199,   200,   201,   202,   203,   203,   205,
    -1,   200,   218,    -1,    -1,    -1,     6,    -1,    -1,     9,
    10,    11,   332,   333,   334,    -1,    -1,   337,    -1,    -1,
   340,    21,    -1,    -1,    24,    -1,    26,    27,    28,    29,
    30,    31,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    40,    41,   362,    -1,    -1,    -1,    -1,    -1,   368,    -1,
    -1,    51,    -1,    -1,    -1,    -1,    -1,    -1,   378,   186,
   187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
   197,   198,   199,   200,   201,   202,   203,    -1,   205,    -1,
    -1,    -1,    -1,    83,    84,     6,    -1,    -1,     9,    10,
    11,    -1,    -1,   299,    94,    95,    96,    97,    -1,    -1,
    21,    -1,    -1,    24,    -1,    26,    27,    28,    29,    30,
    31,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    40,
    41,    -1,    -1,   123,   124,   125,   126,   127,   128,   129,
    51,    -1,    -1,    -1,    -1,    -1,    -1,   137,   138,   139,
    -1,    -1,    -1,    -1,    -1,    -1,     6,    -1,    -1,     9,
    10,    11,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    21,    83,    84,    24,    -1,    26,    27,    28,    29,
    30,    31,   299,    94,    95,    96,    97,    -1,    -1,    -1,
    40,    41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    51,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,   123,   124,   125,   126,   127,   128,   129,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,   137,   138,   139,    -1,
    -1,    -1,    -1,    83,    84,     6,    -1,    -1,     9,    10,
    11,    -1,    -1,    -1,    94,    95,    96,    97,    -1,    -1,
    21,    -1,    -1,    24,    -1,    26,    27,    28,    29,    30,
    31,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    40,
    41,    -1,    -1,   123,   124,   125,   126,   127,   128,   129,
    51,    -1,    -1,    -1,    -1,    -1,    -1,   137,   138,   139,
    -1,    -1,    -1,    -1,    -1,    -1,     6,    -1,    -1,     9,
    10,    11,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    21,    83,    84,    24,    -1,    26,    27,    28,    29,
    30,    31,    -1,    94,    95,    96,    97,    -1,    -1,    -1,
    40,    41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    51,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,   123,   124,   125,   126,   127,   128,   129,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,   137,   138,   139,    -1,
    -1,    -1,    -1,    83,    84,     6,    -1,    -1,     9,    10,
    11,    -1,    -1,    -1,    94,    95,    96,    97,    -1,    -1,
    21,    -1,    -1,    24,    -1,    26,    27,    28,    29,    30,
    31,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    40,
    41,    -1,    -1,   123,   124,   125,   126,   127,   128,   129,
    51,    -1,    -1,    -1,    -1,    -1,    -1,   137,   138,   139,
    -1,    -1,    -1,    -1,     6,     7,    -1,     9,    10,    11,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    83,    84,    26,    27,    28,    29,    30,    31,
    -1,    -1,    -1,    94,    95,    96,    97,    -1,    40,    41,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    51,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,   123,   124,   125,   126,   127,   128,   129,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,   137,   138,   139,    -1,
    -1,    83,    84,     6,     7,    -1,     9,    10,    11,    -1,
    -1,    -1,    94,    95,    96,    97,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    26,    27,    28,    29,    30,    31,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    40,    41,    -1,
    -1,   123,   124,   125,   126,   127,   128,   129,    51,    -1,
    -1,    -1,    -1,    -1,    -1,   137,    -1,   139,    -1,    -1,
    -1,    -1,     6,    -1,    -1,     9,    10,    11,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    83,    84,    26,    27,    28,    29,    30,    31,    -1,    -1,
    -1,    94,    95,    96,    97,    -1,    40,    41,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    51,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
   123,   124,   125,   126,   127,   128,   129,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,   137,    -1,   139,    -1,    -1,    83,
    84,     6,    -1,    -1,     9,    10,    11,    -1,    -1,    -1,
    94,    95,    96,    97,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    26,    27,    28,    29,    30,    31,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    40,    41,    -1,    -1,   123,
   124,   125,   126,   127,   128,   129,    51,    27,    28,    29,
    30,    31,    -1,   137,    -1,   139,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    51,    27,    28,    29,    30,    31,    -1,    83,    84,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    94,
    95,    96,    97,    -1,    -1,    -1,    51,    -1,    -1,    -1,
    -1,    -1,    -1,    83,    84,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    94,    95,    96,    97,   123,   124,
   125,   126,   127,   128,   129,    -1,    -1,    -1,    83,    84,
    -1,    -1,   137,    -1,   139,    -1,    -1,    -1,    -1,    94,
    95,    96,    97,   123,   124,   125,   126,   127,   128,   129,
    -1,    -1,   132,    -1,   134,    -1,    -1,    -1,    -1,   139,
    -1,    27,    28,    29,    30,    31,    80,    -1,   123,   124,
   125,   126,   127,   128,   129,    -1,    -1,   132,    -1,   134,
    -1,    -1,    -1,    -1,   139,    51,    27,    28,    29,    30,
    31,    -1,    -1,   107,   108,   109,   110,   111,   112,   113,
   114,   115,   116,    -1,    -1,    -1,    -1,    -1,   122,    -1,
    51,    27,    28,    29,    30,    31,    -1,    83,    84,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    94,    95,
    96,    97,    -1,    -1,    -1,    51,    77,    -1,    -1,    -1,
    -1,    -1,    83,    84,    -1,    -1,    -1,    27,    28,    29,
    30,    31,    -1,    94,    95,    96,    97,   123,   124,   125,
   126,   127,   128,   129,    -1,    -1,   132,    83,    84,    -1,
    -1,    51,    -1,   139,    -1,    -1,    -1,    -1,    94,    95,
    96,    97,   123,   124,   125,   126,   127,   128,   129,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   139,    -1,
    -1,    -1,    -1,    83,    84,    -1,    -1,   123,   124,   125,
   126,   127,   128,   129,    94,    95,    96,    97,   134,    -1,
    -1,    -1,    -1,   139,    -1,    -1,    27,    28,    29,    30,
    31,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    80,   123,   124,   125,   126,   127,   128,   129,
    51,    -1,    -1,    -1,   134,    -1,    -1,    -1,    -1,   139,
    -1,    -1,    27,    28,    29,    30,    31,    -1,    -1,   107,
   108,   109,   110,   111,   112,   113,   114,   115,   116,    -1,
    -1,    -1,    83,    84,   122,    -1,    51,    27,    28,    29,
    30,    31,    -1,    94,    95,    96,    97,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    51,    -1,    -1,    -1,    -1,    -1,    -1,    83,    84,
    -1,    -1,   123,   124,   125,   126,   127,   128,   129,    94,
    95,    96,    97,   134,    -1,    -1,    -1,    -1,   139,    -1,
    -1,    -1,    -1,    83,    84,    -1,    27,    28,    29,    30,
    31,    -1,    -1,    -1,    94,    95,    96,    97,   123,   124,
   125,   126,   127,   128,   129,    -1,    -1,    -1,    -1,   134,
    51,    -1,    -1,    -1,   139,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,   123,   124,   125,   126,   127,   128,   129,
    80,    -1,    -1,    -1,   134,    -1,    -1,    -1,    -1,   139,
    -1,    -1,    83,    84,    27,    28,    29,    30,    31,    -1,
    -1,    -1,    -1,    94,    95,    96,    97,   107,   108,   109,
   110,   111,   112,   113,   114,   115,   116,    -1,    51,    -1,
    -1,    -1,   122,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,   123,   124,   125,   126,   127,   128,   129,    -1,
    -1,    -1,    -1,   134,    -1,    -1,    -1,    -1,   139,    -1,
    83,    84,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    94,    95,    96,    97,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
   123,   124,   125,   126,   127,   128,   129,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,   139
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "bison.simple"

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi)
#include <alloca.h>
#else /* not sparc */
#if defined (MSDOS) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
 #pragma alloca
#else /* not MSDOS, __TURBOC__, or _AIX */
#ifdef __hpux
#ifdef __cplusplus
extern "C" {
void *alloca (unsigned int);
};
#else /* not __cplusplus */
void *alloca ();
#endif /* not __cplusplus */
#endif /* __hpux */
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif /* alloca not defined.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	return(0)
#define YYABORT 	return(1)
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
int yyparse (void);
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(FROM,TO,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (from, to, count)
     char *from;
     char *to;
     int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (char *from, char *to, int count)
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 192 "bison.simple"

/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#else
#define YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#endif

int
yyparse(YYPARSE_PARAM)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
      yyss = (short *) alloca (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss1, (char *)yyss, size * sizeof (*yyssp));
      yyvs = (YYSTYPE *) alloca (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs1, (char *)yyvs, size * sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) alloca (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls1, (char *)yyls, size * sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 1:
#line 382 "bscala.bison"
{ root = new sProgrammStatement(yyvsp[0].sTypeDeclarations_value); ;
    break;}
case 2:
#line 383 "bscala.bison"
{ root = NULL; ;
    break;}
case 3:
#line 386 "bscala.bison"
{ yyval.sTypeDeclarations_value = new sTypeDeclarations(yyvsp[0].sTypeDeclaration_value); ;
    break;}
case 4:
#line 387 "bscala.bison"
{ yyval.sTypeDeclarations_value = sTypeDeclarations::append(yyvsp[-1].sTypeDeclarations_value, yyvsp[0].sTypeDeclaration_value); ;
    break;}
case 5:
#line 390 "bscala.bison"
{ yyval.sTypeDeclaration_value = new sTypeDeclaration(yyvsp[0].sClassDeclaration_value, NULL, CLASS_DECLARATION); ;
    break;}
case 6:
#line 391 "bscala.bison"
{ yyval.sTypeDeclaration_value = new sTypeDeclaration(NULL, yyvsp[0].sSingletonDeclaration_value, SINGLETON_DECLARATION); ;
    break;}
case 7:
#line 394 "bscala.bison"
{ yyval.sName_value = new sName(yyvsp[0].string_value); ;
    break;}
case 8:
#line 398 "bscala.bison"
{ yyval.sBooleanLiteral_value = new sBooleanLiteral(TRUE_LITERAL); ;
    break;}
case 9:
#line 399 "bscala.bison"
{ yyval.sBooleanLiteral_value = new sBooleanLiteral(FALSE_LITERAL);;
    break;}
case 10:
#line 402 "bscala.bison"
{ yyval.Expression_value = (Expression *) (new sLiteral(*yyvsp[0].int_value, 0.0, FALSE_LITERAL, '\0', "\0", 0,   0.0, INT_LITERAL    ) );;
    break;}
case 11:
#line 403 "bscala.bison"
{ yyval.Expression_value = (Expression *) (new sLiteral(0,   *yyvsp[0].float_value, FALSE_LITERAL, '\0', "\0", 0,   0.0, FLOAT_LITERAL  ) );;
    break;}
case 12:
#line 404 "bscala.bison"
{ yyval.Expression_value = (Expression *) (new sLiteral(0,   0.0, *yyvsp[0].sBooleanLiteral_value,           '\0', "\0", 0,   0.0, BOOLEAN_LITERAL) );;
    break;}
case 13:
#line 405 "bscala.bison"
{ yyval.Expression_value = (Expression *) (new sLiteral(0,   0.0, FALSE_LITERAL,  *yyvsp[0].char_value, "\0", 0,   0.0, CHAR_LITERAL   ) );;
    break;}
case 14:
#line 406 "bscala.bison"
{ yyval.Expression_value = (Expression *) (new sLiteral(0,   0.0, FALSE_LITERAL, '\0', *yyvsp[0].string_value,  0,   0.0, STRING_LITERAL ) );;
    break;}
case 15:
#line 407 "bscala.bison"
{ yyval.Expression_value = (Expression *) (new sLiteral(0,   0.0, FALSE_LITERAL, '\0', "\0", 0,   0.0, NULL_LITERAL   ) );;
    break;}
case 16:
#line 408 "bscala.bison"
{ yyval.Expression_value = (Expression *) (new sLiteral(0,   0.0, FALSE_LITERAL, '\0', "\0", *yyvsp[0].long_value, 0.0, LONG_LITERAL   ) );;
    break;}
case 17:
#line 409 "bscala.bison"
{ yyval.Expression_value = (Expression *) (new sLiteral(0,   0.0, FALSE_LITERAL, '\0', "\0", 0,   *yyvsp[0].double_value, DOUBLE_LITERAL ) );;
    break;}
case 18:
#line 412 "bscala.bison"
{ yyval.sType_value = new sType(yyvsp[0].sPrimitiveType_value, NULL, PRIMITIVE_TYPE); ;
    break;}
case 19:
#line 413 "bscala.bison"
{ yyval.sType_value = new sType(NULL, yyvsp[0].sReferenceType_value, REFERENCE_TYPE); ;
    break;}
case 20:
#line 416 "bscala.bison"
{ yyval.sMethodType_value = new sMethodType(yyvsp[0].sType_value, false); ;
    break;}
case 21:
#line 417 "bscala.bison"
{ yyval.sMethodType_value = new sMethodType(NULL, true); ;
    break;}
case 22:
#line 420 "bscala.bison"
{ yyval.sPrimitiveType_value = new sPrimitiveType(INT); ;
    break;}
case 23:
#line 421 "bscala.bison"
{ yyval.sPrimitiveType_value = new sPrimitiveType(LONG); ;
    break;}
case 24:
#line 422 "bscala.bison"
{ yyval.sPrimitiveType_value = new sPrimitiveType(BYTE); ;
    break;}
case 25:
#line 423 "bscala.bison"
{ yyval.sPrimitiveType_value = new sPrimitiveType(FLOAT); ;
    break;}
case 26:
#line 424 "bscala.bison"
{ yyval.sPrimitiveType_value = new sPrimitiveType(DOUBLE);;
    break;}
case 27:
#line 425 "bscala.bison"
{ yyval.sPrimitiveType_value = new sPrimitiveType(BOOLEAN); ;
    break;}
case 28:
#line 426 "bscala.bison"
{ yyval.sPrimitiveType_value = new sPrimitiveType(CHAR); ;
    break;}
case 29:
#line 427 "bscala.bison"
{ yyval.sPrimitiveType_value = new sPrimitiveType(STRING_CLASS); ;
    break;}
case 30:
#line 430 "bscala.bison"
{ yyval.sReferenceType_value = new sReferenceType(yyvsp[0].sName_value, NULL); ;
    break;}
case 31:
#line 431 "bscala.bison"
{ yyval.sReferenceType_value = new sReferenceType(NULL, yyvsp[0].sArrayType_value); ;
    break;}
case 32:
#line 434 "bscala.bison"
{ yyval.sArrayType_value = new sArrayType(yyvsp[-1].sPrimitiveType_value, NULL, ARRAY_PRIMITVE_TYPE); ;
    break;}
case 33:
#line 435 "bscala.bison"
{ yyval.sArrayType_value = new sArrayType(NULL, yyvsp[-1].sName_value, ARRAY_NAME); ;
    break;}
case 34:
#line 439 "bscala.bison"
{ yyval.sClassDeclaration_value = new sClassDeclaration( yyvsp[-1].sName_value, yyvsp[0].sClassBody_value); ;
    break;}
case 35:
#line 443 "bscala.bison"
{ yyval.sSingletonDeclaration_value = new sSingletonDeclaration( yyvsp[-1].sName_value, yyvsp[0].sClassBody_value); ;
    break;}
case 36:
#line 446 "bscala.bison"
{ yyval.sClassBody_value = new sClassBody(yyvsp[-1].sClassBodyDeclarations_value); ;
    break;}
case 37:
#line 447 "bscala.bison"
{ yyval.sClassBody_value = new sClassBody(NULL); ;
    break;}
case 38:
#line 450 "bscala.bison"
{ yyval.sClassBodyDeclarations_value = new sClassBodyDeclarations(yyvsp[0].sClassBodyDeclaration_value); ;
    break;}
case 39:
#line 451 "bscala.bison"
{ yyval.sClassBodyDeclarations_value = sClassBodyDeclarations::append(yyvsp[-1].sClassBodyDeclarations_value, yyvsp[0].sClassBodyDeclaration_value); ;
    break;}
case 40:
#line 455 "bscala.bison"
{ yyval.sClassBodyDeclaration_value = new sClassBodyDeclaration(yyvsp[0].sClassMemberDeclaration_value, NULL); ;
    break;}
case 41:
#line 456 "bscala.bison"
{ yyval.sClassBodyDeclaration_value = new sClassBodyDeclaration(NULL, yyvsp[0].sConstructorDeclaration_value); ;
    break;}
case 42:
#line 459 "bscala.bison"
{ yyval.sClassMemberDeclaration_value = new sClassMemberDeclaration(yyvsp[0].sFieldDeclaration_value, NULL, FIELD_DECLARATION); ;
    break;}
case 43:
#line 460 "bscala.bison"
{ yyval.sClassMemberDeclaration_value = new sClassMemberDeclaration(NULL, yyvsp[0].sMethodDeclaration_value, METHOD_DECLARATION);;
    break;}
case 44:
#line 463 "bscala.bison"
{ yyval.sFieldDeclaration_value = (new sFieldDeclaration( yyvsp[-2].sVariableDeclaratorId_value, NULL, yyvsp[-1].sType_value)); ;
    break;}
case 45:
#line 464 "bscala.bison"
{ yyval.sFieldDeclaration_value = (new sFieldDeclaration( yyvsp[-4].sVariableDeclaratorId_value, yyvsp[-1].sVariableInitializer_value, yyvsp[-3].sType_value)); ;
    break;}
case 46:
#line 467 "bscala.bison"
{ yyval.sVariableDeclaratorId_value = new sVariableDeclaratorId(yyvsp[0].sName_value, false); ;
    break;}
case 47:
#line 468 "bscala.bison"
{ yyval.sVariableDeclaratorId_value = new sVariableDeclaratorId(yyvsp[0].sName_value, true); ;
    break;}
case 48:
#line 471 "bscala.bison"
{ yyval.sVariableInitializer_value = new sVariableInitializer(yyvsp[0].Expression_value, NULL, EXPRESSION); ;
    break;}
case 49:
#line 472 "bscala.bison"
{ yyval.sVariableInitializer_value = new sVariableInitializer(NULL, yyvsp[0].sArrayInitializer_value, ARRAY_INITIALIZER); ;
    break;}
case 50:
#line 476 "bscala.bison"
{ yyval.sMethodDeclaration_value = new sMethodDeclaration(yyvsp[-1].sMethodHeader_value, yyvsp[0].sBlock_value); ;
    break;}
case 51:
#line 477 "bscala.bison"
{ yyval.sMethodDeclaration_value = new sMethodDeclaration(yyvsp[-1].sMethodHeader_value, NULL); ;
    break;}
case 52:
#line 482 "bscala.bison"
{ yyval.sMethodHeader_value = new sMethodHeader( yyvsp[-2].sMethodDeclarator_value, yyvsp[-1].sMethodType_value); ;
    break;}
case 53:
#line 483 "bscala.bison"
{ yyval.sMethodHeader_value = new sMethodHeader( yyvsp[0].sMethodDeclarator_value, NULL); ;
    break;}
case 54:
#line 486 "bscala.bison"
{ yyval.sMethodDeclarator_value = new sMethodDeclarator(yyvsp[-3].sName_value, yyvsp[-1].sFormalParameterList_value); ;
    break;}
case 55:
#line 487 "bscala.bison"
{ yyval.sMethodDeclarator_value = new sMethodDeclarator(yyvsp[-2].sName_value, NULL); ;
    break;}
case 56:
#line 490 "bscala.bison"
{ yyval.sFormalParameterList_value = new sFormalParameterList(yyvsp[0].sFormalParameter_value); ;
    break;}
case 57:
#line 491 "bscala.bison"
{ yyval.sFormalParameterList_value = sFormalParameterList::append(yyvsp[-2].sFormalParameterList_value, yyvsp[0].sFormalParameter_value); ;
    break;}
case 58:
#line 494 "bscala.bison"
{ yyval.sFormalParameter_value = new sFormalParameter(yyvsp[-1].sName_value, yyvsp[0].sType_value); ;
    break;}
case 59:
#line 499 "bscala.bison"
{ yyval.sConstructorDeclaration_value = new sConstructorDeclaration(yyvsp[-1].sConstructorDeclarator_value, yyvsp[0].sConstructorBody_value); ;
    break;}
case 60:
#line 502 "bscala.bison"
{ yyval.sConstructorDeclarator_value = new sConstructorDeclarator(yyvsp[-3].sName_value, yyvsp[-1].sFormalParameterList_value); ;
    break;}
case 61:
#line 503 "bscala.bison"
{ yyval.sConstructorDeclarator_value = new sConstructorDeclarator(yyvsp[-2].sName_value, NULL); ;
    break;}
case 62:
#line 506 "bscala.bison"
{ yyval.sConstructorBody_value = new sConstructorBody(yyvsp[-2].sExplicitConstructorInvocation_value, yyvsp[-1].sBlockStatements_value); ;
    break;}
case 63:
#line 507 "bscala.bison"
{ yyval.sConstructorBody_value = new sConstructorBody(yyvsp[-1].sExplicitConstructorInvocation_value, NULL); ;
    break;}
case 64:
#line 508 "bscala.bison"
{ yyval.sConstructorBody_value = new sConstructorBody(NULL, yyvsp[-1].sBlockStatements_value);;
    break;}
case 65:
#line 509 "bscala.bison"
{ yyval.sConstructorBody_value = new sConstructorBody(NULL, NULL); ;
    break;}
case 66:
#line 512 "bscala.bison"
{ yyval.sExplicitConstructorInvocation_value = new sExplicitConstructorInvocation(yyvsp[-2].sArgumentList_value, THIS_EXPLICIT_CONSTRUCTOR_INVOCATION); ;
    break;}
case 67:
#line 513 "bscala.bison"
{ yyval.sExplicitConstructorInvocation_value = new sExplicitConstructorInvocation(NULL, THIS_EXPLICIT_CONSTRUCTOR_INVOCATION); ;
    break;}
case 68:
#line 514 "bscala.bison"
{ yyval.sExplicitConstructorInvocation_value = new sExplicitConstructorInvocation(yyvsp[-2].sArgumentList_value, SUPER_EXPLICIT_CONSTRUCTOR_INVOCATION);;
    break;}
case 69:
#line 515 "bscala.bison"
{ yyval.sExplicitConstructorInvocation_value = new sExplicitConstructorInvocation(NULL, SUPER_EXPLICIT_CONSTRUCTOR_INVOCATION);;
    break;}
case 70:
#line 518 "bscala.bison"
{ yyval.sArrayInitializer_value = new sArrayInitializer(yyvsp[-4].sPrimitiveType_value, NULL, yyvsp[-1].sVariableInitializers_value  ); ;
    break;}
case 71:
#line 519 "bscala.bison"
{ yyval.sArrayInitializer_value = new sArrayInitializer(NULL, yyvsp[-4].sName_value, yyvsp[-1].sVariableInitializers_value  ); ;
    break;}
case 72:
#line 520 "bscala.bison"
{ yyval.sArrayInitializer_value = new sArrayInitializer(yyvsp[-3].sPrimitiveType_value, NULL, NULL); ;
    break;}
case 73:
#line 521 "bscala.bison"
{ yyval.sArrayInitializer_value = new sArrayInitializer(NULL, yyvsp[-3].sName_value, NULL); ;
    break;}
case 74:
#line 524 "bscala.bison"
{ yyval.sVariableInitializers_value = new sVariableInitializers(yyvsp[0].sVariableInitializer_value); ;
    break;}
case 75:
#line 525 "bscala.bison"
{ yyval.sVariableInitializers_value = yyval.sVariableInitializers_value = sVariableInitializers::append(yyvsp[-2].sVariableInitializers_value, yyvsp[0].sVariableInitializer_value); ;
    break;}
case 76:
#line 528 "bscala.bison"
{ yyval.sBlock_value = (Statement *) (yyvsp[-1].sBlockStatements_value); ;
    break;}
case 77:
#line 529 "bscala.bison"
{ yyval.sBlock_value = NULL; ;
    break;}
case 78:
#line 532 "bscala.bison"
{ yyval.sBlockStatements_value = new sBlockStatements(yyvsp[0].sBlockStatement_value); ;
    break;}
case 79:
#line 533 "bscala.bison"
{ yyval.sBlockStatements_value = sBlockStatements::append(yyvsp[-1].sBlockStatements_value, yyvsp[0].sBlockStatement_value); ;
    break;}
case 80:
#line 536 "bscala.bison"
{ yyval.sBlockStatement_value = new sBlockStatement(LOCAL_VARIBLE_DECLARATION, yyvsp[-2].sVariableDeclaratorId_value, yyvsp[-1].sType_value, NULL, NULL ); ;
    break;}
case 81:
#line 537 "bscala.bison"
{yyval.sBlockStatement_value = new sBlockStatement(LOCAL_VARIBLE_DECLARATION, yyvsp[-4].sVariableDeclaratorId_value, yyvsp[-3].sType_value, yyvsp[-1].sVariableInitializer_value, NULL ); ;
    break;}
case 82:
#line 538 "bscala.bison"
{ yyval.sBlockStatement_value = new sBlockStatement(STATEMENT, NULL, NULL, NULL, yyvsp[0].Statement_value); ;
    break;}
case 83:
#line 541 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].Statement_value); ;
    break;}
case 84:
#line 542 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].Statement_value); ;
    break;}
case 85:
#line 543 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].Statement_value); ;
    break;}
case 86:
#line 544 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].Statement_value); ;
    break;}
case 87:
#line 545 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].Statement_value); ;
    break;}
case 88:
#line 548 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].Statement_value); ;
    break;}
case 89:
#line 549 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].Statement_value); ;
    break;}
case 90:
#line 550 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].Statement_value); ;
    break;}
case 91:
#line 551 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].Statement_value); ;
    break;}
case 92:
#line 555 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].sBlock_value); ;
    break;}
case 93:
#line 557 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].Statement_value); ;
    break;}
case 94:
#line 558 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].Statement_value); ;
    break;}
case 95:
#line 559 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].Statement_value); ;
    break;}
case 96:
#line 560 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].Statement_value); ;
    break;}
case 97:
#line 561 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[0].Statement_value); ;
    break;}
case 98:
#line 564 "bscala.bison"
{ yyval.Statement_value = (Statement *) (yyvsp[-1].Statement_value); ;
    break;}
case 99:
#line 567 "bscala.bison"
{ yyval.Statement_value = (Statement *)(new sStatementExpression((Expression *)(yyvsp[0].Expression_value)));;
    break;}
case 100:
#line 576 "bscala.bison"
{ yyval.Statement_value = (Statement *) new IfStatement(yyvsp[-3].Expression_value, yyvsp[0].Statement_value, NULL); ;
    break;}
case 101:
#line 577 "bscala.bison"
{ yyval.Statement_value = (Statement *) new IfStatement(yyvsp[-2].Expression_value, yyvsp[0].Statement_value, NULL); ;
    break;}
case 102:
#line 580 "bscala.bison"
{ yyval.Statement_value = (Statement *) new IfStatement(yyvsp[-5].Expression_value, yyvsp[-2].Statement_value, yyvsp[0].Statement_value); ;
    break;}
case 103:
#line 581 "bscala.bison"
{ yyval.Statement_value = (Statement *) new IfStatement(yyvsp[-4].Expression_value, yyvsp[-2].Statement_value, yyvsp[0].Statement_value); ;
    break;}
case 104:
#line 584 "bscala.bison"
{ yyval.Statement_value = (Statement *) new IfStatement(yyvsp[-5].Expression_value, yyvsp[-2].Statement_value, yyvsp[0].Statement_value); ;
    break;}
case 105:
#line 585 "bscala.bison"
{ yyval.Statement_value = (Statement *) new IfStatement(yyvsp[-4].Expression_value, yyvsp[-2].Statement_value, yyvsp[0].Statement_value); ;
    break;}
case 106:
#line 588 "bscala.bison"
{ yyval.Statement_value = (Statement *) new sWhileStatement(yyvsp[-2].Expression_value, yyvsp[0].Statement_value); ;
    break;}
case 107:
#line 591 "bscala.bison"
{ yyval.Statement_value = (Statement *) new sWhileStatement(yyvsp[-2].Expression_value, yyvsp[0].Statement_value); ;
    break;}
case 108:
#line 594 "bscala.bison"
{ yyval.Statement_value = (Statement *) ( new sDoStatement(yyvsp[-5].Statement_value, yyvsp[-2].Expression_value) ); ;
    break;}
case 109:
#line 597 "bscala.bison"
{ yyval.Statement_value = new ForStatement(yyvsp[-6].sName_value,   yyvsp[-4].Expression_value,   yyvsp[-2].Expression_value,   yyvsp[0].Statement_value); ;
    break;}
case 110:
#line 600 "bscala.bison"
{ yyval.Statement_value = new ForStatement(yyvsp[-6].sName_value,   yyvsp[-4].Expression_value,   yyvsp[-2].Expression_value,   yyvsp[0].Statement_value); ;
    break;}
case 111:
#line 603 "bscala.bison"
{ yyval.sForInit_value = new sForInit(yyvsp[0].sStatementExpressionList_value, NULL, NULL, NULL); ;
    break;}
case 112:
#line 604 "bscala.bison"
{ yyval.sForInit_value = new sForInit(NULL, yyvsp[0].sVariableDeclaratorId_value, NULL, NULL); ;
    break;}
case 113:
#line 605 "bscala.bison"
{ yyval.sForInit_value = new sForInit(NULL, yyvsp[-1].sVariableDeclaratorId_value, yyvsp[0].sType_value, NULL); ;
    break;}
case 114:
#line 606 "bscala.bison"
{ yyval.sForInit_value = new sForInit(NULL, yyvsp[-3].sVariableDeclaratorId_value, yyvsp[-2].sType_value, yyvsp[0].sVariableInitializer_value); ;
    break;}
case 115:
#line 609 "bscala.bison"
{ yyval.sForUpdate_value = new sForUpdate(yyvsp[0].sStatementExpressionList_value); ;
    break;}
case 116:
#line 612 "bscala.bison"
{ yyval.sStatementExpressionList_value = new sStatementExpressionList( (sStatementExpression *) (yyvsp[0].Statement_value) ); ;
    break;}
case 117:
#line 613 "bscala.bison"
{ yyval.sStatementExpressionList_value = sStatementExpressionList::append( yyvsp[-2].sStatementExpressionList_value, (sStatementExpression *) (yyvsp[0].Statement_value) ); ;
    break;}
case 118:
#line 616 "bscala.bison"
{ yyval.Statement_value = (Statement *) ( new sReturnStatement( yyvsp[-1].Expression_value ) ); ;
    break;}
case 119:
#line 617 "bscala.bison"
{ yyval.Statement_value = (Statement *) ( new sReturnStatement(NULL) ); ;
    break;}
case 120:
#line 620 "bscala.bison"
{ yyval.Statement_value = (Statement *) ( new sThrowStatement(yyvsp[-1].Expression_value) ); ;
    break;}
case 121:
#line 623 "bscala.bison"
{ yyval.Statement_value = (Statement *) ( new sTryStatement(yyvsp[-1].sBlock_value, yyvsp[0].sCatches_value,   NULL) ); ;
    break;}
case 122:
#line 624 "bscala.bison"
{ yyval.Statement_value = (Statement *) ( new sTryStatement(yyvsp[-2].sBlock_value, yyvsp[-1].sCatches_value,   yyvsp[0].Statement_value  ) ); ;
    break;}
case 123:
#line 625 "bscala.bison"
{ yyval.Statement_value = (Statement *) ( new sTryStatement(yyvsp[-1].sBlock_value, NULL, yyvsp[0].Statement_value  ) ); ;
    break;}
case 124:
#line 628 "bscala.bison"
{ yyval.sCatches_value = new sCatches(yyvsp[0].sCatchClause_value); ;
    break;}
case 125:
#line 628 "bscala.bison"
{ yyval.sCatches_value = sCatches::append(yyvsp[-1].sCatches_value, yyvsp[0].sCatchClause_value); ;
    break;}
case 126:
#line 631 "bscala.bison"
{ yyval.sCatchClause_value = new sCatchClause(yyvsp[-2].sFormalParameter_value, yyvsp[0].sBlock_value); ;
    break;}
case 127:
#line 634 "bscala.bison"
{ yyval.Statement_value = (Statement *) new sFinally(yyvsp[0].sBlock_value); ;
    break;}
case 128:
#line 637 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value; ;
    break;}
case 129:
#line 638 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value; ;
    break;}
case 130:
#line 641 "bscala.bison"
{ yyval.Expression_value = (Expression *) (yyvsp[0].Expression_value);;
    break;}
case 131:
#line 642 "bscala.bison"
{ yyval.Expression_value = (Expression *) (new sThis());;
    break;}
case 132:
#line 643 "bscala.bison"
{ yyval.Expression_value = yyvsp[-1].Expression_value;;
    break;}
case 133:
#line 644 "bscala.bison"
{ yyval.Expression_value = (Expression *) (yyvsp[0].sClassInstanceCreationExpression_value);;
    break;}
case 134:
#line 645 "bscala.bison"
{ yyval.Expression_value = (Expression *) (yyvsp[0].sFieldAccess_value);;
    break;}
case 135:
#line 647 "bscala.bison"
{ yyval.Expression_value = (Expression *) (yyvsp[0].sMethodInvocation_value);;
    break;}
case 136:
#line 653 "bscala.bison"
{ yyval.sClassInstanceCreationExpression_value = new sClassInstanceCreationExpression(yyvsp[-3].sName_value, yyvsp[-1].sArgumentList_value  ); ;
    break;}
case 137:
#line 654 "bscala.bison"
{ yyval.sClassInstanceCreationExpression_value = new sClassInstanceCreationExpression(yyvsp[-2].sName_value, NULL); ;
    break;}
case 138:
#line 657 "bscala.bison"
{ yyval.sArgumentList_value = new sArgumentList(yyvsp[0].Expression_value); ;
    break;}
case 139:
#line 658 "bscala.bison"
{ yyval.sArgumentList_value = sArgumentList::append(yyvsp[-2].sArgumentList_value, yyvsp[0].Expression_value); ;
    break;}
case 140:
#line 661 "bscala.bison"
{ yyval.Expression_value = (Expression *) ( new sArrayCreationExpression(yyvsp[-3].sPrimitiveType_value, NULL) ); ;
    break;}
case 141:
#line 662 "bscala.bison"
{ yyval.Expression_value = (Expression *) ( new sArrayCreationExpression(NULL, yyvsp[-3].sName_value) ); ;
    break;}
case 142:
#line 665 "bscala.bison"
{ yyval.sFieldAccess_value = new sFieldAccess( yyvsp[-2].Expression_value, yyvsp[0].sName_value, false); ;
    break;}
case 143:
#line 666 "bscala.bison"
{ yyval.sFieldAccess_value = new sFieldAccess(NULL, yyvsp[0].sName_value, true); ;
    break;}
case 144:
#line 667 "bscala.bison"
{ yyval.sFieldAccess_value = new sFieldAccess(yyvsp[-2].sName_value, yyvsp[0].sName_value, true); ;
    break;}
case 145:
#line 670 "bscala.bison"
{ yyval.sMethodInvocation_value = new sMethodInvocation(yyvsp[-3].sName_value,yyvsp[-1].sArgumentList_value,NULL); ;
    break;}
case 146:
#line 671 "bscala.bison"
{ yyval.sMethodInvocation_value = new sMethodInvocation(yyvsp[-2].sName_value, NULL, NULL);;
    break;}
case 147:
#line 672 "bscala.bison"
{ yyval.sMethodInvocation_value = new sMethodInvocation(yyvsp[-3].sName_value, yyvsp[-1].sArgumentList_value, yyvsp[-5].Expression_value); ;
    break;}
case 148:
#line 673 "bscala.bison"
{ yyval.sMethodInvocation_value = new sMethodInvocation(yyvsp[-2].sName_value, NULL, yyvsp[-4].Expression_value); ;
    break;}
case 149:
#line 674 "bscala.bison"
{ yyval.sMethodInvocation_value = new sMethodInvocation(yyvsp[-3].sName_value, yyvsp[-1].sArgumentList_value, yyvsp[-5].sName_value); ;
    break;}
case 150:
#line 675 "bscala.bison"
{ yyval.sMethodInvocation_value = new sMethodInvocation(yyvsp[-2].sName_value, NULL, yyvsp[-4].sName_value); ;
    break;}
case 151:
#line 678 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value; ;
    break;}
case 152:
#line 679 "bscala.bison"
{ yyval.Expression_value = (Expression *) (yyvsp[0].sName_value);;
    break;}
case 153:
#line 680 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value; ;
    break;}
case 154:
#line 681 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value; ;
    break;}
case 155:
#line 684 "bscala.bison"
{ yyval.Expression_value = new sPostIncrementExpression(yyvsp[-1].Expression_value); ;
    break;}
case 156:
#line 687 "bscala.bison"
{ yyval.Expression_value = new sPostDecrementExpression(yyvsp[-1].Expression_value); ;
    break;}
case 157:
#line 690 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value;;
    break;}
case 158:
#line 691 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value;;
    break;}
case 159:
#line 692 "bscala.bison"
{ yyval.Expression_value = new sUnaryExpression(NULL, NULL, yyvsp[0].Expression_value, NULL, NULL);;
    break;}
case 160:
#line 693 "bscala.bison"
{ yyval.Expression_value = new sUnaryExpression(NULL, NULL, NULL, yyvsp[0].Expression_value, NULL);;
    break;}
case 161:
#line 694 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value;;
    break;}
case 162:
#line 697 "bscala.bison"
{ yyval.Expression_value = new sPreIncrementExpression(yyvsp[0].Expression_value); ;
    break;}
case 163:
#line 700 "bscala.bison"
{ yyval.Expression_value = new sPreDecrementExpression(yyvsp[0].Expression_value); ;
    break;}
case 164:
#line 703 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value; ;
    break;}
case 165:
#line 704 "bscala.bison"
{ yyval.Expression_value = new sUnaryExpressionNotPlusMinus(NULL, yyvsp[0].Expression_value, NULL, NULL); ;
    break;}
case 166:
#line 705 "bscala.bison"
{ yyval.Expression_value = new sUnaryExpressionNotPlusMinus(NULL, NULL, yyvsp[0].Expression_value, NULL); ;
    break;}
case 167:
#line 708 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value;;
    break;}
case 168:
#line 709 "bscala.bison"
{ yyval.Expression_value = new sMultiplicativeExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value,   TIMES_MULTIPLICATE_EXPRESSION);;
    break;}
case 169:
#line 710 "bscala.bison"
{ yyval.Expression_value = new sMultiplicativeExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value,   DIV_MULTIPLICATE_EXPRESSION  );;
    break;}
case 170:
#line 711 "bscala.bison"
{ yyval.Expression_value = new sMultiplicativeExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value,   XOR_MULTIPLICATE_EXPRESSION  );;
    break;}
case 171:
#line 714 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value;;
    break;}
case 172:
#line 715 "bscala.bison"
{ yyval.Expression_value = new sAdditiveExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value,   PLUS_ADDITIVE_EXPRESSION );;
    break;}
case 173:
#line 716 "bscala.bison"
{ yyval.Expression_value = new sAdditiveExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value,   MINUS_ADDITIVE_EXPRESSION);;
    break;}
case 174:
#line 719 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value;;
    break;}
case 175:
#line 720 "bscala.bison"
{ yyval.Expression_value = new sShiftExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value,   LTLT_SHIFT_EXPRESSION  );;
    break;}
case 176:
#line 721 "bscala.bison"
{ yyval.Expression_value = new sShiftExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value,   GTGT_SHIFT_EXPRESSION  );;
    break;}
case 177:
#line 722 "bscala.bison"
{ yyval.Expression_value = new sShiftExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value,   GTGTGT_SHIFT_EXPRESSION);;
    break;}
case 178:
#line 725 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value;;
    break;}
case 179:
#line 726 "bscala.bison"
{ yyval.Expression_value = new sRelationalExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value,   LT_RELATIONAL_EXPRESSION   );;
    break;}
case 180:
#line 727 "bscala.bison"
{ yyval.Expression_value = new sRelationalExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value,   GT_RELATIONAL_EXPRESSION   );;
    break;}
case 181:
#line 728 "bscala.bison"
{ yyval.Expression_value = new sRelationalExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value,   LTEQ_RELATIONAL_EXPRESSION );;
    break;}
case 182:
#line 729 "bscala.bison"
{ yyval.Expression_value = new sRelationalExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value,   GTEQ_RELATIONAL_EXPRESSION );;
    break;}
case 183:
#line 732 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value;;
    break;}
case 184:
#line 733 "bscala.bison"
{ yyval.Expression_value = new sEqualityExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value,   EQEQ_EQUALITY_EXPRESSION );;
    break;}
case 185:
#line 734 "bscala.bison"
{ yyval.Expression_value = new sEqualityExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value,   NOTEQ_EQUALITY_EXPRESSION);;
    break;}
case 186:
#line 737 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value;;
    break;}
case 187:
#line 738 "bscala.bison"
{ yyval.Expression_value = new sAndExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value  );;
    break;}
case 188:
#line 741 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value; ;
    break;}
case 189:
#line 742 "bscala.bison"
{ yyval.Expression_value = new sExclusiveOrExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value);;
    break;}
case 190:
#line 745 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value; ;
    break;}
case 191:
#line 746 "bscala.bison"
{ yyval.Expression_value = new sInclusiveOrExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value); ;
    break;}
case 192:
#line 749 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value; ;
    break;}
case 193:
#line 750 "bscala.bison"
{ yyval.Expression_value = new sConditionalAndExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value); ;
    break;}
case 194:
#line 753 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value; ;
    break;}
case 195:
#line 754 "bscala.bison"
{ yyval.Expression_value = new sConditionalOrExpression(yyvsp[0].Expression_value, yyvsp[-2].Expression_value); ;
    break;}
case 196:
#line 757 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value; ;
    break;}
case 197:
#line 758 "bscala.bison"
{ yyval.Expression_value = new sConditionalExpression(yyvsp[-4].Expression_value, yyvsp[-2].Expression_value, yyvsp[0].Expression_value);;
    break;}
case 198:
#line 761 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value;;
    break;}
case 199:
#line 762 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value;;
    break;}
case 200:
#line 765 "bscala.bison"
{ yyval.Expression_value = ((sAssignmentOperator *)(yyvsp[-1].Expression_value))->setOperands(yyvsp[-2].Expression_value, yyvsp[0].Expression_value); ;
    break;}
case 201:
#line 768 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].sName_value ;
    break;}
case 202:
#line 769 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].sMethodInvocation_value ;
    break;}
case 203:
#line 770 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].sFieldAccess_value ;
    break;}
case 204:
#line 773 "bscala.bison"
{ yyval.Expression_value = new sAssignmentOperator(ASSIGNING_ASSIGNMENT_OPERATOR   );;
    break;}
case 205:
#line 774 "bscala.bison"
{ yyval.Expression_value = new sAssignmentOperator(TIMESEQ_ASSIGNMENT_OPERATOR     );;
    break;}
case 206:
#line 775 "bscala.bison"
{ yyval.Expression_value = new sAssignmentOperator(DIVEQ_ASSIGNMENT_OPERATOR       );;
    break;}
case 207:
#line 776 "bscala.bison"
{ yyval.Expression_value = new sAssignmentOperator(XOREQ_ASSIGNMENT_OPERATOR       );;
    break;}
case 208:
#line 777 "bscala.bison"
{ yyval.Expression_value = new sAssignmentOperator(PLUSEQ_ASSIGNMENT_OPERATOR      );;
    break;}
case 209:
#line 778 "bscala.bison"
{ yyval.Expression_value = new sAssignmentOperator(MINUSEQ_ASSIGNMENT_OPERATOR     );;
    break;}
case 210:
#line 779 "bscala.bison"
{ yyval.Expression_value = new sAssignmentOperator(LTLTEQ_ASSIGNMENT_OPERATOR      );;
    break;}
case 211:
#line 780 "bscala.bison"
{ yyval.Expression_value = new sAssignmentOperator(GTGTEQ_ASSIGNMENT_OPERATOR      );;
    break;}
case 212:
#line 781 "bscala.bison"
{ yyval.Expression_value = new sAssignmentOperator(GTGTGTEQ_ASSIGNMENT_OPERATOR    );;
    break;}
case 213:
#line 782 "bscala.bison"
{ yyval.Expression_value = new sAssignmentOperator(ANDEQ_ASSIGNMENT_OPERATOR       );;
    break;}
case 214:
#line 783 "bscala.bison"
{ yyval.Expression_value = new sAssignmentOperator(OREQ_ASSIGNMENT_OPERATOR        );;
    break;}
case 215:
#line 784 "bscala.bison"
{ yyval.Expression_value = new sAssignmentOperator(CIRCUMFLEXEQ_ASSIGNMENT_OPERATOR);;
    break;}
case 216:
#line 787 "bscala.bison"
{ yyval.Expression_value = yyvsp[0].Expression_value; ;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 487 "bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;
}
#line 790 "bscala.bison"


void yyerror(char const *s)
{
    printf("%s",s);
}