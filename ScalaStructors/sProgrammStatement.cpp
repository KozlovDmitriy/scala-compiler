#include "Project.h"

void sProgrammStatement::createTableOfConstants() {
	sTypeDeclaration * type_decl = this->type_declarations->begin;
	if ( type_decl != NULL ) {
		while ( type_decl != NULL ) {
			type_decl->getTableOfConstantsRow();
			ofstream out("Constants//"+type_decl->constants_table.getName() + ".txt");
			out << type_decl->constants_table.getStringTable();
			out.close();
			type_decl = type_decl->next;
		}
	}
	vector<Node *> method_decls = this->findAllChildrensOfType( & string("sMethodDeclaration") );
	for (int i = 0; i < method_decls.size(); ++i) {
		sMethodDeclaration * method_decl = (sMethodDeclaration *) method_decls[i];
		method_decl->fillLocalConstants( NULL, true );
		ofstream out( "Constants//" + method_decl->local_constants->getName() + "_local.txt" );
		out << method_decl->local_constants->getStringTable();
		out.close();
	}
}

void sProgrammStatement::createGlobalConstantsTable() {
	// �������� ������ � ���� �������
	sTypeDeclaration * type_decl = this->type_declarations->begin;
	if ( type_decl != NULL ) {
		while ( type_decl != NULL ) {
			*(this->global_constants_table) << type_decl->constants_table;			
			type_decl = type_decl->next;
		}
		ofstream out("Constants//GLOBAL_CONSTANT_TABLE.txt");
		out << this->global_constants_table->getStringTable();
		out.close();
	}
}

void sProgrammStatement::transformTree() {
	// step 1: find array access (langauge feature: there are not arrays!!! arrays is method with override apply method)
	vector<Node *> method_invs = this->findAllChildrensOfType(&string("sMethodInvocation"));
	for (int i = 0; i < method_invs.size(); ++i) {
		sMethodInvocation * cur_method_inv =  static_cast<sMethodInvocation *>(method_invs[i]);
		cur_method_inv->transformToArrayAccess();
	}

	// step 2: replece =[] with single node
	vector<Node *> assignments = this->findAllChildrensOfType(&string("sAssignmentOperator"));
	for (int i = 0; i < assignments.size(); ++i) {
		sAssignmentOperator * cur_assignment = static_cast<sAssignmentOperator *>(assignments[i]);
		cur_assignment->transformToArrayAssignment();
	}
}

vector<unsigned char> * sProgrammStatement::generateByteCode( BinaryWriter * writer ) {
	sTypeDeclaration * type_decl = this->type_declarations->begin;
	if ( type_decl != NULL ) {
		while ( type_decl != NULL ) {
			if ( type_decl->class_declaration != NULL ) {
				type_decl->class_declaration->generateByteCode( NULL );
			}
			if ( type_decl->singleton_declaration != NULL ) {
				type_decl->singleton_declaration->generateByteCode( NULL );
			}
			type_decl = type_decl->next;
		}
	}
	return NULL;
}