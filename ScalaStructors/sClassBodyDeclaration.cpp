#include "Project.h"

TableOfConstantsRow * sClassBodyDeclaration::getTableOfConstantsRow() {
	if ( this->table_of_constants_row == NULL ) {
		if ( constructor_declaration != NULL ) {
			this->table_of_constants_row = constructor_declaration->getTableOfConstantsRow();
		} else {
			this->table_of_constants_row = class_member_declaration->getTableOfConstantsRow();
		}
		if ( next != NULL ) {
			next->getTableOfConstantsRow();
		}
	}
	return this->table_of_constants_row;
}