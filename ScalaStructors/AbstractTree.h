#pragma once
#include "Project.h"
using namespace std;

class sArgumentList : public Node {
public:
	vector<Expression *> expressions;
	sArgumentList(Expression * _expression) {
		this->expressions.push_back(_expression);
		this->addChildren((Node *)_expression, "_expression");
		this->setDotLabel("Argument List");
	}
	static sArgumentList * append(sArgumentList * _argument_list, Expression * _expression) {
		_argument_list->expressions.push_back(_expression);
		if (_expression != NULL) {
			_argument_list->addChildren((Node *)_expression, "next");
		}
		return _argument_list;
	}
};

class sArrayAccess : Node {
public:
	sName * id;
	sPrimaryNoNewArray * primary_no_new_array;
	Expression * expression;
	eArrayAccess type;
	sArrayAccess (
			sName * _id,
			sPrimaryNoNewArray * _primary_no_new_array,
			Expression * _expression,
			eArrayAccess _type
		) : id (_id),
			primary_no_new_array (_primary_no_new_array),
			expression (_expression),
			type (_type) {
		if ( id != NULL ) {
			this->addChildren( (Node *) id, "id" );
			this->setDotLabel( "Array Access : Name" );
		}
		if ( primary_no_new_array != NULL ) {
			this->addChildren( (Node *) primary_no_new_array, "primary_no_new_array" );
			this->setDotLabel( "Array Access : Primary no new array" );
		}
		this->addChildren( (Node *) expression, "expression" );
	}
};

class sVariableInitializer : Node {
public:
	eVariableInitializer type;
	Expression * expression;
	sArrayInitializer * array_initializer;
	sVariableInitializer * next;
	sVariableInitializer (
			Expression * _expression,
			sArrayInitializer * _array_initializer,
			eVariableInitializer _type
		) : expression (_expression),
			array_initializer (_array_initializer),
			type (_type) {
		this->addChildren( (Node *) expression, "expression" );
		this->addChildren( (Node *) array_initializer, "array_initializer" );
		next = NULL;
		if (_type == EXPRESSION) {
			this->setDotLabel( "Variable Initializer : Expression" );
		} else {
			this->setDotLabel( "Variable Initializer : Array initializer" );
		}
	}
};

class sVariableInitializers : Node {
public:
	sVariableInitializer * begin;
	sVariableInitializer * end;
	sVariableInitializers (
			sVariableInitializer * _variable_initializer
		) : begin (_variable_initializer),
			end (_variable_initializer) {
		this->addChildren( (Node *) begin, "begin" );
		this->setDotLabel( "Variable Initializers" );
	}
	static sVariableInitializers * append (
			sVariableInitializers * _variable_initializers,
			sVariableInitializer * _variable_initializer
		) {
		_variable_initializers->end->next = _variable_initializer;
		( (Node *)(_variable_initializers) )->addChildren( (Node *) _variable_initializers->end->next, "next" );
		_variable_initializers->end = _variable_initializer;
		( (Node *)(_variable_initializers) )->rewriteEnd( (Node *) _variable_initializer );
		return _variable_initializers;
	}
};

class sMethodDeclarator : public Node {
public:
	sName * id;
	sFormalParameterList * formal_parametr_list;
	sMethodDeclarator (
			sName * _id,
			sFormalParameterList * _formal_parametr_list
		) : id (_id),
			formal_parametr_list (_formal_parametr_list) {
		this->addChildren( (Node *) id, "id" );
		this->addChildren( (Node *) formal_parametr_list, "formal_parametr_list" );
		this->setDotLabel( "Method Declarator" );
	}
	TableOfConstantsRow * getTableOfConstantsRow() override {
		if ( this->table_of_constants_row == NULL ) {
			this->table_of_constants_row = id->getTableOfConstantsRow();
		}
		return this->table_of_constants_row;
	}
};

// �������� ����� �����

// ������� �����

class sPrimitiveType : public Node {
public:
	ePrimitiveType type;
	sPrimitiveType ( ePrimitiveType _type ) : type (_type) {
		switch (_type) {
			case INT:
				this->setDotLabel( "Primitive Type: INT" );
				break;
			case LONG:
				this->setDotLabel( "Primitive Type: LONG" );
				break;
			case FLOAT:
				this->setDotLabel( "Primitive Type: FLOAT" );
				break;
			case DOUBLE:
				this->setDotLabel( "Primitive Type: DOUBLE" );
				break;
			case CHAR:
				this->setDotLabel( "Primitive Type: CHAR" );
				break;
			case STRING_CLASS:
				this->setDotLabel( "Primitive Type: STRING_CLASS" );
				break;
		}
	}
	TableOfConstantsRow * getTableOfConstantsRow() override {
		if ( this->table_of_constants_row == NULL ) {
			HandlerRow * row = NULL;
			switch ( type ) {
				case INT:			row = new HandlerRow( false, false, "I" );					break;
				case CHAR:			row = new HandlerRow( false, false, "C" );					break;
				case LONG:			row = new HandlerRow( false, false, "I" );					break;
				case FLOAT:			row = new HandlerRow( false, false, "F" );					break;
				case DOUBLE:		row = new HandlerRow( false, false, "F" );					break;
				case STRING_CLASS:	row = new HandlerRow( false, true,  "java/lang/String" );	break;
			}
			this->table_of_constants_row = row;
			//this->getConstantsTable()->addRow( this->table_of_constants_row );
		}
		return this->table_of_constants_row;
	}
};

class sArrayType : public Node {
public:
	eArrayType type;
	sPrimitiveType * primitive_type;
	sName * id;
	sArrayType (
			sPrimitiveType * _primitive_type,
			sName * _id,
			eArrayType _type
		) : primitive_type (_primitive_type),
			id (_id),
			type (_type) {
		if ( primitive_type != NULL ) {
			this->addChildren( (Node *) primitive_type, "primitive_type" );
			this->setDotLabel( "Array Type : Primitive type" );
		}
		if ( id != NULL ) {
			this->addChildren( (Node *) id, "id" );
			this->setDotLabel( "Array Type : Name" );
		}
	}
	TableOfConstantsRow * getTableOfConstantsRow() override {
		if ( this->table_of_constants_row == NULL ) {
			HandlerRow * row  = NULL;
			if ( id != NULL ) {
				row = new HandlerRow( true, true, ( (UTF8Row *) id->getTableOfConstantsRow() )->getValue() );
				this->table_of_constants_row = row;
			} else {
				row = (HandlerRow *) primitive_type->getTableOfConstantsRow();
				row = row->toArray();
				this->table_of_constants_row = row;
			}
		}
		return this->table_of_constants_row;
	}
};

class sReferenceType : public Node {
public:
	sName * id;
	sArrayType * array_type;
	sReferenceType ( sName * _id, sArrayType * _array_type ) : id (_id), array_type (_array_type) {
		this->addChildren( (Node *) array_type, "array_type" );
		this->addChildren( (Node *) id, "id" );
		this->setDotLabel( "Reference Type" );
	}
	TableOfConstantsRow * getTableOfConstantsRow() override {
		if ( this->table_of_constants_row == NULL ) {
			HandlerRow * row  = NULL;
			if ( id != NULL ) {
				row = new HandlerRow( false, true, ( (UTF8Row *) id->getTableOfConstantsRow() )->getValue() );
				this->table_of_constants_row = row;
			} else {
				this->table_of_constants_row = array_type->getTableOfConstantsRow();
			}
		}
		return this->table_of_constants_row;
	}
};

class sType : public Node {
public:
	eType type;
	sPrimitiveType * primitive_type;
	sReferenceType * reference_type;
	sType (
		    sPrimitiveType * _primitive_type,
			sReferenceType * _reference_type,
			eType _type
		) : primitive_type (_primitive_type),
			reference_type (_reference_type),
			type (_type) {
		this->addChildren( (Node *) primitive_type, "primitive_type" );
		this->addChildren( (Node *) reference_type, "reference_type" );
		this->setDotLabel( "Type" );
	}
	TableOfConstantsRow * getTableOfConstantsRow() override {
		if ( this->table_of_constants_row == NULL ) {
			if ( primitive_type != NULL ) {
				this->table_of_constants_row = primitive_type->getTableOfConstantsRow();
			} else {
				this->table_of_constants_row = reference_type->getTableOfConstantsRow();
			}
			this->getConstantsTable()->addRow( this->table_of_constants_row );
		}
		return this->table_of_constants_row;
	}
};

//------------------------------------------------------------------------------------------

class sClassDeclaration : public Node {
public:
	sName * id;
	sClassBody * class_body;
	sClassDeclaration (
			sName * _id,
			sClassBody * _class_body
		) : id (_id),
			class_body (_class_body) {
		this->cpp_type = "sClassDeclaration";
		this->addChildren( (Node *) id, "id" );
		this->addChildren( (Node *) class_body, "class_body" );
		this->setDotLabel( "class" );
	}
	TableOfConstantsRow * getTableOfConstantsRow() override;
	vector<unsigned char> * generateByteCode( BinaryWriter * writer ) override;
};

class sConstructorDeclarator : public Node {
public:
	Expression * id;
	sFormalParameterList * formal_parameter_list;
	sConstructorDeclarator (
			Expression * _id,
			sFormalParameterList * _formal_parameter_list
		) : id (_id),
			formal_parameter_list (_formal_parameter_list) {
		this->addChildren( (Node *) id, "id" );
		this->addChildren( (Node *) formal_parameter_list, "formal_parameter_list" );
		string str = "Constructor Declarator";
		this->setDotLabel( str );
	}
	TableOfConstantsRow * getTableOfConstantsRow() override;
};

class sFormalParameter : Node {
public:
	Expression * variable_declarator_id;
	sType * type;
	sFormalParameter * next;
	sFormalParameter (
			Expression * _variable_declarator_id,
			sType * _type
		) : variable_declarator_id (_variable_declarator_id),
			type (_type) {
		this->addChildren( (Node *) variable_declarator_id, "variable_declarator_id" );
		this->addChildren( (Node *) type, "type" );
		next = NULL;
		this->setDotLabel( "Formal Parameter" );
	}
};

class sFormalParameterList : Node {
public:
	sFormalParameter * begin;
	sFormalParameter * end;
	sFormalParameterList (
			sFormalParameter * _formal_parameter
		) : begin (_formal_parameter),
			end (_formal_parameter) {
		this->addChildren( (Node *) begin, "begin" );
		this->setDotLabel( "Formal Parameter List" );
	}
	static sFormalParameterList * append (
			sFormalParameterList * _formal_parameters,
			sFormalParameter * _formal_parameter
		) {
		_formal_parameters->end->next = _formal_parameter;
		( (Node *)(_formal_parameters) )->addChildren( (Node *) _formal_parameters->end->next, "next" );
		_formal_parameters->end = _formal_parameter;
		( (Node *)(_formal_parameters) )->rewriteEnd( (Node *) _formal_parameter );
		return _formal_parameters;
	}
};

class sVariableDeclaratorId : Node {
public:
	bool is_const;
	sName * id;
	int array_demantions;
	sVariableDeclaratorId ( sName * _id, bool _is_const ) : id (_id), is_const(_is_const) {
		this->array_demantions = 0;
		this->addChildren( (Node *) _id, "id" );
		if ( _is_const ) {
			this->setDotLabel( "val" );
		} else {
			this->setDotLabel( "var" );
		}
	}
	static sVariableDeclaratorId * increment_dimensions (
			sVariableDeclaratorId * _variable_declarator_id
		) {
		++ _variable_declarator_id->array_demantions;
		string str = "Variable Declarator Id : ";
		if ( _variable_declarator_id->is_const ) {
			str.append( "val Name" );
		}
		for (int i = 0; i < _variable_declarator_id->array_demantions; ++i) {
			str.append( "[]" );
		}
		_variable_declarator_id->setDotLabel( str );
		return _variable_declarator_id;
	}
	TableOfConstantsRow * getTableOfConstantsRow() override {
		if ( this->table_of_constants_row == NULL ) {
			this->table_of_constants_row = (UTF8Row *) id->getTableOfConstantsRow();
		}
		return this->table_of_constants_row;
	}
};

// ���������� �����������
class sSingletonDeclaration : public Node {
public:
	sName * id;
	sClassBody * class_body;
	sSingletonDeclaration (
			sName * _id,
			sClassBody * _class_body
		) : id (_id),
			class_body (_class_body) {
		this->addChildren( (Node *) id, "id" );
		this->addChildren( (Node *) class_body, "class_body" );
		this->setDotLabel( "object" );
	}
	TableOfConstantsRow * getTableOfConstantsRow() override;
	vector<unsigned char> * generateByteCode( BinaryWriter * writer ) override;
};


//-- ����� ����� ConditionalExpression

// �������� ����������� �����

class sCatchClause : public Node {
public:
	sFormalParameter * formal_parameter;
	Statement * block;
	sCatchClause * next;
	sCatchClause (
			sFormalParameter * _formal_parameter,
			Statement * _block
		) : formal_parameter (_formal_parameter),
			block (_block) {
		this->addChildren( (Node *) formal_parameter , "formal_parameter");
		this->addChildren( (Node *) block , "block");
		next = NULL;
		this->setDotLabel( "Catch Clause" );
	}
};

class sCatches : public Node {
public:
	sCatchClause * begin;
	sCatchClause * end;
	sCatches (
			sCatchClause * _catch_clause
		) : begin (_catch_clause),
			end (_catch_clause) {
		this->addChildren( (Node *) begin , "begin");
		this->setDotLabel( "Catches" );
	}
	static sCatches * append ( sCatches * _catches, sCatchClause * _catch_clause ) {
		_catches->end->next = _catch_clause;
		( (Node *)(_catches) )->addChildren( (Node *) _catches->end->next , "next");
		_catches->end = _catch_clause;
		( (Node *)(_catches) )->rewriteEnd( (Node *) _catch_clause );
		return _catches;
	}
};

class sFinally : public Node {
public:
	Statement * block;
	sFinally ( Statement * _block ) : block (_block) {
		this->addChildren( (Node *) block , "block");
		this->setDotLabel( "Finally" );
	}
	~sFinally () {
		if ( block != NULL ) {
			delete block;
		}
	}
};

class sForInit : public Node {
public:
	sStatementExpressionList * statement_expression_list;
	sVariableDeclaratorId * local_variable_declaration;
	sType * type;
	sVariableInitializer * variable_initializer;
	sForInit (
			sStatementExpressionList * _statement_expression_list,
			sVariableDeclaratorId * _local_variable_declaration,
			sType * _type,
			sVariableInitializer * _variable_initializer
		) : statement_expression_list (_statement_expression_list),
			local_variable_declaration (_local_variable_declaration),
			type (_type),
			variable_initializer (_variable_initializer) {
		this->addChildren( (Node *) statement_expression_list , "statement_expression_list");
		this->addChildren( (Node *) local_variable_declaration , "local_variable_declaration");
		this->addChildren( (Node *) type , "type");
		this->addChildren( (Node *) variable_initializer , "variable_initializer");
		this->setDotLabel( "For Init" );
	}
};

class sForUpdate : public Node {
public:
	Statement * statement_expression_list;
	sForUpdate (
			Statement * _statement_expression_list
		) : statement_expression_list (_statement_expression_list) {
		this->addChildren( (Node *) statement_expression_list , "statement_expression_list");
		this->setDotLabel( "For Update" );
	}
};

// �������� ������������ ������

class sTypeDeclaration : public Node {
public:
	eTypeDeclaration type;
	sClassDeclaration * class_declaration;
	sSingletonDeclaration * singleton_declaration;
	sTypeDeclaration * next;
	TableOfConstants constants_table;
	sTypeDeclaration (
			sClassDeclaration * _class_declaration,
			sSingletonDeclaration * _singleton_declaration,
			eTypeDeclaration _type
		) : class_declaration (_class_declaration),
			singleton_declaration (_singleton_declaration),
			type (_type) {
		// ����������� �� ���������
		this->constants_table.addRow( new UTF8Row("<init>") );
		this->constants_table.addRow( new UTF8Row("()V") );
		this->cpp_type = "sTypeDeclaration";
		this->addChildren( (Node *) class_declaration , "class_declaration");
		this->addChildren( (Node *) singleton_declaration , "singleton_declaration");
		next = NULL;
		string str = "Type Declarations : ";
		switch (type) {
			case CLASS_DECLARATION :
				str.append( "Class" );
				break;
			case TRAIT_DECLARATION :
				str.append( "Trait" );
				break;
			case SINGLETON_DECLARATION :
				str.append( "Singleton" );
				break;
		}
		this->setDotLabel( str );
	}
	TableOfConstantsRow * getTableOfConstantsRow() override {
		if ( this->table_of_constants_row == NULL ) {
			if ( class_declaration != NULL) {
				this->table_of_constants_row = class_declaration->getTableOfConstantsRow();
			} else {
				this->table_of_constants_row = singleton_declaration->getTableOfConstantsRow();
			}
			this->constants_table.setName( ( (ClassRow *) this->table_of_constants_row )->getClassName() );
		}
		return this->table_of_constants_row;
	}
};

class sTypeDeclarations : public Node {
public:
	sTypeDeclaration * begin;
	sTypeDeclaration * end;
	sTypeDeclarations (
			sTypeDeclaration * _type_declaration
		) : begin (_type_declaration),
			end (_type_declaration) {
		this->addChildren( (Node *) begin , "begin");
		this->setDotLabel( "Type Declarations" );
	}
	static sTypeDeclarations * append (sTypeDeclarations * _type_declarations, sTypeDeclaration * _type_declaration) {
		_type_declarations->end->next = _type_declaration;
		( (Node *)(_type_declarations) )->addChildren( (Node *) _type_declarations->end->next , "next");
		_type_declarations->end = _type_declaration;
		( (Node *)(_type_declarations) )->rewriteEnd( (Node *) _type_declaration );
		return _type_declarations;
	}
};

class sProgrammStatement : public Node {
public:
	sTypeDeclarations * type_declarations;
	TableOfConstants * global_constants_table;
	sProgrammStatement ( sTypeDeclarations * _type_declarations ) : type_declarations( _type_declarations ) {
		this->cpp_type = "sProgrammStatement";
		this->global_constants_table = new TableOfConstants();
		this->addChildren( (Node *) type_declarations , "type_declarations");
		this->setDotLabel( "Programm" );
	}
	void createTableOfConstants();
	void transformTree();
	void createGlobalConstantsTable();
	vector<unsigned char> * generateByteCode( BinaryWriter * writer ) override;
};

//-- ����� ClassDeclaration

//-- -- ����� sClassBody

class sClassBody : public Node {
public:
	sClassBodyDeclarations * class_body_declarations;
	sClassBody ( sClassBodyDeclarations * _class_body_declarations ) : class_body_declarations (_class_body_declarations) {
		this->addChildren( (Node *) class_body_declarations , "class_body_declarations");
		this->setDotLabel( "Class Body" );
	}
	TableOfConstantsRow * getTableOfConstantsRow() override;
};

class sClassBodyDeclaration : public Node {
public:
	sConstructorDeclaration * constructor_declaration;
	sClassMemberDeclaration * class_member_declaration;
	sClassBodyDeclaration * next;
	sClassBodyDeclaration (
			sClassMemberDeclaration * _class_member_declaration,
			sConstructorDeclaration * _constructor_declaration
		) : constructor_declaration (_constructor_declaration),
			class_member_declaration (_class_member_declaration) {
		this->addChildren( (Node *) constructor_declaration , "constructor_declaration");
		this->addChildren( (Node *) class_member_declaration , "class_member_declaration");
		next = NULL;
		this->setDotLabel( "Class Body Declaration" );
		this->cpp_type = "sClassBodyDeclaration";
	}
	TableOfConstantsRow * getTableOfConstantsRow() override;
};

class sClassBodyDeclarations : public Node {
public:
	sClassBodyDeclaration * begin;
	sClassBodyDeclaration * end;
	sClassBodyDeclarations (
			sClassBodyDeclaration * _class_body_declaration
		) : begin (_class_body_declaration),
			end (_class_body_declaration) {
		this->addChildren( (Node *) begin , "begin");
		this->setDotLabel( "Class Body Declarations" );
	}
	static sClassBodyDeclarations * append (
			sClassBodyDeclarations * _class_body_declarations,
			sClassBodyDeclaration * _class_body_declaration
		) {
		_class_body_declarations->end->next = _class_body_declaration;
		( (Node *)(_class_body_declarations) )->addChildren( (Node *) _class_body_declarations->end->next , "next");
		_class_body_declarations->end = _class_body_declaration;
		( (Node *)(_class_body_declarations) )->rewriteEnd( (Node *) _class_body_declaration );
		return _class_body_declarations;
	}
	TableOfConstantsRow * getTableOfConstantsRow() override {
		if ( this->table_of_constants_row == NULL ) {
			if ( begin != NULL ) {
				begin->getTableOfConstantsRow();
			}
		}
		return this->table_of_constants_row;
	}
};


//-- -- -- ����� sConstructorDeclaration

class sConstructorDeclaration : public Node {
public:
	sConstructorDeclarator * constructor_declarator;
	sConstructorBody * constructor_body;
	sConstructorDeclaration (
			sConstructorDeclarator * _constructor_declarator,
			sConstructorBody * _constructor_body
		) : constructor_declarator (_constructor_declarator),
			constructor_body (_constructor_body) {
		this->addChildren( (Node *) constructor_declarator , "constructor_declarator");
		this->addChildren( (Node *) constructor_body , "constructor_body");
		this->setDotLabel( "Constructor Declaration" );
	}
	TableOfConstantsRow * getTableOfConstantsRow() override {
		if ( this->table_of_constants_row == NULL ) {
			if ( constructor_declarator != NULL ) {
				//???
				this->table_of_constants_row = this->constructor_declarator->getTableOfConstantsRow();
			}
		}
		return this->table_of_constants_row;
	}
};

class sConstructorBody : public Node {
public:
	sExplicitConstructorInvocation * explicit_constructor_invocation;
	sBlockStatements * block_statements;
	sConstructorBody (
			sExplicitConstructorInvocation * _explicit_constructor_invocation,
			sBlockStatements * _block_statements
		) : explicit_constructor_invocation (_explicit_constructor_invocation),
			block_statements (_block_statements) {
		this->addChildren( (Node *) explicit_constructor_invocation , "explicit_constructor_invocation");
		this->addChildren( (Node *) block_statements , "block_statements");
		this->setDotLabel( "Constructor Body" );
	}
};

class sExplicitConstructorInvocation : public Node {
public:
	eExplicitConstructorInvocation type;
	sArgumentList * argument_list;
	sExplicitConstructorInvocation (
			sArgumentList * _argument_list,
			eExplicitConstructorInvocation _type
		) : argument_list (_argument_list),
			type (_type) {
		this->addChildren( (Node *) argument_list , "argument_list");
		if (_type == THIS_EXPLICIT_CONSTRUCTOR_INVOCATION) {
			this->setDotLabel( "Explicit Constructor Invocation : THIS" );
		} else {
			this->setDotLabel( "Explicit Constructor Invocation : SUPER" );
		}
	}
};

//-- -- -- ����� ����� sConstructorDeclaration

//-- -- -- ����� sClassMemberDeclaration

class sFieldDeclaration : public Node {
public:
	sVariableDeclaratorId * variable_declarator;
	sVariableInitializer * variable_initializer;
	sType * type;
	sFieldDeclaration (
			sVariableDeclaratorId * _variable_declarator,
			sVariableInitializer * _variable_initializer,
			sType * _type
		) : variable_declarator (_variable_declarator),
			variable_initializer (_variable_initializer),
			type (_type) {
		this->addChildren( (Node *) variable_declarator , "variable_declarator");
		this->addChildren( (Node *) variable_initializer , "variable_initializer");
		this->addChildren( (Node *) type , "type");
		this->setDotLabel( "Field Declaration" );
		this->cpp_type = "sFieldDeclaration";
	}
	TableOfConstantsRow * getTableOfConstantsRow() override;
};


class sMethodType : public Node {
public:
	bool is_unit;
	sType * type;
	sMethodType (
			sType * _type,
			bool _is_unit
		) : type (_type),
			is_unit (_is_unit) {
		this->addChildren( (Node *) type , "type");
		if ( is_unit ) {
			this->setDotLabel( "Method Type : Unit" );
		} else {
			this->setDotLabel( "Method Type : Not unit" );
		}
	}
	TableOfConstantsRow * getTableOfConstantsRow() override {
		if ( this->table_of_constants_row == NULL ) {
			if ( type != NULL ) {
				this->table_of_constants_row = type->getTableOfConstantsRow();
			} else {
				this->table_of_constants_row = new HandlerRow(false,false,"V");
				//this->getConstantsTable()->addRow( this->table_of_constants_row );
			}
		}
		return this->table_of_constants_row;
	}
};

class sMethodHeader : public Node {
public:
	sMethodDeclarator * method_declarator;
	sMethodType * method_type;
	sMethodHeader (
			sMethodDeclarator * _method_declarator,
			sMethodType * _method_type
		) : method_declarator (_method_declarator),
			method_type (_method_type) {
		this->addChildren( (Node *) method_declarator , "method_declarator");
		if ( method_type == NULL ) {
			method_type = new sMethodType(NULL, true);
		}
		this->addChildren( (Node *) method_type , "method_type");
		this->setDotLabel( "Method Header" );
	}
	TableOfConstantsRow * getTableOfConstantsRow() override {
		if ( this->table_of_constants_row == NULL ) {
			UTF8Row * _name = (UTF8Row *) method_declarator->id->getTableOfConstantsRow();
			vector< HandlerRow * > _args;
			if ( method_declarator->formal_parametr_list != NULL ) {
				sFormalParameter * _parameter = method_declarator->formal_parametr_list->begin;
				while (_parameter != NULL) {
					_args.push_back( (HandlerRow *) _parameter->type->getTableOfConstantsRow() );
					_parameter = _parameter->next;
				}
			}
			MethodHandlerRow * _type = new MethodHandlerRow( _args,  (HandlerRow *) method_type->getTableOfConstantsRow() );
			this->getConstantsTable()->addRow( _type );
			this->table_of_constants_row = new NameAndTypeRow(_name, _type);
			this->getConstantsTable()->addRow( this->table_of_constants_row );
		}
		return this->table_of_constants_row;
	}
};

class sMethodDeclaration : public Node {
public:
	sMethodHeader * method_header;
	Statement * block;
	TableOfLocalConstants * local_constants;
	sMethodDeclaration (
			sMethodHeader * _method_header,
			Statement * _block
		) : method_header (_method_header),
			block (_block) {
		this->local_constants = new TableOfLocalConstants();
		this->cpp_type = "sMethodDeclaration";
		this->addChildren( (Node *) method_header , "method_header");
		this->addChildren( (Node *) block , "block");
		this->setDotLabel( "Method Declaration" );
	}
	TableOfConstantsRow * getTableOfConstantsRow() override;
	void fillLocalConstants(sBlockStatement * cur_block_stmt, bool includelast);
	vector<unsigned char> * generateByteCode( BinaryWriter * writer ) override;
};

class sClassMemberDeclaration : public Node {
public:
	eClassMemberDeclaration type;
	sFieldDeclaration * field_declaration;
	sMethodDeclaration * method_declaration;
	sClassMemberDeclaration (
			sFieldDeclaration * _field_declaration,
			sMethodDeclaration * _method_declaration,
			eClassMemberDeclaration _type
		) : field_declaration (_field_declaration),
			method_declaration (_method_declaration),
			type (_type) {
		this->addChildren( (Node *) field_declaration , "field_declaration");
		this->addChildren( (Node *) method_declaration , "method_declaration");
		if (_type == FIELD_DECLARATION) {
			this->setDotLabel( "Class Member Declaration : Field declaration" );
		} else {
			this->setDotLabel( "Class Member Declaration : Method declaration" );
		}
	}
	TableOfConstantsRow * getTableOfConstantsRow() override {
		if ( this->table_of_constants_row == NULL ) {
			if ( field_declaration != NULL ) {
				this->table_of_constants_row = field_declaration->getTableOfConstantsRow();
			} else {
				this->table_of_constants_row = method_declaration->getTableOfConstantsRow();
			}
		}
		return this->table_of_constants_row;
	}
};

//-- -- -- ����� ����� sClassMemberDeclaration

//-- -- ����� ����� sClassBody

//-- ����� ����� ClassDeclaration

// ����� ����� sTypeDeclarations