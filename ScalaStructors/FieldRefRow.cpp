#include "Project.h"

int FieldRefRow::getClassNumber() {
	return this->class_pointer->getNumber();
}

int FieldRefRow::getNameAndTypeNumber() {
	return this->name_and_type_pointer->getNumber();
}

void FieldRefRow::generateFieldTableByteCode( BinaryWriter * writer ) {
	if ( this->abstract_node->attr.is_static ) {
		writer->writeU2( ACC_PUBLIC + ACC_STATIC );
	} else {
		writer->writeU2( ACC_PUBLIC );
	}
	writer->writeU2( this->name_and_type_pointer->getNameNumber() );
	writer->writeU2( this->name_and_type_pointer->getTypeNumber() );
	writer->writeU2( 0 );
}