#include "Project.h"

fakeArrayAccess::fakeArrayAccess(sName * _arrayname, Expression * _argument, Expression * _source) : 
	arrayname(_arrayname),
	argument(_argument),
	source(_source) 
{
	bool selfmethod = false;
	this->transformed = true;
	this->cpp_type = "fakeArrayAccess";
	string todotlabel = "[]";
	this->setDotLabel(todotlabel);
	this->addChildren(source, "source");
	this->addChildren(arrayname, "arrayname");
	this->addChildren(argument, "argument");
}