#include "Project.h"

TableOfConstants * Node::getConstantsTable() {
	sTypeDeclaration * type_decl = (sTypeDeclaration *) this->findParentOfType( & string("sTypeDeclaration") );
	if ( type_decl != NULL ) {
		return & type_decl->constants_table;
	} else {
		return NULL;
	}
}

string Node::findType(string var, bool includelast) {
	sMethodDeclaration * nearestMethodDeclaration = static_cast<sMethodDeclaration *>(findParentOfType(new string("sMethodDeclaration")));
	sBlockStatement * curBlockStatement = static_cast<sBlockStatement *>(findParentOfType(new string("sBlockStatement")));
	if (curBlockStatement == NULL && this->cpp_type == "sBlockStatement") 
	{
		curBlockStatement = static_cast<sBlockStatement *>(this);
	}
	nearestMethodDeclaration->fillLocalConstants(curBlockStatement, includelast);
	sTypeDeclaration * globaltableowner = static_cast<sTypeDeclaration *>(findParentOfType(new string("sTypeDeclaration")));
	string result = nearestMethodDeclaration->local_constants->getVariableType(var);
	if (result.size() == 0) {
		result = globaltableowner->constants_table.getVariableType(var);
	}
	return result;
}

void Node::attribution(bool forcestatic) {
	Node * self = this;
	bool _forcestatic = forcestatic || (this->cpp_type == "sTypeDeclaration" && ((sTypeDeclaration *)(self))->type == SINGLETON_DECLARATION);
	if (_forcestatic) // we under singleton!
	{
		if (this->cpp_type == "sFieldDeclaration" || this->cpp_type == "sMethodDeclaration")
		{
			this->attr.is_static = true;
		} 
		else if (this->cpp_type == "sClassBodyDeclaration")
		{
			sClassBodyDeclaration * _sClassBodyDeclaration = static_cast<sClassBodyDeclaration *>(self);
			_sClassBodyDeclaration->constructor_declaration = NULL;
			_sClassBodyDeclaration->removeChild("constructor_declaration");

		}
	}
	for (int i = 0; i < childrens.size(); ++i)
	{
		childrens[i]->attribution(_forcestatic);
	}
	calcAttr();
}