#include "Project.h"

void TableOfConstantsRow::generateByteCode( BinaryWriter * writer ) {
	unsigned int * data = NULL;
	writer->writeU1( this->type );
	switch ( this->type ) {
		case CONSTANT_UTF8: 
			writer->writeU2( this->getValue().length() );
			for (char ch : this->getValue() ) {
				writer->writeU1( ch );
			}
			break;
		case CONSTANT_INTEGER: writer->writeU4( ( (IntegerRow *) this )->getIntValue() );  break;
		case CONSTANT_FLOAT:   writer->writeF4( ( (FloatRow *) this )->getFloatValue() );  break;
		case CONSTANT_LONG: 
			data = (unsigned int *) ( (LongRow *) this )->getLongValue();
			writer->writeU4( data[1] );
			writer->writeU4( data[0] );
 			break;
		case CONSTANT_DOUBLE:  
			data = (unsigned int *) ( (DoubleRow *) this )->getDoubleValue();
			writer->writeU4( data[1] );
			writer->writeU4( data[0] );
			break;
		case CONSTANT_CLASS:   writer->writeU2( ( (ClassRow *) this )->getUtf8Number() );  break;
		case CONSTANT_STRING:  writer->writeU2( ( (StringRow *) this )->getUtf8Number() ); break;
		case CONSTANT_FIELDREF:
			writer->writeU2( ( (FieldRefRow *) this )->getClassNumber() );
			writer->writeU2( ( (FieldRefRow *) this )->getNameAndTypeNumber() );
			break;
		case CONSTANT_METHODREF:
			writer->writeU2( ( (MethodRefRow *) this )->getClassNumber() );
			writer->writeU2( ( (MethodRefRow *) this )->getNameAndTypeNumber() );
			break;
		case CONSTANT_NAMEANDTYPE: 
			writer->writeU2( ( (NameAndTypeRow *) this )->getNameNumber() );
			writer->writeU2( ( (NameAndTypeRow *) this )->getTypeNumber() );
			break;
		default: break;
	}
}