#include "Project.h"

TableOfConstantsRow * sSingletonDeclaration::getTableOfConstantsRow() {
	if ( this->table_of_constants_row == NULL ) {
		this->table_of_constants_row = new ClassRow( (UTF8Row*) id->getTableOfConstantsRow() );
		this->getConstantsTable()->addRow( this->table_of_constants_row );
		if ( class_body != NULL ) {
			class_body->getTableOfConstantsRow();
		}
	}
	return this->table_of_constants_row;
}

vector<unsigned char> * sSingletonDeclaration::generateByteCode( BinaryWriter * writer ) {
	writer = new BinaryWriter("GenereatedClassFiles\\" + * this->id->id + ".class");
    if ( !writer->isAlive() ) {
        return false;
    }
    // Write magic number and version.
    writer->writeU4(MAGIC_NUMBER);
    writer->writeU2(VERSION_MINOR);
    writer->writeU2(VERSION_MAJOR);
	sProgrammStatement * root = (sProgrammStatement *) this->findParentOfType( & string( "sProgrammStatement" ) );
	root->global_constants_table->generateByteCode( writer );
    // Write access flags.
    writer->writeU2(ACC_SUPER + ACC_PUBLIC);
    // Write CONSTANT_Class for this and parent classes.
	writer->writeU2( this->table_of_constants_row->getNumber() );
    writer->writeU2( 0 ); // 0 for Object class
    // Write number of class interfaces (0), skipping the table itself.
    writer->writeU2(0);
    // Write number of class fields.
	writer->writeU2( this->getConstantsTable()->getFieldRefCount() );
    // Write class fields table.
	for ( TableOfConstantsRow * field : * this->getConstantsTable()->getTable() ) {
		if ( field->type == CONSTANT_FIELDREF ) {
			( (FieldRefRow * ) field )->generateFieldTableByteCode( writer );
		}
    }
    // Write number of class methods.
    writer->writeU2( this->getConstantsTable()->getMethodRefCount() );
    // Write class methods table.
    for ( TableOfConstantsRow * field : * this->getConstantsTable()->getTable() ) {
        if ( field->type == CONSTANT_METHODREF ) {
			( (MethodRefRow * ) field )->generateMethodTableByteCode( writer );
		}
    }
    // Write number of class attributes (0), skipping the table itself.
    writer->writeU2(0);
	return NULL;
}