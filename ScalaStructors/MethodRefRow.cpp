#include "Project.h"

int MethodRefRow::getClassNumber() {
	return this->class_pointer->getNumber();
}

int MethodRefRow::getNameAndTypeNumber() {
	return this->name_and_type_pointer->getNumber();
}

void MethodRefRow::generateMethodTableByteCode( BinaryWriter * writer ) {
	if ( this->abstract_node->attr.is_static ) {
		writer->writeU2( ACC_PUBLIC + ACC_STATIC );
	} else {
		writer->writeU2( ACC_PUBLIC );
	}
	writer->writeU2( this->name_and_type_pointer->getNameNumber() );
	writer->writeU2( this->name_and_type_pointer->getTypeNumber() );
	writer->writeU2( 1 ); // ���������� ���������
	writer->writeU2( 1 ); // ����� Code � ������� ��������
	this->abstract_node->generateByteCode( writer );
}