#pragma once

class sType;
class sPrimitiveType;
class sReferenceType;
class sArrayType;
class sExpression;
class sAssignmentExpression;
class sConditionalExpression;
class sConditionalOrExpression;
class sConditionalAndExpression;
class sInclusiveOrExpression;
class sExclusiveOrExpression;
class sAndExpression;
class sEqualityExpression;
class sRelationalExpression;
class sShiftExpression;
class sAdditiveExpression;
class sMultiplicativeExpression;
class sUnaryExpression;
class sPreIncrementExpression;
class sPreDecrementExpression;
class sUnaryExpressionNotPlusMinus;
class sPostfixExpression;
class sPrimary;
class sArrayCreationExpression;
class sDimExprs;
class sDimExpr;
class sDims;
class sPostIncrementExpression;
class sPostDecrementExpression;
class sAssignment;
class sLeftHandSide;
class sFieldAccess;
class sArrayAccess;
class sPrimaryNoNewArray;
class sLiteral;
class sBooleanLiteral;
class sClassInstanceCreationExpression;
class sMethodInvocation;
class sAssignmentOperator;
class sArgumentList;
class sStatement;
class sStatementWithoutTrailingSubstatement;
class sExpressionStatement;
class sStatementExpression;
class sDoStatement;
class sReturnStatement;
class sThrowStatement;
class sTryStatement;
class sCatches;
class sCatchClause;
class sFinally;
class sIfThenStatement;
class sIfThenElseStatement;
class sStatementNoShortIf;
class sIfThenElseStatementNoShortIf;
class sWhileStatementNoShortIf;
class sForStatementNoShortIf;
class sForInit;
class sStatementExpressionList;
class sForUpdate;
class sWhileStatement;
class sForStatement;
class sProgramm;
class sName;
class sTypeDeclarations;
class sTypeDeclaration;
class sClassDeclaration;
class sClassBody;
class sClassBodyDeclarations;
class sClassBodyDeclaration;
class sConstructorDeclaration;
class sConstructorDeclarator;
class sFormalParameterList;
class sFormalParameter;
class sVariableDeclaratorId;
class sConstructorBody;
class sExplicitConstructorInvocation;
class sBlockStatements;
class sBlockStatement;
class sVariableDeclarator;
class sVariableInitializer;
class sArrayInitializer;
class sVariableInitializers;
class sClassMemberDeclaration;
class sFieldDeclaration;
class sMethodDeclaration;
class sMethodHeader;
class sMethodDeclarator;
class sMethodType;
class sBlock;
class sCastExpression;
class sPackageDeclaration;
class sImportDeclarations;
class sSingletonDeclaration;
class sSingleTypeImportDeclaration;
class sTypeImportOnDemandDeclaration;

enum eType {
	PRIMITIVE_TYPE = 0,
	REFERENCE_TYPE = 1
};

enum ePrimitiveType {
	INT = 0,
	LONG = 1,
	FLOAT = 2, 
	DOUBLE = 3,
	BOOLEAN = 4,
	CHAR = 5,
	STRING_CLASS = 6,
	BYTE = 7
};

enum eArrayType {
	ARRAY_PRIMITVE_TYPE = 0,
	ARRAY_NAME = 1
};

enum eModifier { 
	EMPTY_MODIFIER = 0,
	STATIC_MODIFIER = 1,
	PUBLIC_MODIFIER = 2, 
	PROTECTED_MODIFIER = 3,
	PRIVATE_MODIFIER = 4,
	SEALED_MODIFIER = 5,
	FINAL_MODIFIER = 6,
	ABSTRACT_MODIFIER = 7
};

enum eAssignmentExpression {
	CONDITIONAL_EXPRESSION = 0,
	ASSIGMENT = 1
};

enum eEqualityExpression {
	EMPTY_EQUALITY_EXPRESSION = 0,
	EQEQ_EQUALITY_EXPRESSION = 1,
	NOTEQ_EQUALITY_EXPRESSION = 2
};

enum eRelationalExpression {
	EMPTY_RELATIONAL_EXPRESSION = 0,
	LT_RELATIONAL_EXPRESSION = 1,
	GT_RELATIONAL_EXPRESSION = 2,
	LTEQ_RELATIONAL_EXPRESSION = 3,
	GTEQ_RELATIONAL_EXPRESSION = 4
};

enum eShiftExpression {
	EMPTY_SHIFT_EXPRESSION = 0,
	LTLT_SHIFT_EXPRESSION = 1,
	GTGT_SHIFT_EXPRESSION = 2,
	GTGTGT_SHIFT_EXPRESSION = 3
};

enum eAdditiveExpression {
	EMPTY_ADDITIVE_EXPRESSION = 0,
	PLUS_ADDITIVE_EXPRESSION = 1,
	MINUS_ADDITIVE_EXPRESSION = 2
};

enum eMultiplicativeExpression {
	EMPTY_MULTIPLICATE_EXPRESSION = 0,
	TIMES_MULTIPLICATE_EXPRESSION = 1,
	DIV_MULTIPLICATE_EXPRESSION = 2,
	XOR_MULTIPLICATE_EXPRESSION = 3
};

enum ePrimary {
	PRIMARY_NO_NEW_ARRAY = 0,
	ARRAY_CREATION_EXPRESSION = 1
};

enum eCastExpression {
	PRIMITIVETYPE_ARRAY_CAST = 0,
	PRIMITIVETYPE_CAST = 1,
	EXPRESSION_CAST = 2,
	ARRAY_CAST = 3
};

enum eLeftHandSide {
	LEFT_HAND_SIDE_NAME = 0,
	LEFT_HAND_SIDE_FIELD_ACCESS = 1,
	LEFT_HAND_SIDE_ARRAY_ACCESS = 2
};

enum eArrayAccess {
	ARRAY_ACCESS_NAME = 0,
	ARRAY_ACCESS_PRIMARY_NO_NEW_ARRAY = 1
};

enum eLiteral {
	INT_LITERAL = 0,
	FLOAT_LITERAL = 1,
	BOOLEAN_LITERAL = 2,
	CHAR_LITERAL = 3,
	STRING_LITERAL = 4,
	NULL_LITERAL = 5,
	LONG_LITERAL = 6,
	DOUBLE_LITERAL = 7
};

enum eBooleanLiteral {
	TRUE_LITERAL = 0,
	FALSE_LITERAL = 1
};

enum eMethodInvocation {
	NAME = 0,
	PRIMARY = 1
};

enum eAssignmentOperator {
	ASSIGNING_ASSIGNMENT_OPERATOR = 0,
	TIMESEQ_ASSIGNMENT_OPERATOR = 1,
	DIVEQ_ASSIGNMENT_OPERATOR = 2,
	XOREQ_ASSIGNMENT_OPERATOR = 3,
	PLUSEQ_ASSIGNMENT_OPERATOR = 4,
	MINUSEQ_ASSIGNMENT_OPERATOR = 5,
    LTLTEQ_ASSIGNMENT_OPERATOR = 6,
	GTGTEQ_ASSIGNMENT_OPERATOR = 7,
	GTGTGTEQ_ASSIGNMENT_OPERATOR = 8,
	ANDEQ_ASSIGNMENT_OPERATOR = 9,
	OREQ_ASSIGNMENT_OPERATOR = 10,
	CIRCUMFLEXEQ_ASSIGNMENT_OPERATOR = 11
};

enum eImportDeclarationType {
	SINGLE_TYPE_IMPORT_DECLARATION = 0,
	TYPE_IMPORT_ON_DEMAND_DECLARATION = 1
};

enum eTypeDeclaration {
	CLASS_DECLARATION = 0,
	TRAIT_DECLARATION = 1,
	SINGLETON_DECLARATION = 2
};

enum eExplicitConstructorInvocation {
	THIS_EXPLICIT_CONSTRUCTOR_INVOCATION = 0,
	SUPER_EXPLICIT_CONSTRUCTOR_INVOCATION = 1
};

enum eBlockStatement {
	LOCAL_VARIBLE_DECLARATION = 0,
	STATEMENT = 1
};

enum eVariableInitializer {
	EXPRESSION = 0,
	ARRAY_INITIALIZER = 1
};

enum eClassMemberDeclaration {
	FIELD_DECLARATION = 0,
	METHOD_DECLARATION = 1
};

enum eArrayCreationExpression {
	ARRAY_CREATION_EXPRESSION_PRIMITIVE_TYPE = 0,
	ARRAY_CREATION_EXPRESSION_ID_TYPE = 1
};