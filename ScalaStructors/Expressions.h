#pragma once
#include "Project.h"

// �����
class sName : public Expression {
public:
	string * id;
	sName( string * _id ) : Expression(), id (_id) {
		this->cpp_type = "sName";
		string str = "Name : ";
		str.append( *_id );
		this->setDotLabel( str );
	}
	TableOfConstantsRow * getTableOfConstantsRow() override {
		if ( this->table_of_constants_row == NULL ) {
			this->table_of_constants_row = new UTF8Row( *id );
			this->getConstantsTable()->addRow( this->table_of_constants_row );
		}
		return this->table_of_constants_row;
	}
};

class sClassInstanceCreationExpression : public Expression {
public:
	sName * id;
	sArgumentList * argument_list;
	sClassInstanceCreationExpression (
			sName * _id,
			sArgumentList * _argument_list
		) : id (_id),
			argument_list (_argument_list) {
		this->addChildren( (Node *) argument_list , "argument_list");
		this->setDotLabel( "Class Instance Creation Expression" );
	}
};

class sAssignmentExpression : public Expression {
public:
	eAssignmentExpression type;
	Expression * conditional_expression;
	Expression * assignment;
	sAssignmentExpression (
			Expression * _conditional_expression,
			Expression * _assignment,
			eAssignmentExpression _type
		) : conditional_expression (_conditional_expression),
			assignment (_assignment),
			type (_type) {
		if ( conditional_expression != NULL ) {
			this->addChildren( (Node *) conditional_expression , "conditional_expression");
			this->setDotLabel( "Conditional Expression" );
		}
		if ( assignment != NULL ) {
			this->addChildren( (Node *) assignment , "assignment");
			this->setDotLabel( "Assignment" );
		}
	}
};

class sConditionalExpression : public Expression {
public:
	Expression * first;
	Expression * second;
	Expression * third;
	sConditionalExpression (
			Expression * _first,
			Expression * _second,
			Expression * _third
		) : first (_first),
			second (_second),
			third (_third) {
		this->addChildren( (Node *) first , "first");
		this->addChildren( (Node *) second , "second");
		this->addChildren( (Node *) third , "third");
		this->setDotLabel( "Conditional Expression" );
	}
};

class sConditionalOrExpression : public Expression {
public:
	Expression * conditional_or_expression;
	Expression * conditional_and_expression;
	sConditionalOrExpression (
			Expression * _conditional_and_expression,
			Expression * _conditional_or_expression
		) : conditional_or_expression (_conditional_or_expression),
			conditional_and_expression (_conditional_and_expression) {
		this->addChildren( (Node *) conditional_or_expression , "conditional_or_expression");
		this->addChildren( (Node *) conditional_and_expression , "conditional_and_expression");
		this->setDotLabel( "Conditional Or Expression" );
	}
};

class sConditionalAndExpression : public Expression {
public:
	Expression * conditional_and_expression;
	Expression * inclusive_or_expression;
	sConditionalAndExpression (
			Expression * _inclusive_or_expression,
			Expression * _conditional_and_expression
		) : conditional_and_expression (_conditional_and_expression),
			inclusive_or_expression (_inclusive_or_expression) {
		this->addChildren( (Node *) conditional_and_expression , "conditional_and_expression");
		this->addChildren( (Node *) inclusive_or_expression , "inclusive_or_expression");
		this->setDotLabel( "&&" );
	}
};

class sInclusiveOrExpression : public Expression {
public:
	Expression * inclusive_or_expression;
	Expression * exclusive_or_expression;
	sInclusiveOrExpression (
			Expression * _exclusive_or_expression,
			Expression * _inclusive_or_expression
		) : inclusive_or_expression (_inclusive_or_expression),
			exclusive_or_expression (_exclusive_or_expression) {
		this->addChildren( (Node *) inclusive_or_expression , "inclusive_or_expression");
		this->addChildren( (Node *) exclusive_or_expression , "exclusive_or_expression");
		this->setDotLabel( "Inclusive Or Expression" );
	}
};

class sExclusiveOrExpression : public Expression {
public:
	Expression * exclusive_or_expression;
	Expression * and_expression;
	sExclusiveOrExpression (
			Expression * _and_expression,
			Expression * _exclusive_or_expression
		) : exclusive_or_expression (_exclusive_or_expression),
			and_expression (_and_expression) {
		this->addChildren( (Node *) exclusive_or_expression , "exclusive_or_expression");
		this->addChildren( (Node *) and_expression , "and_expression");
		this->setDotLabel( "Exclusive Or Expression" );
	}
};

class sAndExpression : public Expression {
public:
	Expression * and_expression;
	Expression * equality_expression;
	sAndExpression (
			Expression * _equality_expression,
			Expression * _and_expression
		) : equality_expression (_equality_expression),
			and_expression (_and_expression) {
		this->addChildren( (Node *) and_expression , "and_expression");
		this->addChildren( (Node *) equality_expression , "equality_expression");
		this->setDotLabel( "And Expression" );
	}
};

class sEqualityExpression : public Expression {
public:
	eEqualityExpression type;
	Expression * equality_expression;
	Expression * relational_expression;
	sEqualityExpression (
			Expression * _relational_expression,
			Expression * _equality_expression,
			eEqualityExpression _type
		) : relational_expression (_relational_expression),
			equality_expression (_equality_expression),
			type (_type) {
		this->addChildren( (Node *) equality_expression , "equality_expression");
		this->addChildren( (Node *) relational_expression , "relational_expression");
		string str = "Equality Expression";
		if (_type == EQEQ_EQUALITY_EXPRESSION) {
			str.append(" : ==");
		}
		if (_type == NOTEQ_EQUALITY_EXPRESSION) {
			str.append(" : !=");
		}
		this->setDotLabel( str );
	}
};

class sRelationalExpression : public Expression {
public:
	eRelationalExpression type;
	Expression * relational_expression;
	Expression * shift_expression;
	sRelationalExpression (
			Expression * _shift_expression,
			Expression * _relational_expression,
			eRelationalExpression _type
		) : shift_expression (_shift_expression),
			relational_expression (_relational_expression),
			type (_type) {
		this->addChildren( (Node *) relational_expression , "relational_expression");
		this->addChildren( (Node *) shift_expression , "shift_expression");
		string str = "";
		switch (_type) {
			case LT_RELATIONAL_EXPRESSION :
				str = "<";
				break;
			case GT_RELATIONAL_EXPRESSION :
				str = ">";
				break;
			case LTEQ_RELATIONAL_EXPRESSION :
				str = "<=";
				break;
			case GTEQ_RELATIONAL_EXPRESSION :
				str = ">=";
				break;
		}
		this->setDotLabel( str );
	}
};

class sShiftExpression : public Expression {
public:
	eShiftExpression type;
	Expression * shift_expression;
	Expression * additive_expression;
	sShiftExpression (
			Expression * _additive_expression,
			Expression * _shift_expression,
			eShiftExpression _type
		) : shift_expression (_shift_expression),
			additive_expression (_additive_expression),
			type (_type) {
		this->addChildren( (Node *) shift_expression , "shift_expression");
		this->addChildren( (Node *) additive_expression , "additive_expression");
		string str = "Shift Expression";
		switch (_type) {
			case LTLT_SHIFT_EXPRESSION :
				str.append(" : <<");
				break;
			case GTGT_SHIFT_EXPRESSION :
				str.append(" : >>");
				break;
			case GTGTGT_SHIFT_EXPRESSION :
				str.append(" : >>>");
				break;
		}
		this->setDotLabel( str );
	}
};

class sAdditiveExpression : public Expression {
public:
	eAdditiveExpression type;
	Expression * additive_expression;
	Expression * multiplicative_expression;
	sAdditiveExpression (
			Expression * _multiplicative_expression,
			Expression * _additive_expression,
			eAdditiveExpression _type
		) : multiplicative_expression (_multiplicative_expression),
			additive_expression (_additive_expression),
			type (_type) {
		this->addChildren( (Node *) additive_expression , "additive_expression");
		this->addChildren( (Node *) multiplicative_expression , "multiplicative_expression");
		string str = "Additive Expression";
		switch (_type) {
			case PLUS_ADDITIVE_EXPRESSION :
				str.append(" : +");
				break;
			case MINUS_ADDITIVE_EXPRESSION :
				str.append(" : -");
				break;
		}
		this->setDotLabel( str );
	}
};
;

class sMultiplicativeExpression : public Expression {
public:
	eMultiplicativeExpression type;
	Expression * multiplicative_expression;
	Expression * unary_expression;
	sMultiplicativeExpression (
			Expression * _unary_expression,
			Expression * _multiplicative_expression,
			eMultiplicativeExpression _type
		) : unary_expression (_unary_expression),
			multiplicative_expression (_multiplicative_expression),
			type (_type) {
		this->addChildren( (Node *) multiplicative_expression , "multiplicative_expression");
		this->addChildren( (Node *) unary_expression , "unary_expression");
		string str = "Multiplicative Expression";
		switch (_type) {
			case TIMES_MULTIPLICATE_EXPRESSION :
				str.append(" : *");
				break;
			case DIV_MULTIPLICATE_EXPRESSION :
				str.append(" : /");
				break;
			case XOR_MULTIPLICATE_EXPRESSION :
				str.append(" : %");
				break;
		}
		this->setDotLabel( str );
	}
};

class sUnaryExpression : public Expression {
public:
	Expression * pre_increment_expression;
	Expression * pre_decrement_expression;
	Expression * plus_unary_expression;
	Expression * minus_unary_expression;
	Expression * unary_expression_not_plus_minus;
	sUnaryExpression (
			Expression * _pre_increment_expression,
			Expression * _pre_decrement_expression,
			Expression * _plus_unary_expression,
			Expression * _minus_unary_expression,
			Expression * _unary_expression_not_plus_minus
		) :	pre_increment_expression (_pre_increment_expression),
			pre_decrement_expression (_pre_decrement_expression),
			plus_unary_expression (_plus_unary_expression),
			minus_unary_expression (_minus_unary_expression),
			unary_expression_not_plus_minus (_unary_expression_not_plus_minus) {
		this->addChildren( (Node *) pre_increment_expression , "pre_increment_expression");
		this->addChildren( (Node *) pre_decrement_expression , "pre_decrement_expression");
		this->addChildren( (Node *) plus_unary_expression , "plus_unary_expression");
		this->addChildren( (Node *) minus_unary_expression , "minus_unary_expression");
		this->addChildren( (Node *) unary_expression_not_plus_minus , "unary_expression_not_plus_minus");
		this->setDotLabel( "Unary Expression" );
	}
};

class sPreIncrementExpression : public Expression {
public:
	Expression * unary_expression;
	sPreIncrementExpression (
			Expression * _unary_expression
		) : unary_expression (_unary_expression) {
		this->addChildren( (Node *) unary_expression , "unary_expression");
		this->setDotLabel( "++( )" );
	}
};

class sPreDecrementExpression : public Expression {
public:
	Expression * unary_expression;
	sPreDecrementExpression (
			Expression * _unary_expression
		) : unary_expression (_unary_expression) {
		this->addChildren( (Node *) unary_expression , "unary_expression");
		this->setDotLabel( "--( )" );
	}
};

class sUnaryExpressionNotPlusMinus : public Expression {
public:
	Expression * postfix_expression;
	Expression * tilde_unary_expression;
	Expression * not_unary_expression;
	Expression * cast_expression;
	sUnaryExpressionNotPlusMinus (
			Expression * _postfix_expression,
			Expression * _tilde_unary_expression,
			Expression * _not_unary_expression,
			Expression * _cast_expression
		) :	postfix_expression (_postfix_expression),
			tilde_unary_expression (_tilde_unary_expression),
			not_unary_expression (_not_unary_expression),
			cast_expression (_cast_expression) {
		this->addChildren( (Node *) postfix_expression , "postfix_expression");
		this->addChildren( (Node *) tilde_unary_expression , "tilde_unary_expression");
		this->addChildren( (Node *) not_unary_expression , "not_unary_expression");
		this->addChildren( (Node *) cast_expression , "cast_expression");
		this->setDotLabel( "Unary Expression Not Plus Minus" );
	}
};

class sPostIncrementExpression : public Expression {
public:
	Expression * postfix_expression_before_inc;
	sPostIncrementExpression (
			Expression * _postfix_expression_before_inc
		) : postfix_expression_before_inc (_postfix_expression_before_inc) {
		this->addChildren( (Node *) postfix_expression_before_inc , "postfix_expression_before_inc");
		this->setDotLabel( "( )++" );
	}
};

class sPostDecrementExpression : public Expression {
public:
	Expression * postfix_expression_before_dec;
	sPostDecrementExpression (
			Expression * _postfix_expression_before_dec
		) : postfix_expression_before_dec (_postfix_expression_before_dec) {
		this->addChildren( (Node *) postfix_expression_before_dec , "postfix_expression_before_dec");
		this->setDotLabel( "( )--" );
	}
};

class sAssignment : public Expression {
public:
	sLeftHandSide * left_hand_side;
	sAssignmentOperator * assigment_operator;
	Expression * assigment_expression;
	sAssignment (
			sLeftHandSide * _left_hand_side,
			sAssignmentOperator * _assigment_operator,
			Expression * _assigment_expression
		) : left_hand_side (_left_hand_side),
			assigment_operator (_assigment_operator),
			assigment_expression (_assigment_expression) {
		this->addChildren( (Node *) left_hand_side , "left_hand_side");
		this->addChildren( (Node *) assigment_operator , "assigment_operator");
		this->addChildren( (Node *) assigment_expression , "assigment_expression");
		this->setDotLabel( "Assignment" );
	}
};

class sAssignmentOperator : public Expression {
public:
	Expression * left_hand_side;
	Expression * assigment_expression;
	eAssignmentOperator type;
	sAssignmentOperator(eAssignmentOperator _type);

	sAssignmentOperator * setOperands(Expression * _left_hand_side, Expression * _assigment_expression);
	void transformToArrayAssignment();
};

class sThis : Expression {
public:
	sThis() {
		this->setDotLabel( "This" );
	}
};

class sBooleanLiteral : public Expression {
public:
	eBooleanLiteral type;
	sBooleanLiteral ( eBooleanLiteral _type ) : type (_type) {
		if (_type == TRUE_LITERAL) {
			this->setDotLabel( "TRUE" );
		} else {
			this->setDotLabel( "FALSE" );
		}
	}
};

// ��� ���� ���������
class sLiteral : public Expression {
public:
	eLiteral type;
	int int_literal;
	float float_literal;
	sBooleanLiteral boolean_literal;
	char char_literal;
	string string_literal;
	long long long_literal;
	double double_literal;
	sLiteral (
			int _int_literal,
			float _float_literal,
			sBooleanLiteral _boolean_literal,
			char _char_literal,
			string _string_literal,
			long long _long_literal,
			double _double_literal,
			eLiteral _type
		) : int_literal (_int_literal),
			float_literal (_float_literal),
			boolean_literal (_boolean_literal),
			char_literal (_char_literal),
			string_literal (_string_literal),
			long_literal (_long_literal),
			double_literal (_double_literal),
			type (_type) {
		this->cpp_type = "sLiteral";
		string str = "";
		ostringstream buff;
		switch (_type) {
			case INT_LITERAL :
				this->attr.ariphmetic_type = CINT;
				str.append( "Int literal : " );
				str.append( string( itoa( int_literal, new char[50], 10 ) ) );
				break;
			case FLOAT_LITERAL :
				this->attr.ariphmetic_type = CFLOAT;
				str.append( "Float literal : " );
				buff << float_literal;
				str.append( buff.str() );
				break;
			case BOOLEAN_LITERAL :
				this->attr.ariphmetic_type = CBOOL;
				this->addChildren( (Node *) & boolean_literal , "& boolean_literal");
				str.append( "Boolean literal" );
				break;
			case CHAR_LITERAL :
				this->attr.ariphmetic_type = CCHAR;
				str.append( "Char literal : " );
				str.push_back( char_literal );
				break;
			case STRING_LITERAL :
				this->attr.ariphmetic_type = CSTRING;
				str.append( "String literal : " );
				str.append( string_literal );
				break;
			case NULL_LITERAL :
				this->attr.ariphmetic_type = CNULL;
				str.append( "Null literal" );
				break;
			case LONG_LITERAL :
				this->attr.ariphmetic_type = CLONG;
				str.append( "Long literal : " );
				str.append( string( itoa( long_literal, new char[50], 10 ) ) );
				break;
			case DOUBLE_LITERAL :
				this->attr.ariphmetic_type = CDOUBLE;
				str.append( "Double literal : " );
				buff << double_literal;
				str.append( buff.str() );
				break;
		}
		this->setDotLabel( str );
	}
	TableOfConstantsRow * getTableOfConstantsRow() override {
		if ( this->table_of_constants_row == NULL ) {
			UTF8Row * row = NULL;
			switch (type) {
				case INT_LITERAL : this->table_of_constants_row = new IntegerRow( this->int_literal ); break;
				case FLOAT_LITERAL : this->table_of_constants_row = new FloatRow( this->float_literal ); break;
				case CHAR_LITERAL : this->table_of_constants_row = new IntegerRow( (int) this->char_literal ); break;
				case STRING_LITERAL :
					row = new UTF8Row( this->string_literal );
					this->getConstantsTable()->addRow( row );
					this->table_of_constants_row = new StringRow( row );
					break;
				case LONG_LITERAL : this->table_of_constants_row = new LongRow( this->int_literal ); break;
				case DOUBLE_LITERAL : this->table_of_constants_row = new DoubleRow( this->float_literal ); break;
			}
			this->getConstantsTable()->addRow( this->table_of_constants_row );
		}
		return this->table_of_constants_row;
	}
};

class sFieldAccess : public Expression {
public:
	bool is_super;
	Expression * source;
	sName * target;
	sFieldAccess (
			Expression * _primary,
			sName * _id,
			bool _is_super
			) : source(_primary),
			target(_id),
			is_super (_is_super) {
		this->addChildren((Node *) source, "source");
		this->addChildren( (Node *) target , "target");
		this->setDotLabel( "Field Access" );
	}
};

class sMethodInvocation : public Expression {
public:
	sName * methodname;
	Expression * source; // if source == null it equals 'this'
	sArgumentList * argument_list;
	sMethodInvocation(sName * _methodname,sArgumentList * _argument_list,Expression * _source);
	void transformToArrayAccess();
	virtual void calcAttr() override;
private:
	bool canBeArrayAccess();
};

class sArrayInitializer : Expression {
public:
	sVariableInitializers * variable_initializers;
	sPrimitiveType * primitive_type;
	sName * id;
	sArrayInitializer (
			sPrimitiveType * _primitive_type,
			sName * _id,
			sVariableInitializers * _variable_initializers
		) :	primitive_type (_primitive_type),
			id (_id),
			variable_initializers (_variable_initializers) {
		this->addChildren( (Node *) id , "id");
		this->addChildren( (Node *) primitive_type , "primitive_type");
		this->addChildren( (Node *) variable_initializers , "variable_initializers");
		if ( primitive_type != NULL ) {
			this->setDotLabel( "Array Creation Expression : Primitive Type" );
		} else {
			this->setDotLabel( "Array Creation Expression : ID" );
		}
	}
};

class sArrayCreationExpression : Expression {
public:
    sPrimitiveType * primitive_type;
    sName * name;
    sArrayCreationExpression (sPrimitiveType * _primitive_type, sName * _name) : primitive_type (_primitive_type), name(_name)
    {
        this->addChildren( (Node *) primitive_type , "primitive_type");
        this->addChildren( (Node *) name , "name");
        this->setDotLabel( "Empty Array Creation Expression" );
    }
};

class fakeArrayAccess : public Expression {
public:
	sName * arrayname;
	Expression * source;
	Expression * argument;
	fakeArrayAccess(sName * _arrayname, Expression * _argument, Expression * _source);
};

class fakeArrayAssignment : public sAssignmentOperator {
public:
	sName * arrayname;
	Expression * source;
	Expression * indexator;
	fakeArrayAssignment(eAssignmentOperator _type,
		sName * _arrayname,
		Expression * _indexator,
		Expression * _source,
		Expression * _assigment_expression
		);
};

class sCastExpression : public Expression {
public:
	eCastExpression type;
	sPrimitiveType * primitive_type;
	sDims * dims;
	sExpression * expression;
	string name;
	sUnaryExpression * unary_expression;
	sUnaryExpressionNotPlusMinus * unary_expression_not_plus_minus;
	sCastExpression(
		sPrimitiveType * _primitive_type,
		sDims * _dims,
		sExpression * _expression,
		string _name,
		sUnaryExpression * _unary_expression,
		sUnaryExpressionNotPlusMinus * _unary_expression_not_plus_minus,
		eCastExpression _type
		) : primitive_type(_primitive_type),
		dims(_dims),
		expression(_expression),
		name(_name),
		unary_expression(_unary_expression),
		unary_expression_not_plus_minus(_unary_expression_not_plus_minus),
		type(_type) {
		this->addChildren((Node *)primitive_type, "primitive_type");
		this->addChildren((Node *)dims, "dims");
		this->addChildren((Node *)expression, "expression");
		this->addChildren((Node *)unary_expression, "unary_expression");
		this->addChildren((Node *)unary_expression_not_plus_minus, "unary_expression_not_plus_minus");
		string str = "Cast Expression : ";
		switch (_type) {
		case PRIMITIVETYPE_ARRAY_CAST:
			str.append(" : Primitive Array Cast");
			break;
		case PRIMITIVETYPE_CAST:
			str.append(" : Primitive Cast");
			break;
		case EXPRESSION_CAST:
			str.append(" : Expression Cast");
			break;
		case ARRAY_CAST:
			str.append(" : Array Cast");
			break;
		}
		this->setDotLabel(str);
	}
};
