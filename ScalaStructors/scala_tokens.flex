%{
  #pragma warning(disable : 4996)
  #include <stdio.h>
  #include <string.h>
  #include <stdlib.h>
  #include "bscala.bison_tab.h"
  char * tokenStart;
  char tstr[10000];
  int tokenLength;
%}

LineTerminator \r|\n|\r\n
InputCharacter [^\r\n]

WhiteSpace {LineTerminator}|[ \t\f]+

TraditionalComment "/*"[^*]~"*/"|"/*""*"+"/"
EndOfLineComment "//"{InputCharacter}*{LineTerminator}?
Comment {TraditionalComment}|{EndOfLineComment}

Identifier ([a-zA-Z_$][a-zA-Z0-9_$]+)|([a-zA-Z$])

DecIntegerLiteral 0|[1-9][0-9]*
DecLongLiteral    {DecIntegerLiteral}[lL]

HexIntegerLiteral 0[xX]0*{HexDigit}{1,8}
HexLongLiteral    0[xX]0*{HexDigit}{1,16}[lL]
HexDigit          [0-9a-fA-F]

OctIntegerLiteral 0+{OctDigit}{1,15}
OctLongLiteral    0+{OctDigit}{1,21}[lL]
OctDigit          [0-7]

FLit1    [0-9]+\.[0-9]*
FLit2    \.[0-9]+
FLit3    [0-9]+
Exponent [eE][+-]?[0-9]+

FloatLiteral  ({FLit1}|{FLit2}|{FLit3}){Exponent}?[fF]
DoubleLiteral ({FLit1}|{FLit2}|{FLit3}){Exponent}?[dD]?

StringCharacter [^\r\n\"\\]
SingleCharacter [^\r\n\'\\]

%option noyywrap
%option never-interactive
%option debug
%option outfile="lexer.cpp"

%x TCHARLITERALSTATE
%x STRING
%x MCOMMENT
%x MCOMMENT_TAG

%%
"def"            {return TDEF;}
"import"         {return TIMPORT;}
"package"        {return TPACKAGE;}
"if"             {return TIF;}
"then"           {return TTHEN;}
"else"           {return TELSE;}
"while"          {return TWHILE;}
"for"            {return TFOR;}
"do"             {return TDO;}
"Boolean"        {return TBOOLEAN;}
"Int"            {return TINT;}
"Double"         {return TDOUBLE;}
"Byte"           {return TBYTE;}
"Short"          {return TSHORT;}
"Char"            {return TCHAR;}
"Long"            {return TLONG;}
"Float"           {return TFLOAT;}
"Unit"            {return UNIT;}
"Array"            {return TARRAY;}
"val"             {return VAL;}
"with"            {return WITH;}
"type"            {return TTYPE;}
"var"             {return VAR;}
"yield"           {return YIELD;}
"return"          {return TRETURN;}
"true"            {return TTRUE;}
"false"           {return TFALSE;}
"null"            {return TNULL;}
"this"            {return TTHIS;}
"super"           {return SUPER;}
"private"        {return TPRIVATE;}
"protected"      {return TPROTECTED;}
"override"       {return OVERRIDE;}
"abstract"       {return TABSTRACT;}
"final"          {return FINAL;}
"sealed"         {return SEALED;}
"throw"          {return TTHROW;}
"try"            {return TTRY;}
"catch"          {return TCATCH;}
"finally"        {return FINALLY;}
"trait"          {return TRAIT;}
"class"          {return TCLASS;}
"case"           {return TCASE;}
"forSome"        {return TFORSOME;}
"implicit"       {return IMPLICIT;}
"lazy"           {return LAZY;}
"match"          {return MATCH;}
"new"            {return TNEW;}
"..."            {return TDOTTDOTTDOT;}
"extends"        {return EXTENDS;}
"public"        {return TPUBLIC;}
"static"         {return TSTATIC;}
"until"         {return TUNTIL;}

"_"                            {return ANYVAR;}

"object"                       {return TOBJECT;}

"Throwable"                    {return TTHROWABLE_JI;}
"Cloneable"                    {return CLONABLE_JI;}
"Comparable"                   {return COMPARABLE_JI;}
"Serializable"                 {return SERIALIZABLE_JI;}
"Runnable"                     {return RUNNABLE_JI;}
"String"                     {return STRING_TCLASS;}

"("                            {  return (unsigned char)'('; }
")"                            {  return (unsigned char)')'; }
"{"                            {  return (unsigned char)'{'; }
"}"                            {  return (unsigned char)'}'; }
"["                            {  return (unsigned char)'['; }
"]"                            {  return (unsigned char)']'; }
";"                            {return SEMICOLON;}
","                            {return COMMA;}
"."                            {return POINT;}
"="                            {return ASSIGNING;}
">"                            {return GT;}
"<"                            {return LT;}
"!"                            {return NOT;}
"~"                            {return TILDE;}
"?"                            {return QUESTION;}
":"                            {return COLON;}
"=="                           {return EQEQ;}
"<="                           {return LTEQ;}
">="                           {return GTEQ;}
"=>"                           {return COMPILES;}
"!="                           {return NOTEQ;}
"&&"                           {return ANDAND;}
"||"                           {return OROR;}
"++"                           {return PLUSPLUS;}
"--"                           {return MINUSMINUS;}
"+"                            {return PLUS;}
"-"                            {return MINUS;}
"*"                            {return TIMES;}
"/"                            {return DIV;}
"&"                            {return AND;}
"|"                            {return OR;}
"^"                            {return CIRCUMFLEX;}
"%"                            {return XOR;}
"<<"                           {return LTLT;}
">>"                           {return GTGT;}
">>>"                          {return GTGTGT;}
"+="                           {return PLUSEQ;}
"-="                           {return MINUSEQ;}
"*="                           {return TIMESEQ;}
"/="                           {return DIVEQ;}
"&="                           {return ANDEQ;}
"|="                           {return OREQ;}
"^="                           {return CIRCUMFLEXEQ;}
"%="                           {return XOREQ;}
"<<="                          {return LTLTEQ;}
">>="                          {return GTGTEQ;}
">:"                           {return GTCOLON;}
"<:"                           {return LTCOLON;}
"<%"                           {return LTDIV;}
"<-"                           {return LTMINUS;}
":::"                          {return COLONCOLONCOLON;}
">>>="                         {return GTGTGTEQ;}

\" {
  BEGIN(STRING);
  tokenStart = yytext+1;
  tokenLength = 0;
}

\' {
  BEGIN(TCHARLITERALSTATE);
  tokenStart = yytext+1;
  tokenLength = 0;
}

{DecIntegerLiteral}            {
  long t = strtol(yytext,NULL,10);
  yylval.int_value = new long(t);
  return TINTLITERAL;
}

{DecLongLiteral}               {
  long long t = strtol(yytext,NULL,10);
  yylval.long_value = new __int64(t);
  return TLONGLITERAL;
}

{HexIntegerLiteral}            {
  long t = strtol(yytext,NULL,16);
  yylval.int_value = new long(t);
  return TINTLITERAL;
}

{HexLongLiteral}               {
  long long t = strtol(yytext,NULL,16);
  yylval.long_value = new __int64(t);
  return TLONGLITERAL;
}

{OctIntegerLiteral}            {
  long t = strtol(yytext,NULL,8);
  yylval.int_value = new long(t);
  return TINTLITERAL;
}

{OctLongLiteral}               {
  long long t = strtol(yytext,NULL,8);
  yylval.long_value = new __int64(t);
  return TLONGLITERAL;
}

{FloatLiteral}                 {
  double t = strtod(yytext,NULL);
  yylval.float_value = new float((float)t);
  return TFLOATLITERAL;
}

{DoubleLiteral}                {
  double t = strtod(yytext,NULL);
  yylval.double_value = new double(t);
  return TDOUBLELITERAL;
}

"/*"                          {
                                 BEGIN(MCOMMENT);
                                 tokenStart = yytext;
                                 tokenLength = 3;
                               }

{Comment}                      { }


{WhiteSpace}                   {  }


{Identifier}                   {
	yylval.string_value = new string(yytext);
	return ID;
}

<STRING>\" {
   BEGIN(INITIAL);
   tstr[0] = 0;
   strncat(tstr, tokenStart, tokenLength);
   // TODO STRING PREPROCESSING
   yylval.string_value = new string(tstr);
   return STRINGLITERAL;
}

<STRING>{StringCharacter}+             { tokenLength += yyleng; }

<STRING>{LineTerminator}               {
  BEGIN(INITIAL);
}

<TCHARLITERALSTATE>\'  {
   BEGIN(INITIAL);
   tstr[0] = 0;
   strncat(tstr, tokenStart, tokenLength);
   // TODO STRING PREPROCESSING
   yylval.char_value = new char(tstr[0]);
   return TCHARLITERAL;
}

<TCHARLITERALSTATE>{SingleCharacter}+             { tokenLength += yyleng; }

<TCHARLITERALSTATE>{LineTerminator}               {
  BEGIN(INITIAL);
}

<MCOMMENT>"*/" {
   BEGIN(INITIAL);
   tstr[0] = 0;
   strncat(tstr, tokenStart, tokenLength+1);
}

<MCOMMENT>.|\n                        { tokenLength ++; }



%%